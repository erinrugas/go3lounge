<?php

namespace App\Providers;

use App\Notifications\Channels\ InfoBipChannel;
use Illuminate\Support\ServiceProvider;

class InfoBipServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InfoBipChannel::class, function ($app) {
            return new InfoBipChannel(
                $app['config']['services.infobip.username'],
                $app['config']['services.infobip.password'],
                $app['config']['services.infobip.sms_from']
            );
        });
    }
}
