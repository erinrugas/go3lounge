<?php

namespace App\Providers;

use App\Notifications\Channels\InfobipSmsChannel;
use Illuminate\Support\ServiceProvider;
use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;

class InfobipSmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InfobipSmsChannel::class, function ($app) {
            return new InfobipSmsChannel(
                new SendSingleTextualSms(new BasicAuthConfiguration(
                    $app['config']['services.infobip.username'], $app['config']['services.infobip.password']
                )),
                $app['config']['services.infobip.sms_from']
            );
        });
    }
}
