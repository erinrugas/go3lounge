<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\MerchantInformations;
use App\Models\Merchants\MerchantInformation;
use App\Models\Review;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class SendReviewSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendsms:review';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending SMS Review Executed';

    /**
     * sms
     * @var string
     */
    private $channel = "review-sms";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $merchantInformations = MerchantInformation::get();

        foreach($merchantInformations as $merchantInformation) {
            \DB::purge('mysql2');
            $bsid = \App\Services\Utilities\Helper::padZero($merchantInformation->business_id, 4);
            config(["database.connections.mysql2.prefix" => $bsid . '_']);

            if($merchantInformation->timezone()->count() > 0){
                $tz = $merchantInformation->timezone->timezone;
                date_default_timezone_set($tz);
            }

            $reviews = Review::with('customer')->get();

            foreach ($reviews as $review) {
                if ($review->status != 0) {
                    if (Carbon::parse($review->date_reviewed)->addMinutes(5)->format('Y-m-d H:i') == Carbon::now()->format('Y-m-d H:i')) {
                        $review->notify(new \App\Notifications\ReviewMessage($review, $review->rating, $bsid, "sms"));
                        Log::channel($this->channel)->info('SMS Review link sent '.Carbon::now()->format('Y-m-d H:i:s') - $review->customer->tel_prefix);
                    }
                }
            }
        }

        $this->line('Review SMS is Executed');
    }
}
