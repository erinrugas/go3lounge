<?php

namespace App\Notifications;

use App\Contracts\Constant;
use App\Models\Customer;
use App\Models\Message;
use App\Models\MessageCategory;
use App\Notifications\Channels\InfoBipChannel;
use App\Notifications\Messages\InfobipMessage;
use App\Services\Utilities\Helper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class ReviewMessage extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Customer
     * @var
     */
    private $customer;

    /**
     * Notification Type
     * @var
     */
    private $notificationType;

    /**
     * How many Star
     * @var
     */
    private $rate;

    /**
     * Business Id
     * @var
     */
    private $bid;

    /**
     * ReviewMessage constructor.
     * @param $customer
     * @param $rate
     * @param $bid
     * @param $notificationType
     */
    public function __construct($customer, $rate, $bid, $notificationType)
    {
        $this->customer = $customer;
        $this->rate = $rate;
        $this->notificationType = $notificationType;
        $this->bid = $bid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->notificationType == "mail") {
            return ['mail'];
        } else {
            return [InfoBipChannel::class];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toInfoBip($notifiable)
    {
        $businessId = Helper::padZero($this->bid, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);

        $low = MessageCategory::where(['status' => Constant::STATUS_INTEGER_ACTIVE, 'id' => Constant::STATUS_MESSAGE_CATEGORY_LOW])->first();
        $high = MessageCategory::where(['status' => Constant::STATUS_INTEGER_ACTIVE, 'id' => Constant::STATUS_MESSAGE_CATEGORY_HIGH])->first();

        if ($this->rate < 5) {
            $messages = Message::where(['status' => Constant::STATUS_INTEGER_ACTIVE, 'message_category_id' => $low->id])->get();
            $message = Arr::random($messages->toArray());
        } else {
            $messages = Message::where(['status' => Constant::STATUS_INTEGER_ACTIVE, 'message_category_id' => $high->id])->get();
            $message = Arr::random($messages->toArray());
        }

        $sanitizePhoneNumber = $notifiable->customer->tel_prefix . preg_replace('/[^0-9]/', '', $notifiable->customer->mobile_number);

        return (new InfobipMessage())
            ->message($message['message'])
            ->mobileNumber($sanitizePhoneNumber)
            ->getInformation();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
