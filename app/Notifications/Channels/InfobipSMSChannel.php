<?php

namespace App\Notifications\Channels;

use App\Notifications\Messages\InfobipMessage;
use Illuminate\Notifications\Notification;
use infobip\api\client\SendSingleTextualSms as InfobipClient;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest as InfobipRequest;

class InfobipSmsChannel
{
    /**
     * The Nexmo client instance.
     *
     * @var InfobipClient
     */
    protected $infobipClient;

    /**
     * The phone number notifications should be sent from.
     *
     * @var string
     */
    protected $from;

    /**
     * The phone number notifications should be sent to.
     *
     * @var string
     */
    protected $to;

    /**
     * Create a new Nexmo channel instance.
     *
     * @param  InfobipClient $infobip
     * @param  string $from
     */
    public function __construct(InfobipClient $infobip, $from)
    {
        $this->infobipClient = $infobip;
        $this->from = $from;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return bool|\infobip\api\model\sms\mt\send\SMSResponse
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toInfobip($notifiable);

        if (is_string($message)) {
            $message = new InfobipMessage($message);
        }

        $to = isset($message->to) ? $message->to : (isset($this->to) ? $this->to : $notifiable->routeNotificationFor('infobip'));
        if (!$to) {
            return false;
        }

        $infobipRequest = new InfobipRequest();
        $infobipRequest->setFrom($message->from ?: $this->from);
        $infobipRequest->setTo($to);
        $infobipRequest->setText(trim($message->content));

        return $this->infobipClient->execute($infobipRequest);
    }

    /**
     * @param string $to
     * @return InfobipSmsChannel
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }
}
