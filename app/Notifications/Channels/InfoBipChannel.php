<?php
/**
 * Created by PhpStorm.
 * User: eunamagpantay
 * Date: 3/19/19
 * Time: 12:39 PM
 */

namespace App\Notifications\Channels;


use Go3\InfoBip\InfoBipProvider;
use Illuminate\Notifications\Notification;

class InfoBipChannel
{
    /**
     * username
     * @var string
     */
    protected $username;

    /**
     * password
     * @var string
     */
    protected $password;

    /**
     * clientName
     * @var string
     */
    protected $clientName;

    /**
     * InfoBipChannel constructor.
     * @param $username
     * @param $password
     * @param $clientName
     */
    public function __construct($username, $password, $clientName)
    {
        $this->username = $username;
        $this->password = $password;
        $this->clientName = $clientName;
    }

    /**
     * Send
     * @param $notifiable
     * @param Notification $notification
     * @return mixed
     */
    public function send($notifiable, Notification $notification)
    {
        $infoBipMessage = $notification->toInfoBip($notifiable);
        $client = new InfoBipProvider($this->username, $this->password, $this->clientName);
        $response = $client->send(
            $infoBipMessage['message'],
            $infoBipMessage['mobileNumber']
        );
        return $response;
    }
}
