<?php
/**
 * Created by PhpStorm.
 * User: eunamagpantay
 * Date: 1/14/19
 * Time: 5:43 PM
 */

namespace App\Notifications\Messages;


class NexmoSMSMessage
{

    /**
     * @var string
     */
    private $from = '';

    /**
     * @var string
     */
    private $to = '';

    /**
     * @var string
     */
    private $text = '';

    /**
     * @param $from
     * @return NexmoSMSMessage
     */
    public function from($from) : NexmoSMSMessage
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @param $to
     * @return NexmoSMSMessage
     */
    public function to($to) : NexmoSMSMessage
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @param $text
     * @return NexmoSMSMessage
     */
    public function text($text) : NexmoSMSMessage
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Get information
     * @return array
     */
    public function getInformation() : array
    {
        return [
            'from' => $this->from,
            'to' => $this->to,
            'text' => $this->text
        ];
    }

}
