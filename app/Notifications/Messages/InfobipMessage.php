<?php

namespace App\Notifications\Messages;

class InfobipMessage
{
    public $message;

    public $mobileNumber;

    /**
     * Create a new message instance.
     * InfobipMessage constructor.
     * @param $message
     * @param $mobileNumber
     */
    public function __construct($message = "", $mobileNumber = "")
    {
        $this->message = $message;
        $this->mobileNumber = $mobileNumber;
    }

    /**
     * @param $message
     * @return $this
     */
    public function message($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param $mobileNumber
     * @return $this
     */
    public function mobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
        return $this;
    }

    /**
     * Get information
     * @return array
     */
    public function getInformation()
    {
        return [
            'mobileNumber' => $this->mobileNumber,
            'message' => $this->message
        ];
    }
}
