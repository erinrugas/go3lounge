<?php
/**
 * Created by PhpStorm.
 * User: eunamagpantay
 * Date: 3/25/19
 * Time: 1:14 PM
 */

namespace App\Notifications\Messages;

class PusherMessage
{
    /**
     * @var
     */
    private $channel;

    /**
     * @var
     */
    private $event;

    /**
     * @var
     */
    private $data;

    /**
     * PusherMessage constructor.
     * @param $channel
     * @param $event
     * @param $data
     */
    public function __construct($channel = "", $event = "", $data = "")
    {
        $this->channel = $channel;
        $this->event = $event;
        $this->data = $data;
    }

    /**
     * Channel
     * @param $channel
     * @return PusherMessage
     */
    public function channel($channel) : PusherMessage
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * Event
     * @param $event
     * @return PusherMessage
     */
    public function event($event) : PusherMessage
    {
        $this->event = $event;
        return $this;
    }

    /**
     * Data
     * @param $data
     * @return PusherMessage
     */
    public function data($data) : PusherMessage
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get information
     * @return array
     */
    public function getInformation()
    {
        return [
            'channel' => $this->channel,
            'event' => $this->event,
            'data' => $this->data
        ];
    }
}
