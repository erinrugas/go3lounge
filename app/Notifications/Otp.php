<?php

namespace App\Notifications;

use App\Models\Customer;
use App\Notifications\Channels\InfoBipChannel;
use App\Notifications\Messages\InfobipMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Otp extends Notification
{
    use Queueable;

    /**
     * Customer
     * @var
     */
    private $customer;

    /**
     * Notification Type
     * @var
     */
    private $notificationType;

    /**
     * OTP code
     * @var
     */
    private $code;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($code, $notificationType)
    {
        $this->code = $code;
        $this->notificationType = $notificationType;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->notificationType == "mail") {
            return ['mail'];
        } else {
            return [InfoBipChannel::class];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toInfoBip($notifiable)
    {

        $message = "{$this->code} is your Go3Checkin code.";

        $sanitizePhoneNumber = $notifiable->tel_prefix . str_replace("-", "", $notifiable->mobile_number);

        return (new InfobipMessage())
            ->message($message)
            ->mobileNumber($sanitizePhoneNumber)
            ->getInformation();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
