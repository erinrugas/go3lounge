<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 2/10/20
 * Time: 9:14 AM
 */

namespace App\Contracts;


interface MerchantInformationContract
{

    /**
     * Get Customer By Mobile Number
     * @param $mobile
     * @return mixed
     */
    public function getCustomerByNumber($mobile);

}
