<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erin-rugas
 * Date: 1/28/20
 * Time: 11:08 AM
 */

namespace App\Contracts;

interface OTP {

    /**
     * @param $data
     * @param $isSend
     * @return mixed
     */
    public function send($data, $isSend);

}
