<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 1/28/20
 * Time: 11:26 AM
 */

namespace App\Contracts;


class Constant
{

    const STATUS_INTEGER_ACTIVE = 1;
    const STATUS_INTEGER_INACTIVE = 0;
    const STATUS_INTEGER_DELETED = 2;
    const STATUS_TEXT_ACTIVE = 'ACTIVE';
    const STATUS_TEXT_INACTIVE = 'INACTIVE';
    const STATUS_MESSAGE_CATEGORY_LOW = 2;
    const STATUS_MESSAGE_CATEGORY_HIGH = 3;

    const FLAG_SUCCESS = "SUCCESS";
    const FLAG_ERROR = "ERROR";

    const HTTP_RESPONSE_SUCCESS = 200;
    const HTTP_RESPONSE_NOT_FOUND = 404;
    const HTTP_RESPONSE_UN_PROCESS = 422;
    const HTTP_RESPONSE_UNAUTHORIZED = 401;
    const HTTP_RESPONSE_NEWLY_CREATED = 201;



}
