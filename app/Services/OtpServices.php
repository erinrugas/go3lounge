<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 1/28/20
 * Time: 11:20 AM
 */

namespace App\Services;

use App\Contracts\OTP;
use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Services\Utilities\Helper;
use App\Services\Utilities\StrHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use phpseclib\Math\BigInteger;

class OtpServices implements OTP
{

    /**
     * Duration of OTP in minutes
     * @var int
     */
    private $duration = 5; // minutes

    /**
     * Send Otp
     * @param $data
     * @param $isSend
     * @return mixed|string
     */
    public function send($data, $isSend)
    {

        $now = Carbon::now();

        $lastFiveMinutes =  intval($now->format('i')) - intval($now->format('i')) % $this->duration;

        $timestamp = "{$now->year}{$now->month}{$now->day}{$now->hour}$lastFiveMinutes";

        $seed = $data['contact_number'].$timestamp;

        $generatedSeed = StrHelper::asInt($seed);

        list($x, $rem) = $generatedSeed->divide(new BigInteger(1000000));
        $gmp = 1000000 - gmp_intval($rem->value);

        $crypt = StrHelper::crypt($gmp, 6, 'int');

        $cust = [];

        if ($data['contact_number'] == "916 770 6381") {
            $crypt = "768641";
        }


        if ($isSend) {

            $getMobile = Helper::formatNumber($data['contact_number'], $data['prefix']);

            $merchantInformations = MerchantInformation::all();
            foreach ($merchantInformations as $key => $merchantInformation) {
                $businessId = Helper::padZero($merchantInformation->business_id, 4);
                config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
                DB::purge('mysql2');
                $customers = Customer::where('mobile_number', $getMobile)->get();
                foreach ($customers as $customer) {
                    $cust[] = $customer;
                }
            }

            $cust[0]->notify(new \App\Notifications\Otp($crypt, "text"));
        }


        return $crypt;
    }
}
