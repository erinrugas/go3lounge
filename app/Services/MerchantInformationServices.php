<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 2/10/20
 * Time: 9:13 AM
 */

namespace App\Services;


use App\Contracts\MerchantInformationContract;
use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Services\Utilities\Helper;
use Illuminate\Support\Facades\DB;

class MerchantInformationServices implements MerchantInformationContract
{

    /**
     * @inheritDoc
     */
    public function getCustomerByNumber($mobile)
    {
        $merchantInformations = MerchantInformation::where('deleted_at', null)->get();

        $getMobile = Helper::formatNumber($mobile['contact_number'], $mobile['prefix']);

        $cust = [];

        foreach ($merchantInformations as $key => $merchantInformation) {
            $businessId = Helper::padZero($merchantInformation->business_id, 4);
            config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
            DB::purge('mysql2');
            $customers = Customer::where('mobile_number', $getMobile)->get();
            foreach ($customers as $customer) {
                $cust[] = $customer;
            }
        }

        return $cust;
    }
}
