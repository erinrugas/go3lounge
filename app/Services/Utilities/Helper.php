<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 1/28/20
 * Time: 11:25 AM
 */

namespace App\Services\Utilities;


use App\Contracts\Constant;

class Helper
{

    /**
     * Json response for api request
     *
     * @param $flag Constant::FLAG_SUCCESS || Constant::FLAG_ERROR
     * @param $userMessage user explanation message = Product has been successfully created.
     * @param $internalMessage developer explanation message = Product has been created with id $Product->id
     * @param $code =
     * @return \Illuminate\Http\JsonResponse
     */
    public static function responseJson($flag, $userMessage, $internalMessage, $code)
    {
        return response()->json(["flag" => $flag, "userMessage" => $userMessage, "internalMessage" => $internalMessage], $code);
    }

    /**
     *
     * @param $id
     * @param null $length
     * @return string
     */
    public static function padZero($id, $length = NULL)
    {
        if (!is_null($length)) {
            $id_len = strlen($id);
            $dif = $length-$id_len;

            $pad = [];
            for ($i=0; $i<$dif; $i++) {
                $pad[] = 0;
            }

            array_push($pad, $id);
            $padded = implode('', $pad);

            return $padded;
        }
    }

    /**
     * Retained all number
     * @param $string
     * @return null|string|string[]
     */
    public static function retainedAllNumbers($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^0-9]/', '', $string); // Removes special chars.
    }

    public static function formatNumber($number, $tel_prefix)
    {
        // Allow only Digits, remove all other characters.
        $number = preg_replace("/[^\d]/","",$number);

        // get number length.
        $length = strlen($number);
        switch($tel_prefix){
            case "1":

                // if number = 10
                if($length == 10) {
                    $number = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $number);
                }

                return $number;

                break;

            case "63":

                // if number = 10
                if($length == 10) {
                    $number = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1 $2 $3", $number);
                }

                return $number;

                break;

            default:
                return $number;
        }
    }

}
