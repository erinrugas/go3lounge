<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 1/29/20
 * Time: 11:39 AM
 */

namespace App\Services\Utilities;
use Illuminate\Support\Str;
use phpseclib\Math\BigInteger;

class StrHelper
{
    /**
     * Simple Algo to Encrypt Seeder (I used this for OTP)
     * @param $seed
     * @param int $limit
     * @param string $type
     * @return string
     */
    public static function crypt($seed, $limit, $type = 'str')
    {
        if ($type == 'int') {
            $hayStack = "0123456789";
        } else {
            $hayStack = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }

        srand($seed);

        $str = "";
        $x = 0;

        for (; $x < $limit; $x++) {

            $i = rand(0, 9);
            $str .= $hayStack[$i];
        }

        return $str;
    }

    /**
     * String as int
     * @return BigInteger
     */
    public static function asInt($string)
    {
        $x = new BigInteger(0);

        for ($i = 0; $i < strlen($string); $i++) {
            $r = new BigInteger(31);
            $s = $r->multiply($x);
            $t = new BigInteger(ord($string[$i]));
            $x = $s->add($t);
        }

        return $x;
    }

    /**
     * Encrypt Data
     * @param $data
     * @return false|string
     */
    public static function encrypt($data)
    {
        $key = "g03_chEck1n_s3KretK3y!!!"; // any random string

        $length = strlen($key);
        if ($length < 16)
            $key = str_repeat($key, ceil(16/$length));

        return openssl_encrypt($data, 'BF-ECB', $key);
    }

    /**
     * Decrypt Data
     * @param $data
     * @return false|string
     */
    public static function decrypt($data)
    {
        $key = "g03_chEck1n_s3KretK3y!!!"; // any random string
        $length = strlen($key);

        if ($length < 16)
            $key = str_repeat($key, ceil(16/$length));

        return openssl_decrypt($data, 'BF-ECB', $key);
    }
}
