<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $information = [
            'name'  => request('first_name'). ' ' . request('last_name'),
            'email' => request('email'),
            'contact'   => request('contact_number'),
            'message'   => request('message'),
            'subject'   => request('subject')
        ];
        return $this->from(request('email'))->subject(request('subject').' ('.request('email').')')
            ->markdown('emails.contacts', compact('information'));
    }
}
