<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    /**
     * connection
     * @var string
     */
    protected $connection = "mysql2";

    /**
     * table name
     * @var string
     */
    protected $table = "messages";
}
