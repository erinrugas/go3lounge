<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantContactPerson extends Model
{
    protected $table = 'merchant_contact_person';

    protected $guarded = [];
}
