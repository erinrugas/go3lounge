<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUrl extends Model
{
    protected $connection = "mysql2";

    protected $guarded = [];

    protected $table = "campaign_urls";
}
