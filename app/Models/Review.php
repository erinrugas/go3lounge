<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Review extends Model
{

    use Notifiable;

    /**
     * connection
     * @var string
     */
    protected $connection = 'mysql2';

    protected $table = 'reviews';

    protected $dates = ['created_at', 'updated_at', 'send_date', 'date_reviewed'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

}
