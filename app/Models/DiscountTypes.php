<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountTypes extends Model
{
//    protected $connection = "mysql2";
    protected $table = "discount_types";
    protected $guarded = [];

    const DISCOUNT_TYPE_PERCENTAGE = 'percentage';
    const DISCOUNT_TYPE_PERCENTAGE_INT = 1;
    const DISCOUNT_TYPE_PERCENTAGE_SYMBOL = "%";
    const DISCOUNT_TYPE_AMOUNT = 'amount';
    const DISCOUNT_TYPE_AMOUNT_INT = 2;
    const DISCOUNT_TYPE_AMOUNT_SYMBOL = "$";

}
