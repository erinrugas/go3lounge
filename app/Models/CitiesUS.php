<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CitiesUS extends Model
{
    protected $table = 'us_zip_codes';
    protected $guard = [];

    public function states()
    {
        return $this->hasOne('App\Models\States', 'id', 'state_id');
    }
}
