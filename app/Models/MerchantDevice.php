<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantDevice extends Model
{
    protected $table = 'merchant_devices';
}
