<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    use SoftDeletes;

    protected $connection = "mysql2";
    protected $table = "services";

    protected $guarded = [];

    public function category()
    {
        $this->hasOne('App\Models\ServiceCategories', 'id','service_category_id');
    }
}
