<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageCategory extends Model
{

    const LOW_RATE = 2;
    const HIGH_RATE = 3;

    protected $guarded = [];

    /**
     * connection
     * @var string
     */
    protected $connection = "mysql2";

    /**
     * table name
     * @var string
     */
    protected $table = "message_categories";

    public function message()
    {
        return $this->belongsTo('App\\Models\\Message','id', 'message_category_id');
    }
}
