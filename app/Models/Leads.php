<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $connection = "mysql2";

    protected $guarded = [];

    protected $table = "leads";
}
