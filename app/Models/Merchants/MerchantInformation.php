<?php

namespace App\Models\Merchants;

use Illuminate\Database\Eloquent\Model;

class MerchantInformation extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'merchant_informations';

    protected $guarded = [];

    public $primaryKey  = 'business_id';

    public function timezone(){
        return $this->hasOne('App\Models\Merchants\Timezones', 'id', 'business_timezone');
    }
}
