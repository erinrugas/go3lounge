<?php

namespace App\Models\Merchants;

use Illuminate\Database\Eloquent\Model;

class Timezones extends Model
{
    protected $table = "timezones";
    protected $guard = [];
}
