<?php

namespace App\Models;

use App\Services\Utilities\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{

    use Notifiable;

    protected $guarded = [];

    /**
     * connection
     * @var string
     */
    protected $connection = "mysql2";

    /**
     * table name
     * @var string
     */
    protected $table = "customers";

    /**
     * Infobip
     * @param null $notification
     * @return mixed
     */
    public function routeNotificationForInfoBip($notification)
    {
        return Helper::retainedAllNumbers($this->mobile_number);
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review', 'cus');
    }

}
