<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentService extends Model
{
    protected $connection = "mysql2";
    protected $table = "appointment_services";
    protected $guarded = [];

    public $timestamps = false;

    public function services(){
        return $this->belongsTo('App\Models\Appointments', 'appointment_id', 'id');
    }
}
