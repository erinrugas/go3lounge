<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * connection
     * @var string
     */
    protected $connection = 'mysql2';

    protected $guarded = [];

    protected $table = "campaigns";
}
