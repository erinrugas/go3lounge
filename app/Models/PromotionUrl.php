<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionUrl extends Model
{
    /**
     * connection
     * @var string
     */
    protected $connection = 'mysql2';

    protected $guarded = [];

    protected $table = "promotion_urls";
}
