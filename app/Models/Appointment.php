<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{

    /**
     * table name
     * @var string
     */
    protected $table = 'appointments';

    /**
     * connection
     * @var string
     */
    protected $connection = "mysql2";

    /**
     * guarded
     * @var array
     */
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    public function review()
    {
        return $this->hasOne('App\Models\Review', 'appointment_id', 'id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Services', 'App\Models\AppointmentService', 'appointment_id', 'service_id')->withPivot(['start_time','end_time'])->withTrashed();
    }

}
