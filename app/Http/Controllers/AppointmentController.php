<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Merchants\MerchantInformation;
use App\Services\Utilities\Helper;
use App\Services\Utilities\StrHelper;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($bsid, $id)
    {
        $businessId = Helper::padZero($bsid, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
        $appointment = Appointment::with('customer','services')->where('id', StrHelper::decrypt($id))->firstOrFail();
        $merchant = MerchantInformation::where('business_id', $bsid)->firstOrFail();

        return view('appointments.index', compact('merchant', 'appointment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancel($bsid, $id)
    {
        $businessId = Helper::padZero($bsid, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);

        $appointment = Appointment::with('customer','services')->where('id', StrHelper::decrypt($id))->firstOrFail();

        $appointment->checkout_time = null;
        $appointment->status_id = 4;
        $appointment->save();

        return redirect()->route('appointment.index', ['bsid' => $bsid, 'id' => $id]);
    }

    public function confirmed($bsid, $id)
    {
        $businessId = Helper::padZero($bsid, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);

        $appointment = Appointment::with('customer','services')->where('id', StrHelper::decrypt($id))->firstOrFail();
        $appointment->checkout_time = null;
        $appointment->status_id = 5;
        $appointment->save();

        return redirect()->route('appointment.index', ['bsid' => $bsid, 'id' => $id]);
    }
}
