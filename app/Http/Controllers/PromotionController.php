<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Models\Promotion;
use App\Models\PromotionUrl;
use App\Services\Utilities\Helper;
use Illuminate\Http\Request;

class PromotionController extends Controller
{

    public function index()
    {
        return view('promotions.index');
    }

    public function show($bsid, $codeUrl)
    {
        $code = explode('-', $codeUrl);
        $businessId = Helper::padZero($bsid, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
        $merchant = MerchantInformation::where('business_id', $businessId)->first();

        $promotion = Promotion::where('code', $code[1])->firstOrFail();
        $promotionUrl = PromotionUrl::where('url', $codeUrl)->firstOrFail();
        $customer = Customer::where('id', $code[2])->firstOrFail();

        $promotionUrl->viewed = $promotionUrl->viewed + 1;
        $promotionUrl->save();

        return view('promotions.show', compact('promotionUrl', 'promotion', 'customer'));
    }

}
