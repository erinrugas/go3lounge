<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 1/30/20
 * Time: 11:44 AM
 */

namespace App\Http\Controllers\Traits;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait AuthUsers
{
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logOut(Request $request)
    {
//        $this->guard()->logout();

        $request->session()->forget('customer');

        $request->session()->invalidate();

        $request->session()->regenerate();



    }

    /**
     * Username
     * @param $request
     * @return mixed
     */
    public function username(Request $request)
    {
        return [
            'prefix'    => $request->prefix,
            'contact_number'    => $request->contact_number
        ];
    }

    /**
     * Auth guard
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
