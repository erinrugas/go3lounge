<?php

namespace App\Http\Controllers;

use App\Contracts\Constant;
use App\Contracts\MerchantInformationContract;
use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Services\Utilities\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{

    /**
     * Merchant Information
     * @var
     */
    public $merchantInformation;

    public function __construct(MerchantInformationContract $merchantInformation)
    {
        $this->merchantInformation = $merchantInformation;
    }

    /**
     * List of Stores
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $merchantInformations = MerchantInformation::where('deleted_at', null)->get();

        foreach ($merchantInformations as $key => $merchantInformation) {
            $businessId = Helper::padZero($merchantInformation->business_id, 4);
            config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
            DB::purge('mysql2');
        }
        return view('stores.index', compact('merchantInformations'));
    }

    public function search()
    {
        dd(request('search'));
    }

}
