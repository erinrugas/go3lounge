<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Constant;
use App\Contracts\MerchantInformationContract;
use App\Contracts\OTP;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AuthUsers;
use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Providers\RouteServiceProvider;
use App\Services\Utilities\Helper;
use App\Services\Utilities\StrHelper;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Http\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

//    use AuthenticatesUsers;
    use AuthUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Merchant Information
     * @var
     */
    public $merchantInformation;

    /**
     * OTP
     * @var OTP
     */
    public $otp;

    /**
     * LoginController constructor.
     * @param OTP $otp
     * @param MerchantInformationContract $merchantInformation
     */
    public function __construct(OTP $otp, MerchantInformationContract $merchantInformation)
    {
        $this->middleware('guest')->except('logout');
        $this->otp = $otp;
        $this->merchantInformation = $merchantInformation;
    }

    public function logout(Request $request)
    {

        if ($request->isMethod('POST')) {
            $request->session()->forget('customer');
            $request->session()->invalidate();
            $request->session()->regenerate();
            return redirect()->route('home');
        }
        abort(404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if ($request->isMethod('POST')) {
            $otp = $this->otp($request, false);
            $decryptOTP = StrHelper::decrypt($otp);

            if ($decryptOTP == request('otp')) {
                session(['customer' => $this->username($request)]);
                return Helper::responseJson(Constant::FLAG_SUCCESS, "Accepted Code", $otp, Constant::HTTP_RESPONSE_SUCCESS);
            } else {
                return Helper::responseJson(Constant::FLAG_ERROR, "Invalid Code", request('otp'), Constant::HTTP_RESPONSE_UN_PROCESS);
            }
        }
        abort(404);



    }

    /**
     * Generate OTP
     * @param Request $request
     * @param OTP $otp
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateOTP(Request $request)
    {
        $username = $this->username($request);

        $getCustomers = $this->merchantInformation->getCustomerByNumber($username);

        if (count($getCustomers) == 0) {
            return Helper::responseJson(Constant::FLAG_ERROR, "Mobile number not exist.", request('contact_number'), Constant::HTTP_RESPONSE_UN_PROCESS);
        }

        $encryptOTP = $this->otp($request, true);
        $decryptOTP = StrHelper::decrypt($encryptOTP);

        return Helper::responseJson(Constant::FLAG_SUCCESS, "OTP Sent", $encryptOTP, Constant::HTTP_RESPONSE_SUCCESS);

    }

    /**
     * One Time Password
     * @param $request
     * @param bool $isSend
     * @return false|string
     */
    public function otp($request, $isSend = false)
    {
        $otp = $this->otp->send($this->username($request), $isSend);
        return StrHelper::encrypt($otp);
    }

}
