<?php

namespace App\Http\Controllers;

use App\Contracts\Constant;
use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Models\Review;
use App\Services\Utilities\Helper;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReviewController extends Controller
{

    /**
     * Main Page
     * @param $businessId
     * @param $rate_code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($businessId, $rate_code)
    {
        $businessId = Helper::padZero($businessId, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
        $merchant = MerchantInformation::where('business_id', $businessId)->firstOrFail();

        $review = Review::where([
            'link_id' =>  $rate_code,
        ])->firstOrFail();

        if ($review->status == 0) {
            return view('reviews.index', compact('review', 'merchant'));
        }

        return redirect()->route('review.show', [$businessId, $rate_code]);

    }

    /**
     * Send Review
     * @param Request $request
     * @param $businessId
     * @param $rate_code
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $businessId, $rate_code)
    {
        $businessId = Helper::padZero($businessId, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);

        //set timezone
        $merchant = MerchantInformation::where('business_id', $businessId)->with('timezone')->first();

        if($merchant->timezone()->count() > 0){
            $tz = $merchant->timezone->timezone;
            date_default_timezone_set($tz);
        }

        $review = Review::where('link_id', $rate_code)->first();
        $review->rating = request('rate');
        $review->status = ( request('rate') < 5 ) ? 1 : 2;
        $review->comments = request('comments');
        $review->date_reviewed = Carbon::now()->format('Y-m-d H:i:s');
        $review->platform = request('platform');
        $review->save();

        $platform = ['facebook', 'yelp', 'google'];

        if (request('rate') >= 4) {
            if (is_null($merchant->business_place_id) && is_null($merchant->business_yelp_id) && is_null($merchant->business_fb_id)) {
                session()->flash('message', 'Thank you for your review.');
                return redirect()->route('review.show',[$businessId, $rate_code]);
            }

            if (!in_array($request->platform, $platform)) {
                return Helper::responseJson(Constant::FLAG_SUCCESS, 'Thank you for your reviews.', 'Review Success', Constant::HTTP_RESPONSE_SUCCESS);
            }
        } else {
            session()->flash('message', 'Thank you for your review.');
            return redirect()->route('review.show',[$businessId, $rate_code]);
        }
    }

    /**
     * Show Merchant with Review
     * @param $bid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($bid, $rate_code)
    {
        config(["database.connections.mysql2.prefix" => "{$bid}_"]);
        $merchantInformation = MerchantInformation::where('business_id', $bid)->firstOrFail();
        
        $customers = Customer::all();
        $rating = Review::with('customer')->where([
            'link_id' => $rate_code
        ])->firstOrFail();
        
        if ($rating->status != 0) {
            $yelps = null;
            $googleReviews = null;
            
            // if (!is_null($merchantInformation)) {
            //     $client = new Client();
            //     if (!is_null($merchantInformation->business_yelp_id)) {
            //         $yelpReviews = $client->request("GET", "https://api.yelp.com/v3/businesses/$merchantInformation->business_yelp_id/reviews", [
            //             'headers'        => ['Authorization' => 'Bearer gE-koy4heSldAnDCRMfhDYVhrSihLpU4HmMvzrQ2r9T0IceZ8SZm6eOeIFt4KJrrGCo45tQq7qlOOhCB3XbQWqdlOAyUpPcJcbsKDWkYbkVpfNQrTKsSkwwxA1BBXnYx'],
            //             'http_errors' => false
            //         ]);
            //         $yelps = $yelpReviews;
            //     }
            //     if (!is_null($merchantInformation->business_place_id)) {
            //         $googleReviews = $client->get("https://maps.googleapis.com/maps/api/place/details/json?placeid=$merchantInformation->business_place_id&key=".env('GOOGLE_API_KEY'));
            //         $googleReviews = json_decode($googleReviews->getBody(), TRUE);
            //     }
            // }

            return view('reviews.show',compact('merchantInformation', 'rating', 'customers', 'yelps', 'googleReviews'));
        } else {
            return $this->index($bid, $rate_code);
        }
    }

}
