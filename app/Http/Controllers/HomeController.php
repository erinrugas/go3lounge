<?php

namespace App\Http\Controllers;

use App\Contracts\OTP;
use App\Models\Appointment;
use App\Models\Customer;
use App\Models\Merchants\MerchantInformation;
use App\Models\Review;
use App\Services\Utilities\Helper;
use App\Services\Utilities\StrHelper;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpseclib\Math\BigInteger;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @param OTP $otp
     */
//    public function __construct(OTP $otp)
//    {
////        $this->middleware('auth');
//        $this->otp = $otp;
//
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     $client = new Client();
    //     $merchantInformations = MerchantInformation::where('deleted_at', null)->get();

    //     $merchantStores = [];
    //     $recentStoresWithReview = [];
    //     foreach ($merchantInformations as $merchantInformation) {
    //         config(["database.connections.mysql2.prefix" => \App\Services\Utilities\Helper::padZero($merchantInformation->business_id, 4)."_"]);
    //         \DB::purge('mysql2');

    //         $googleApi = $client->get("https://maps.googleapis.com/maps/api/place/details/json?placeid=$merchantInformation->business_place_id&key=".env('GOOGLE_API_KEY'));
    //         $googleApi = json_decode($googleApi->getBody(), TRUE);
    //         rsort($merchantStores);
    //         rsort($recentStoresWithReview);

    //         $customers = \App\Models\Customer::where('mobile_number', Helper::formatNumber(session('customer')['contact_number'], session('customer')['prefix']))->get();

    //         $reviews = Review::where('rating','!=', null)->get();
    //         foreach ($customers as $customer) {

    //             //VISITED STORES
    //             foreach (\App\Models\Appointment::with(['customer' => function( $query ) use ($customer) {
    //                 $query->where('mobile_number', $customer->mobile_number);
    //             }])->with(['review' => function($q) use ($customer) {
    //                 $q->where('status', '!=', 0)->where('customer_id', $customer->id);
    //             }])->get()->groupBy('customer_id') as $key => $appointment) {

    //                 $review_count = $reviews->sum('rating');
    //                 $merchantStores[] = [
    //                     'count' => $appointment[0]->count(),
    //                     'customer' => $appointment[0]->customer,
    //                     'merchant' => $merchantInformation,
    //                     'total_review'  => $review_count / count($reviews),
    //                     'google'    => (array_key_exists('result', $googleApi)) ? $googleApi['result'] : [],
    //                 ];

    //                 $recentStoresWithReview[] = [
    //                     'created_at' => Carbon::parse($appointment[0]->created_at)->format('Y-m-d H:i:s'),
    //                     'customer' => $appointment[0]->customer,
    //                     'merchant' => $merchantInformation,
    //                     'total_review'  => $review_count / count($reviews),
    //                     'google'    => (array_key_exists('result', $googleApi)) ? $googleApi['result'] : [],
    //                 ];
    //             }
    //         }
    //     }

    //     $recentStores = $recentStoresWithReview;
    //     $stores = $merchantStores;

    //     return view('home.index', compact('stores', 'recentStores'));
    // }

    public function index() 
    {
        return view('home.index2');
    }
}
