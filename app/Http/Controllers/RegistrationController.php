<?php

namespace App\Http\Controllers;

use App\Models\CitiesPH;
use App\Models\CitiesUS;
use App\Models\Countries;
use App\Models\Devices;
use App\Models\DiscountTypes;
use App\Models\MerchantContactPerson;
use App\Models\MerchantDevice;
use App\Models\States;
use App\Models\Timezones;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\Register;
use App\Http\Requests\Registration;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;


class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Countries::whereIn('iso_code_2', ['US'])->get();
        $states = States::where('country_id', '1')->get();
        $cities = CitiesUS::where('state_id', '1')->get();
        $timezones = Timezones::all();
        return view('register.index', compact('countries', 'states', 'cities', 'timezones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Registration $request)
    {
        DB::transaction(function () use ($request) {
            $leads = new Register;
            $leads->business_name = $request->business_name;
            $leads->business_owner = $request->business_owner_first_name .' '.$request->business_owner_last_name;
            $leads->business_category = $request->business_category;
            $leads->business_street = $request->business_street;
            $leads->business_city = $request->business_city;
            $leads->business_state = $request->business_state;
            $leads->business_timezone = $request->business_timezone;
            $leads->business_zip = explode('_',$request->business_zipcode)[0];
            $leads->business_email = $request->business_email;
            $leads->business_prefix = is_null($request->business_prefix) ? 1 : $request->business_prefix;
            $leads->business_contact = $request->business_contact;
            $leads->business_fax = $request->business_fax;
            $leads->business_corporate = $request->business_corporate;
            $leads->birth_date = $request->birth_date;
            $leads->google_id = $request->google_id;
            $leads->yelp_id = $request->yelp_id;
            $leads->facebook_page = $request->facebook_page;
            $leads->logo_preference = $request->logo_preference;
            $leads->logo_description = $request->logo_description;
            $leads->cover_preference = $request->cover_preference;
            $leads->cover_description = $request->cover_description;
            $leads->marketing_materials = $request->marketing_materials;
            $leads->phone_1 = $request->business_phone_1;
            $leads->phone_2 = $request->business_phone_2;

            if ($request->hasFile('business_permit')) {
                $file = $request->file('business_permit');
                $extension = $file->getClientOriginalExtension();
                $filename = "business-permit.{$extension}";
                $business_permit_uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                $leads->business_permit = str_replace('public', 'storage', $business_permit_uploaded_file);
            } else {
                $leads->business_permit = "";
            }


            $leads->business_tax_id = "";

            $leads->privacy = request('privacy_url');
            $leads->terms_and_conditions = request('terms_and_condition');

            $leads->save();

            if ($request->hasFile('merchant_setting_file')) {
                $file = $request->file('merchant_setting_file');
                $extension = $file->getClientOriginalExtension();
                $filename = "merchant-settings.{$extension}";
                $merchant_setting_uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                $leads->merchant_settings = str_replace('public', 'storage', $merchant_setting_uploaded_file);
            }

            if($request->hasFile('business_logo')){
                $file = $request->file('business_logo');
                $extension = $file->getClientOriginalExtension();
                $filename = "business-logo.{$extension}";
                $business_logo_uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                $leads->business_logo = str_replace('public', 'storage', $business_logo_uploaded_file);
            }

            if($request->hasFile('business_cover')){
                $file = $request->file('business_cover');
                $extension = $file->getClientOriginalExtension();
                $filename = "business-cover.{$extension}";
                $business_cover_uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                $leads->business_cover = str_replace('public', 'storage', $business_cover_uploaded_file);
            }

            if($request->hasFile('service_menu')){
                foreach ($request->file('service_menu') as $key => $file) {
                    $extension = $file->getClientOriginalExtension();
                    $additional = $key + 1;
                    $filename = "service-menu-{$additional}.{$extension}";
                    $service_uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                    $leads->service_menu = str_replace('public', 'storage', $service_uploaded_file);
                }
            }



            if ($request->hasFile('merchant_promotion_and_campaigns')) {
                $file = $request->file('merchant_promotion_and_campaigns');
                $extension = $file->getClientOriginalExtension();
                $filename = "promotion-and-campaign.{$extension}";
                $promotion_uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                $leads->promotion_and_campaigns = str_replace('public', 'storage', $promotion_uploaded_file);
            }

            if ($request->hasFile('business_tax_id')) {
                $file = $request->file('business_tax_id');
                $extension = $file->getClientOriginalExtension();
                $filename = "business-tax-id.{$extension}";
                $uploaded_file = $file->storeAs("public/leads/{$leads->id}", $filename);
                $leads->business_tax_id = str_replace('public', 'storage', $uploaded_file);
            }
            if (request('manage_device') == 'on') {
                $this->devices($request, $leads->id);
            }

            $this->contactPerson($leads->id);

            $goetuLeads = $this->forwardLeads($request, $leads->business_logo);
            $leads->goetu_leads_id = json_decode($goetuLeads)->data->partnerInfo->id;
            $leads->save();

            return 'true';
        });
    }

    public function forwardLeads($request, $logo)
    {
        if ( $request->business_category == "1" || $request->business_category == "2" || $request->business_category == "3" ) {
            $category = '7298';//business type in goetu for salons / dentist / spa
        }

        $state = States::where('name', $request->business_state)->first();

        $client = new Client(['base_uri' => config('app.goetu_api'), 'http_errors' => true]);
        $header = [
            'Authorization' => 'Bearer 5jkLRFUikbQcTvumTEoSB425z08kGGXo',
            'Accept' => 'application/json',
            'Content-Type' =>  'application/json',
            'username'  => 'C100001',
            'email' => 'go3dev@go3solutions.com'
        ];

        $response = $client->post('api/register_leads',
            [
                'headers'   => $header,
                'json'  => [
                    "logo" => config('app.merchant_url').'/'.$logo,
                    'products'  => 362,
                    'businessInfo' => [
                        'businessName'  =>  $request->business_name,
                        'businessOwnerFirstName'    => $request->business_owner_first_name,
                        'businessOwnerLastName'    => $request->business_owner_last_name,
                        'businessCategory'    => $category,
                    ],

                    'dbaAddress' => [
                        'dbaBusinessAddress' => $request->business_street,
                        'dbaBusinessAddress2' => null,
                        'dbaZip'    => (int) explode('_',$request->business_zipcode)[0],
                        'dbaState'  => $state->abbr,
                        'dbaCity'   => $request->business_city,
                    ],

                    'businessContact'   => [
                        'phone1'    => $request->business_phone_1,
                        'phone2'    => $request->business_phone_2,
                        'mobile_number' => $request->business_contact,
                        'email' => $request->business_email,
                    ],

                    'businessContactPerson' => [
                        'title' => ucfirst($request->contact_person_title),
                        'firstName' => $request->contact_person_first_name,
                        'lastName'  => $request->contact_person_last_name,
                        'middleName'    => $request->contact_person_middle_initial,
                        'phone1'    => $request->contact_person_phone_1,
                        'phone2'    => $request->contact_person_phone_2,
                        'mobile_number' => $request->contact_person_mobile_number,
                        'fax'   => $request->contact_person_fax,
                        'email' => $request->contact_person_email
                    ]
                ],
            ]
        );
        return $response->getBody();
    }

    public function contactPerson($leadId)
    {
        MerchantContactPerson::create([
            'leads_id'  => $leadId,
            'title' => request('contact_person_title'),
            'first_name'    => request('contact_person_first_name'),
            'last_name' => request('contact_person_last_name'),
            'middle_initial'    => request('contact_person_middle_initial'),
            'mobile_number' => request('contact_person_mobile_number'),
            'phone_1'    => request('contact_person_phone_1'),
            'phone_2'   => request('contact_person_phone_2'),
            'fax'   => request('contact_person_fax'),
            'email' => request('contact_person_email')
        ]);
    }

    public function devices(Request $request, $leadId)
    {
        $device  = new MerchantDevice();

        if(request()->hasFile('logo')){
            $file = request()->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filename = "device-logo.{$extension}";
            $uploaded_file = $file->storeAs("public/leads/$leadId", $filename);
        }

        $device->name = request('device_name');
        $device->lead_id = $leadId;
        $device->theme_color1 = request('theme_color1');
        $device->theme_color2 = request('theme_color2');
        $device->font_color1 = request('font_color1');
        $device->font_color2 = request('font_color2');
        $device->btn_color1 = request('btn_color1');
        $device->btn_color2 = request('btn_color2');
        $device->pin = request('pin');
        $device->slide_duration = request('slide_duration');
        $device->verkey = Str::random(12);
        $device->mode_id = 1;
        $device->logo = isset($uploaded_file) ? str_replace('public', 'storage', $uploaded_file)
            : 'images/logo/defaultlogo.png';

        $device->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cacheCountryStateCities()
    {
        $this->countryState();
        $this->countryUSCities();
//        $this->countryPHCities();
    }

    public function countryState()
    {
        $seconds = "2678400";

        // load us state to cache data
        Cache::remember('state', $seconds, function () {
            return States::all();
        });
    }

    public function countryUSCities()
    {
        $seconds = "2678400";
        $states = cache('state');

        foreach($states as $state) {
            $id = $state->id;

            if ($state->country_id == '1') {
                // clear existing
                Cache::forget('us_cities_'.$id);

                // load us state to cache data
                Cache::remember('us_cities_'.$id, $seconds, function () use ($id) {
                    return DB::table('us_zip_codes')->select(DB::raw('DISTINCT(city)'))->where('state_id', $id)->get();
                });
            }
        }
    }

    public function countryPHCities()
    {
        $seconds = "2678400";
        $states = cache('state');

        foreach($states as $state) {
            $id = $state->id;

            if ($state->country_id == '2') {
                // clear existing
                Cache::forget('ph_cities_'.$id);

                // load us state to cache data
                Cache::remember('ph_cities_'.$id, $seconds, function () use ($id) {
                    return DB::table('ph_zip_codes')->select(DB::raw('DISTINCT(city)'))->where('state_id', $id)->get();
                });
            }
        }
    }

    public function states($id)
    {
        $states = cache('state');
        $newStates = [];

        foreach($states as $state) {
            if ($state->country_id == $id) {
                $newStates[] = $state;
            }
        }

        return response()->json($newStates);
    }

    public function cities($id, $country_id = 1)
    {
        switch($country_id) {
            case '1':
                $name = 'us_cities_'.$id;
                $cities = cache($name);
                break;

            case '2':
                $name = 'ph_cities_'.$id;
                $cities = cache($name);
                break;
        }
        return response()->json($cities);
    }

    public function cityLookup(Request $request)
    {
        $search = $request->search;
        $cities = CitiesUS::where('zip_code','LIKE','%'.$search.'%')->with('states')->get();

        $response = [];
        foreach($cities as $city){
            $response[] = [
                "id" => $city->zip_code."_".$city->city,
                "text" => $city->zip_code." | ".$city->states->name." | ". $city->city,
                "zip" => $city->zip_code,
                "city" => $city->city,
                "country" => $city->country_id,
                "state" => $city->states->name,
            ];
        }

        echo json_encode($response);

        exit;
    }
}
