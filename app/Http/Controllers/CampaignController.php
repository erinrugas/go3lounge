<?php

namespace App\Http\Controllers;

use App\Models\Leads;
use App\Models\Campaign;
use App\Models\CampaignUrl;
use App\Models\Merchants\MerchantInformation;
use App\Services\Utilities\Helper;
use Illuminate\Http\Request;

class CampaignController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('campaign.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bsid, $codeUrl)
    {
        $code = explode('-', $codeUrl);

        $businessId = Helper::padZero($bsid, 4);
        config(["database.connections.mysql2.prefix" => "{$businessId}_"]);
        $merchant = MerchantInformation::where('business_id', $businessId)->first();

        $campaign = Campaign::where('code', $code[1])->firstOrFail();
        $campaignUrl = CampaignUrl::where('url', $codeUrl)->firstOrFail();
        $lead = Leads::where('id', $code[2])->firstOrFail();

        $campaignUrl->viewed = 1;
        $campaignUrl->save();

        $promo_from = date('F d, Y', strtotime($campaign->date_from));
        $promo_until = date('F d, Y', strtotime($campaign->date_to));

        return view('campaign.show', [
            'url' => $campaignUrl,
            'campaign' => $campaign,
            'lead' => $lead,
            'from' => $promo_from,
            'until' => $promo_until
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
