<?php

namespace App\Http\Controllers;

use App\Mail\SendContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function index()
    {
        return view('contacts.index');
    }

    public function send(Request $request)
    {
        $this->validator();

        Mail::to(config('app.contact_us_mail_to'))->send(new SendContactUs($request));
        session()->flash('success', 'Your message has been sent. We will keep you posted.');
        return response()->json(['success' => true]);
    }

    public function validator()
    {
        $validator = Validator::make(request()->all(), [
            'first_name'    => 'required',
            'last_name'    => 'required',
            'subject'    => 'required',
            'email'    => 'required|email',
            'contact_number'    => 'required',
            'message'    => 'required',
        ]);

        return $validator->validate();
    }
}
