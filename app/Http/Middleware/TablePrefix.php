<?php

namespace App\Http\Middleware;

use App\Services\Utilities\Helper;
use Closure;

class TablePrefix
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $connection = "mysql2";
        $business_id = Helper::padZero(Auth::user()->business_id, 4);
        config(["database.connections.$connection.prefix" => $business_id . "_"]);

        return $next($request);
    }
}
