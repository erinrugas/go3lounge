<?php

namespace App\Http\Requests;

use App\Models\Merchants\MerchantInformation;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class Registration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $formValidation = [
            'business_name' => 'required',
            'business_owner_first_name' => 'required',
            'business_owner_last_name' => 'required',
            'business_category' => 'required',
            'business_street' => 'required',
            'business_city' => 'required',
            'business_state' => 'required',
            'business_zipcode' => 'required',
            'business_email' => 'required|email|regex:/(\.\w+(\.[a-z]+)?)$/|max:60',
            'business_contact' => 'required|min:12',
            'business_phone_1'  => 'required|min:12',
            'business_fax'  => 'nullable|min:12',
//            'birth_date' => 'bail|required|max:20',
            'business_permit'   => 'required',
            'business_tax_id'   => 'required',
            'business_timezone'   => 'required',

            //contact person
            'contact_person_title'  => 'required',
            'contact_person_first_name' => 'required',
            'contact_person_last_name'  => 'required',
            'contact_person_mobile_number'  => 'required|min:12',
            'contact_person_phone_1'    => 'required|min:12',
            'contact_person_email'    => 'required|email|unique:merchant_contact_person,email',

            //device
            'device_name'   => 'required_if:manage_device,on',
            'pin'   => 'required_if:manage_device,on',
            'slide_duration'   => 'required_if:manage_device,on',

            //
            'terms' => 'required',
            'privacy'   => 'required'
        ];

        if ($this->input('business_corporate_yesno') == 'yes' && !is_null($this->input('business_corporate'))) {
            $formValidation['business_corporate'] = 'email|regex:/(\.\w+(\.[a-z]+)?)$/';
        }

        return $formValidation;
    }

    public function messages()
    {
        return [
            'business_contact.min' => 'The business contact must be at least 10 characters.',
            'device_name.required_if'   => 'The device name is required',
            'pin.required_if'   => 'The pin is required',
            'slide_duration.required_if'   => 'The slide duration is required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $merchantInformation = MerchantInformation::where('business_email', request('business_email'))->first();
            if (!is_null($merchantInformation)) {
                $validator->errors()->add('business_email', 'The email field is already exists.');
            }
        });
    }
}
