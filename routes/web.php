<?php


//Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

// Registration Route
Route::get('/register', 'RegistrationController@create')->name('register.index');
Route::post('/register/store', 'RegistrationController@store')->name('register.store');

Route::get('/promotions/{bsid}/{code}', 'PromotionController@show')->name('promotions.show');

Route::get('/stores', 'StoreController@index')->name('stores.index');
Route::get('/stores/search', 'StoreController@search')->name('stores.search');

Route::get('/profile', 'UserController@index')->name('profile.index');

Route::get('/campaigns/{bsid}/{code}', 'CampaignController@show')->name('campaign.index');

Route::get('/reviews/{business_id}/{rate_code}', 'ReviewController@index')->name('review.index');
Route::put('/reviews/{business_id}/{rate_code}', 'ReviewController@update')->name('review.update');
Route::get('/reviews/{business_id}/{rate_code}', 'ReviewController@show')->name('review.show');

Route::prefix('otp')->group(function() {
    Route::get('/get', 'Auth\\LoginController@generateOTP')->name('get.otp');
//    Route::post('/resend', 'Auth\\LoginController@resendCode')->name('resend.otp');
});

Route::get('/appointments/{bsid}/{id}', 'AppointmentController@index')->name('appointment.index');
Route::post('/appointment/cancel/{bsid}/{id}', 'AppointmentController@cancel')->name('appointment.cancel');
Route::post('/appointment/confirmed/{bsid}/{id}', 'AppointmentController@confirmed')->name('appointment.confirmed');

Route::any('/login', 'Auth\\LoginController@login')->name('login');
Route::any('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/contact-us', 'ContactController@index')->name('contact.index');
Route::post('/contact-us', 'ContactController@send')->name('contact.send');

// Route State and City
Route::get('/counrty/cacheCountryStateCities', 'RegistrationController@cacheCountryStateCities');
Route::get('/cities/get/{id}/{country}', 'RegistrationController@cities');
Route::get('/states/get/{id}', 'RegistrationController@states');

// Route post State and City
Route::post('/cities/all', 'RegistrationController@cityLookup');

Route::get('/privacy', 'PrivacyController@index')->name('privacy.index');
Route::get('/terms-and-conditions', 'TermsAndConditionsController@index')->name('terms.index');
