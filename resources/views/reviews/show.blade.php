@extends('layouts.review')

@section('scripts')
    <script>

        $('.kv-ltr-theme-fas-star, .with-num-review').rating({
            hoverOnClear: false,
            theme: 'krajee-fas',
            containerClass: 'is-star',
            min: 0, max: 5, step: 0.1, stars: 5,
            displayOnly: true,
            showClear: false,
            showCaption: false,
        });

        $('.fas-star-wo-caption').rating({
            hoverOnClear: false,
            theme: 'krajee-fas',
            containerClass: 'is-star',
            min: 0, max: 5, step: 0.1, stars: 5,
            displayOnly: true,
            showClear: false,
            showCaption: false
        });
    </script>
@endsection

@section('content')
    @if(!is_null($merchantInformation->business_cover_img ))
        @php( $bgimage = config('app.merchant_url').'/'.$merchantInformation->business_cover_img )
    @else
        @php( $bgimage = '/storage/img/bg.jpg' )
    @endif
    <div class="bg-image" style="background-image: url('{{ $bgimage }}');"> </div>
    
    <div class="container bg-white p-0">
        <div class="container-fluid">
            <div class="row">
                <div class="card-img-top banner-image">
                    <img class="conver-image" src="/storage/img/cover4.jpg">
                </div>
            </div>

            <div class="row bg-white">
                <div class="p-3 border-top">
                    <h1 class="text-left font-italic business-name m-0">{{ $merchantInformation->business_name }}</h1>
                </div>
                
                <div class="col-lg-12 p-3 border-top">
                    <a class="business-phone" href="tel:{{ $merchantInformation->business_phone }}">
                        <div class="row" data-phone="{{ $merchantInformation->business_phone }}">
                            <div class="col-3 text-20"><span class="link-icon-circle"><i class="fas fa-phone text-blue"></i></span></div>
                            <div class="col-9 text-20 mt-1 pl-0"><p>{{ $merchantInformation->business_phone }}</p></div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-12 p-3 border-top">
                    <a  class="business-address" href="https://maps.google.com/?q={{ $merchantInformation->business_address  }}" target="_blank">
                        <div class="row" data-address="{{ $merchantInformation->business_address }}">
                            <div class="col-3 text-20"><span class="link-icon-circle"><i class="fas fa-map-marker-alt text-blue"></i></span></div>
                            <div class="col-9 text-20 mt-1 pl-0"><p>{{ $merchantInformation->business_address }}</p></div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-12 p-3 border-top">
                    <a class="business-email" href="{{ $merchantInformation->business_email }}" >
                        <div class="row" data-email="{{ $merchantInformation->business_email }}">
                            <div class="col-3 text-20"><span class="link-icon-circle"><i class="far fa-envelope text-blue"></i></span></div>
                            <div class="col-9 text-20 mt-1 pl-0">{{ $merchantInformation->business_email }}</div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-12 merchant-footer bg-dark py-3">
                    <img src="/storage/logo/logo-white.png" width="200px">
                </div>
            </div>
        </div>
    </div>
@endsection
