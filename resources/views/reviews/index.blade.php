@extends('layouts.review')

@section('content')
    @if(!is_null($merchant->business_cover_img ))
        @php( $bgimage = config('app.merchant_url').'/'.$merchant->business_cover_img )
    @else
        @php( $bgimage = '/storage/img/bg.jpg' )
    @endif
    <div class="bg-image" style="background-image: url('{{ $bgimage }}');"> </div>
    <div id="formContent">
    @if (session()->has('message'))
        <div>
            <h3 class="font-weight-bold">{{ session('message') }}</h3>
        </div>
    @else
        <div>
            <h3 class="font-weight-bold mt-5">Rate your experience for <br> {{ $merchant->business_name }}</h3>
        </div>
        <form method="POST" action="{{ route('review.update',[request()->segments()[1], request()->segments()[2]]) }}">
            @csrf
            @method("PUT")
            <div class="form-group">
                <input id="store-rate" name="rate" class="kv-ltr-theme-fas-star rating-loading"  dir="ltr" data-size="md">
                <input type="hidden" name="platform" value="go3checkin">
            </div>

            <div class="form-group" id="comment-form" >
                <hr>
                <div>
                    <h5 class="font-weight-bold ">Leave Comments or Suggestions</h5>
                </div>
                <textarea name="comments" class="form-control  mt-4" id="comments" cols="30" placeholder="Comments / Suggestions" rows="5"></textarea>
            </div>

            <div class="form-group d-none" id="social-review">
                <hr>
                @if (is_null($merchant->business_place_id) && is_null($merchant->business_yelp_id) && is_null($merchant->business_fb_id))
                <div>
                    <h5 class="font-weight-bold ">Submit review to any of the platform below.</h5>
                </div>
                @else
                    <div>
                        <h5 class="font-weight-bold ">Leave Comments or Suggestions</h5>
                    </div>
                @endif

                <div id="checker"
                    @if (is_null($merchant->business_place_id) && is_null($merchant->business_yelp_id) && is_null($merchant->business_fb_id))
                        data-platform-checker="0"
                    @else
                        data-platform-checker="1"
                    @endif
                >
                </div>

                @if (!is_null($merchant->business_place_id))
                <div class="google-review mt-4">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{ url('reviews/google-review.png') }}" alt="Google Review">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-purple text-uppercase btn-xl mt-3 w-50"
                                    id="google-review"
                                    data-google-pid="{{ $merchant->business_place_id }}"
                                    data-bid="{{ request()->segments()[1] }}"
                                    data-lid="{{ request()->segments()[2] }}">
                                Rate in Google
                            </button>
                        </div>
                    </div>
                </div>
                @endif

                @if (!is_null($merchant->business_yelp_id))
                <div class="yelp-review mt-4">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{ url('reviews/yelp-review.png') }}" alt="Yelp Review">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-purple text-uppercase btn-xl mt-3 w-50" id="yelp-review"
                                    data-bid="{{ request()->segments()[1] }}" data-lid="{{ request()->segments()[2] }}"
                                    data-yid="{{ $merchant->business_yelp_id }}"
                                    >
                                Rate in Yelp
                            </button>
                        </div>
                    </div>
                </div>
                @endif

                @if (!is_null($merchant->business_fb_id))
                <div class="facebook-review mt-4">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="{{ url('reviews/facebook-review.png') }}" class="w-100" style="max-width:350px;" alt="Facebook Review">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-purple text-uppercase btn-xl mt-3 w-50"
                                    id="facebook-review" data-fb-review="{{ $merchant->business_fb_id }}"
                                    data-bid="{{ request()->segments()[1] }}"
                                    data-lid="{{ request()->segments()[2] }}">
                                Rate in Facebook
                            </button>
                        </div>
                    </div>
                </div>
                @endif
            </div>

            <div class="form-group" id="review-submit">
                <button type="submit" class="btn btn-purple text-uppercase btn-xl mt-4" id="btn-reviews" disabled>Submit </button>
            </div>
        </form>
    @endif
    </div>
@endsection

