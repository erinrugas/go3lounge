@component('mail::message')
# Subject: {{ $information['subject'] }}

# From:
{{ $information['name'] }} - {{ $information['email'] }} ({{ $information['contact'] }})
<br><br>

# Message:
{{ $information['message'] }}
@endcomponent
