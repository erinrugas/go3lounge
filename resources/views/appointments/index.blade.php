@extends('layouts.review')

@section('content')
    @if(!is_null($merchant->business_cover_img ))
        @php( $bgimage = config('app.merchant_url').'/'.$merchant->business_cover_img )
    @else
        @php( $bgimage = '/storage/img/bg.jpg' )
    @endif
    <div class="bg-image" style="background-image: url('{{ $bgimage }}');"> </div>
    <div id="formContent">
        <div class="text-center">
            <h3 class="font-weight-bold ">Your Appointment Informations </h3>
            <br>
            <h3 class="font-weight-bold">{{ $merchant->business_name }}</h3>
            <h6 class="mb-3">{{ $merchant->business_address}}</h6>
        </div>

        <hr>

        <div class="mt-5">
            <h5 ><strong>When:</strong> {{ \Carbon\Carbon::parse($appointment->checkin_time )->format('F j, Y h:ia') }}</h5>
            <h5 ><strong>Services:</strong> <br>
                <ul>
                @foreach ($appointment->services as $service)
                        <li>{{ $service->name }} </li>
                @endforeach
                </ul>
            </h5>
            <h5><strong>Status: </strong> {{ \DB::table(\Request::segment(2).'_appointment_status')->where('id', $appointment->status_id)->first()->name }}</h5>
        </div>

        <div class="mt-5 text-center">

            @if ($appointment->status_id == 6)
                <h5>Confirm your Appointment</h5>
                <div class="d-inline-block">
                    <div class="float-left mr-2">
                        <form action="{{ route('appointment.confirmed', ['bsid' => \Request::segment(2), 'id' => \Request::segment(3) ] ) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-outline-primary btn-lg mt-2">CONFIRM</button>
                        </form>
                    </div>
                    <div class="float-right">
                        <form action="{{ route('appointment.cancel', ['bsid' => \Request::segment(2), 'id' => \Request::segment(3) ] ) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-outline-danger btn-lg mt-2">CANCEL</button>
                        </form>
                    </div>
                </div>

            @endif
        </div>

        <hr>

        <div class="text-center mt-5">
            <h6 class="mb-3 mb-2">For questions or clarifications </h6>
            <h6>
                Contact us: +{{ $merchant->business_tel_prefix}}
                {{ \App\Services\Utilities\Helper::formatNumber($merchant->business_phone, $merchant->business_tel_prefi) }}
                <br>
                Email us: {{ $merchant->business_email }}
            </h6>
        </div>
    </div>
@endsection

