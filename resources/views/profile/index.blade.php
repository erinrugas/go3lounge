@extends('layouts.app')

@section('stylesheets')
    <link rel="stylesheet" href="{{ mix('css/datepicker/datepicker.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/momentjs/momentjs.js')  }}"></script>
    <script src="{{ mix('js/datepicker/datepicker.js')  }}"></script>
    <script src="{{ mix('js/profile/index.js')  }}"></script>
@endsection

@section('content')
    <header class="masthead" id="profile-masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="glide" id="masthead-glide">
                    <div data-glide-el="track" class="glide__track">
                        <h1 class="text-white font-weight-bold mb-5 text-capitalize">Profile</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="page-section bg-gray" id="user-profile">
        <div class="container">
            <div class="row">
                <div class="header " id="user-profile-header">
                    <h1 class="text-center">Edit User Profile</h1>
                    <p >update your registered information</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="">
                        @csrf
                        @method("PUT")
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="emp-no" class="col-sm-2 col-form-label">No.</label>
                                    <div class="col-sm-10">
                                        <input type="text" readonly class="form-control-plaintext" id="emp-no" value="0001">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control inp-height-20" id="email" value="johndoe@email.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Mobile Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="mobile_number" class="form-control inp-height-20" id="mobile-number" value="(63) 987-654-3210">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control inp-height-20" id="name" value="John Doe">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nickname" class="col-sm-2 col-form-label">Nickname</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="nickname" class="form-control inp-height-20" id="nickname" value="John">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="birthdate" class="col-sm-2 col-form-label">Birthday</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="birth_date" class="form-control inp-height-20"   id="birth-date" value="12/25/2019">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-xl bg-purple float-right" id="edit-profile "  ><i class="fas fa-pencil-alt"></i> EDIT PROFILE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
