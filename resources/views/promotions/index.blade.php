@extends('layouts.app')

@section('content')
    <header class="masthead" id="promotion-masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="glide" id="masthead-glide">
                    <div data-glide-el="track" class="glide__track">
                        <h1 class="text-white font-weight-bold mb-5 text-capitalize">Promotions</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="page-section promo-list" >
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div id="promotion-list-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    <h1>Promotion Lists</h1>
                                    <p>list of promotions that are available to you.</p>
                                </div>
                            </div>

                            <div class="col-md-6 mt-4">
                                <div class="float-right">
                                    <input type="text" class="form-control" placeholder="Find by Store" id="find-store">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" id="promo-list">
                <div class="col-md-3">
                    <a href="">
                        <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/spa-promo-design-template-2f488f8771cb79b1a35de6a62e47d13e_screen.jpg?ts=1567439530" alt="" class="promo-list-img">
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="https://pbs.twimg.com/media/DLQWEwDUEAAFtHB.jpg" alt="" class="img-thumbnail promo-list-img">
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="https://toccarespa.com.ph/promos/1a_classic_FB.jpg" alt="" class="img-thumbnail promo-list-img">
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/spa-promo-design-template-2f488f8771cb79b1a35de6a62e47d13e_screen.jpg?ts=1567439530" alt="" class="img-thumbnail promo-list-img">
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/hair-salon-special-offer-discount-banner-design-template-34f3aee8ea279d7493751dddc0042a33_screen.jpg?ts=1561539354" alt="" class="img-thumbnail promo-list-img">
                    </a>

                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="http://lakehamiltonpublish.com/wp-content/uploads/2018/09/hair-salon-banner.jpg" alt="" class="img-thumbnail promo-list-img">
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/purple-hair-salon-special-offer-banner-design-template-c8a655238cce2f50b9546f210eb3199f_screen.jpg?ts=1561539416" alt="" class="img-thumbnail promo-list-img">
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="">
                        <img src="https://cdn.shopify.com/s/files/1/1412/4580/files/Banner_2_bc802f78-1c19-49df-b944-cd7990061ef9_x788.jpg?v=1567335954" alt="" class="img-thumbnail promo-list-img">
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
