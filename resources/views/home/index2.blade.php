@extends('layouts.app2')

@section('pageHeader')
    Home
@endsection

@section('myStyle')
    <style>
        #about, #process, #how-it-works, #benefit {
            outline: none
        }

        /* .android-bg{
            position: relative;
            display: flex;
            align-items: center;
            justify-content: center;
            background-image: url('https://placekitten.com/1200/800');
            background-size: cover;
        }
        .android-bg::before{
            content: "";
            position: absolute;
            top: 0px;
            right: 0px;
            bottom: 0px;
            left: 0px;
            //background-color: rgba(0,0,0,0.25);
            background-color: rgba(152, 66, 211, 0.63);
        } */

        #download:focus{
            outline: none;
        }
        .android-bg{
            padding: 30px 0 20px;
            width: 100%;
            /* background-image: url('/storage/page/home/android-bg.jpg?h=786d100ac9c20678a1c19beda99d2281'); */
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100%;
            position: relative;
            margin: 0;
        }
        .android-bg::before{
            content: "";
            position: absolute;
            top: 0px;
            right: 0px;
            bottom: 0px;
            left: 0px;
            /* background-color: rgba(255, 255, 255,0.6); */
        }
        .android-bg h1{
            position: relative;
            width: 100%;
            font-size: 42px;
            font-family: Poppins, sans-serif;
        }
        .android-bg .android-btn-container{
            width: 100%;
            height: 100%;
            padding: 0;
            min-height: auto;
        }
        .android-btn-container .android-btn-image{
            margin: 50px 0;
            width: auto;
            height: auto;
            /* margin-top: 50px;
            margin-bottom: 50px; */
            width: 100%;
            max-width: 300px;
        }
        

    </style>
@endsection

@section('content')
<div style="background-image: url('/storage/page/home/backuper.png?h=b30779d596a190821e615a83db7e45ef');background-repeat: no-repeat;background-size: auto;background-position: top right;margin-bottom: 20px;height: auto; outline:none; ">
        <div class="container" id="about" >
            <div class="row" style="min-height: auto;">
                <div class="col-md-6" style="max-width: auto;width: auto;">
                    <div style="min-height: 245px;padding-top: 200px;padding-left: 0px;height: auto;z-index: 5;padding-bottom: 55px;">
                        <h1 class="text-left" style="font-weight: 200;font-size: 40px;font-family: Poppins, sans-serif;">Welcome to</h1>
                        <h1 class="display-4 text-left" style="font-weight: bold;font-size: 60px;font-family: Poppins, sans-serif;width: auto;max-width: auto;"><strong>GO3CHECKIN</strong></h1>
                        <div style="z-index: 2; position: absolute;">
                            <p style="font-family: Poppins, sans-serif;max-width: 80%;width: auto;">Creative, flexible and affordable website design.&nbsp;Custom websites at affordable prices.&nbsp;Delivering succes with every solution</p>
                            <a class="btn btn-primary btn-lg text-white border rounded-0 gradbutton" href="{{ route('register.index') }}"
                                style="width: 200px;font-family: Poppins, sans-serif;">Join for free</a></div>
                    </div>
                </div>
                <div class="col-md-6" style="width: auto;max-width: auto;height: auto;"><img class="img-fluid" src="/storage/page/home/tabletas.png?h=ecfb04e30101a77b88828c540c538e18" style="padding-top: 140px;width: auto;height: auto;"></div>
            </div>
        </div>
    </div>
    <div class="wavec" style="margin: 0px;width: 100%;max-width: 100%;min-width: 100%;height: auto;margin-top: -100px;"><div class="header">

        <div>
            <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
                <defs>
                    <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
                </defs>
                <g class="parallax">
                    <use xlink:href="#gentle-wave" x="48" y="7" fill="#eadffb" />
                </g>
            </svg>
        </div>

    </div>
    <section style="background-color: #eadffb;background-image: url('/storage/page/home/bgwl.png?h=bd3ee1c63b78c9514196eec61c981a44');background-repeat: no-repeat;background-position: bottom left;background-size: auto;margin-top: 0px;" id="how-it-works">
        <div class="container" style="padding-top: 15px;padding-bottom: 30px;" >
            <div class="row">
                <div class="col-md-6" style="padding-bottom: 10px;">
                    <h2 class="display-4 text-left" style="font-family: Poppins, sans-serif;">What is GO3CHECKIN?</h2>
                    <p class="lead" style="font-family: Poppins, sans-serif;font-size: 18px;">
                        <strong>Go3checkin</strong> is an application which can be used by the salon to monitor their checked in clients and promote their business through the reviews of their client. It also helps them to send promotions and reminders to their clients through text messages
                    </p>
                    <a href="{{ route('contact.index') }}" class="btn btn-primary btn-lg text-white border rounded-0 gradbutton" role="button" style="width: 200px;font-family: Poppins, sans-serif;">Learn more</a>
                </div>
                <div class="col-md-6">
                    <video width="100%" height="350px" preload controls src="/storage/video/marketing_video.mov"></video>
                    <!-- <div class="d-inline-block" style="background-image: url('/storage/page/home/vidback.png?h=047209b98640887eb749415f2fd53293');width: 100%;height: auto;background-size: contain;background-repeat: no-repeat;background-position: center;min-height: 350px;"></div> -->
                </div>
            </div>
        </div>
    </section>
    <div id="process" class="borderbott" style="padding-top: 20px;padding-left: 20px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="font-family: Poppins, sans-serif;padding-top: 40px;">
                    <h1 class="display-1 text-center" style="font-size: 38px;padding-top: 50px;padding-bottom: 50px;font-family: Poppins, sans-serif;"><strong>Get your Go3checkin app in 4 easy steps!</strong></h1>
                </div>
            </div>
            <div class="row" style="padding-top: 0;">
                <div class="col-md-3 text-center" style="height: auto;">
                    <div class="text-center d-inline-block procontent" style="width: 80%;"><img src="/storage/page/home/step1.png?h=ff7b0d4be276043ac7febee8bf47ee15">
                        <p style="font-weight: bold;font-size: 20px;margin-top: 20px;margin-bottom: 20px;font-family: Poppins, sans-serif;"><strong>Sign Up or</strong><br><strong>Contact us</strong></p>
                    </div>
                    <div class="text-center d-inline-block float-right rightarrows" style="width: 20%;height: auto;padding-left: 20px;padding-top: 60px;"><img src="/storage/page/home/arrow.png?h=97927f2434ac16a11d18d7700c4f0976" style="padding-top: 0;"></div>
                </div>
                <div class="col-md-3 text-center" style="height: auto;">
                    <div class="text-center d-inline-block procontent" style="width: 80%;"><img src="/storage/page/home/step2.png?h=d482af882fde7f8ffb744171edaef5f2">
                        <p style="font-weight: bold;font-size: 20px;margin-top: 20px;margin-bottom: 20px;font-family: Poppins, sans-serif;"><strong>Verify</strong></p>
                    </div>
                    <div class="text-center d-inline-block float-right rightarrows" style="width: 20%;height: auto;padding-left: 20px;padding-top: 60px;"><img src="/storage/page/home/arrow.png?h=97927f2434ac16a11d18d7700c4f0976" style="padding-top: 0;"></div>
                </div>
                <div class="col-md-3 text-center" style="height: auto;">
                    <div class="text-center d-inline-block procontent" style="width: 80%;"><img src="/storage/page/home/step3.png?h=651a54524c8be5fad9b6ba64fb53e579">
                        <p style="font-weight: bold;font-size: 20px;margin-top: 20px;margin-bottom: 20px;font-family: Poppins, sans-serif;"><strong>Download the app</strong></p>
                    </div>
                    <div class="text-center d-inline-block float-right rightarrows" style="width: 20%;height: auto;padding-left: 20px;padding-top: 60px;"><img src="/storage/page/home/arrow.png?h=97927f2434ac16a11d18d7700c4f0976" style="padding-top: 0;"></div>
                </div>
                <div class="col-md-3 text-center" style="height: auto;">
                    <div class="text-center d-inline-block procontent" style="width: 80%;"><img src="/storage/page/home/step4.png?h=8e93939d6d8dd5dfc73c6da3bafe9767">
                        <p style="font-weight: bold;font-size: 20px;margin-top: 20px;margin-bottom: 20px;font-family: Poppins, sans-serif;"><strong>Gather reviews</strong></p>
                    </div>
                    <div class="text-center d-inline-block float-right rightarrows" style="width: 20%;height: auto;"><img style="padding-top: 60px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center gradback" style="padding-top: 10px;padding-bottom: 10px;width: 100%;height: 100%;">
        <div class="container text-center" id="benefit" style="padding-top: 50px;padding-bottom: 0;width: 100%;max-width: 100%;">
            <div class="row">
                <div class="col-md-12" style="width: 100%;padding-top: 40px;">
                    <h1 class="display-1 text-center" style="width: 100%;font-size: 42px;font-family: Poppins, sans-serif;"><strong>Benefits &amp; Features</strong><br></h1>
                </div>
            </div>
            <div class="row text-center" style="width: 100%;padding: 0px;background-image: url('/storage/page/home/tabback.png?h=786d100ac9c20678a1c19beda99d2281');background-position: center;background-repeat: no-repeat;background-size: contain;margin: 0;margin-left: 0;">
                <div class="col-md-12 text-center" style="min-height: auto;height: 100%;width: 100%;padding: 0;padding-top: 0;padding-bottom: 0;"><img src="/storage/page/home/tablet.png?h=5d31a2e13c435fa4d751bfc9682ff466" style="margin: 0;width: auto;height: auto;margin-top: 50px;margin-bottom: 50px;max-width: 80%;"></div>
            </div>
        </div>
    </div>
    <div class="text-center" style="width: 100%;height: 100%;">
        <div class="container text-center" id="download" style="padding-top: 50px;padding: 0;width: 100%;max-width: 100%;">
            <div class="row text-center android-bg">
                <h1 class="display-1 text-center"><strong>Download Merchant Kiosk App</strong><br></h1>
                <div class="col-md-3 offset-md-3 text-center android-btn-container">
                    <a href="/storage/Go3CheckinKiosk.apk" title="Download Kiosk Android APK">
                        <img src="/storage/page/home/android-dl.png?h=5d31a2e13c435fa4d751bfc9682ff466" class="android-btn-image" style="padding: 0 16px; ">
                    </a>
                </div>
                <div class="col-md-3 text-center android-btn-container">
                    <a href="https://play.google.com/store/apps/details?id=com.go3checkin.go3checkin_store_kiosk" title="Download Kiosk Android APK">
                        <img src="/storage/page/home/android-dl-play.png?h=5d31a2e13c435fa4d751bfc9682ff466" class="android-btn-image" style="max-width: 300px;">
                    </a>
                </div>
            </div>
        </div>
    </div>
{{--    <div style="background-color: #f4f3f6;">--}}
{{--        <div class="container" style="padding-top: 100px;padding-bottom: 50px;">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <h1 class="text-center" style="font-family: Poppins, sans-serif;"><strong>Testimonials</strong></h1>--}}
{{--                    <h3 class="text-center" style="font-weight: 600;font-size: 25px;margin-top: 20px;margin-bottom: 20px;padding-bottom: 10px;padding-top: 10px;font-family: Poppins, sans-serif;"><strong>See what people are saying about Go3checkin</strong></h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row text-center space-rows">--}}
{{--                <div class="col text-center" style="min-width: 343px;">--}}
{{--                    <div class="card border-white shadow-none cards-shadown cards-hover" data-aos="flip-left" data-aos-duration="950" style="max-width: 329px;height: 408px;background-image: url('/storage/page/home/card01.png?h=708b20748fc36f5e2cab323ec3956783');background-repeat: no-repeat;background-size: auto;">--}}
{{--                        <div class="card-header border-white shadow-none" style="background-color: transparent;">--}}
{{--                            <div class="cardheader-text">--}}
{{--                                <p style="margin-top: 50px;color: rgb(0,0,0);font-size: 12px;">My first time using Go3checkin really happy<br>with their quality and services. Will do more business&nbsp;with them in the future. Highly recommended!!!!</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card-body border-white shadow-none" style="padding-bottom: 10px;padding-top: 140px;">--}}
{{--                            <p class="card-text cardbody-sub-text" style="color: rgb(255,255,255);font-size: 25px;">Name01</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col text-center" style="min-width: 343px;">--}}
{{--                    <div class="card cards-shadown cards-hover" data-aos="slide-right" data-aos-duration="950" style="max-width: 329px;height: 408px;background-image: url('/storage/page/home/card02.png?h=7226baa89983b78a054f2e1c61758179);background-size: auto;background-repeat: no-repeat;">--}}
{{--                        <div class="card-header border-white shadow-none" style="background-color: transparent;">--}}
{{--                            <div class="cardheader-text">--}}
{{--                                <p style="margin-top: 50px;color: rgb(0,0,0);font-size: 12px;">My first time using Go3checkin really happy<br>with their quality and services. Will do more business&nbsp;with them in the future. Highly recommended!!!!</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card-body border-white shadow-none" style="padding-top: 140px;padding-bottom: 10px;">--}}
{{--                            <p class="card-text cardbody-sub-text" style="color: rgb(255,255,255);font-size: 25px;">Name02</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col text-center" style="min-width: 343px;">--}}
{{--                    <div class="card cards-shadown cards-hover" data-aos="flip-up" data-aos-duration="950" style="max-width: 329px;height: 408px;background-image: url('storage/page/home/card03.png?h=9f6c1ab09d82a2f41df49471f152e5f3');background-repeat: no-repeat;background-size: auto;">--}}
{{--                        <div class="card-header border-white shadow-none cards-header-hover" style="background-color: transparent;">--}}
{{--                            <div class="cardheader-text">--}}
{{--                                <p style="margin-top: 50px;color: rgb(0,0,0);font-size: 12px;">My first time using Go3checkin really happy<br>with their quality and services. Will do more business&nbsp;with them in the future. Highly recommended!!!!</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card-body border-white shadow-none" style="padding-top: 140px;padding-bottom: 10px;">--}}
{{--                            <p class="card-text cardbody-sub-text" style="color: rgb(255,255,255);font-size: 25px;">Name03</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div>--}}
{{--        <div class="container" style="padding-top: 50px;padding-bottom: 50px;"><a class="border-white shadow-none" href="register.html"><img class="bg-white border-white shadow-none" src="/storage/page/home/processback.png?h=72ed7bf5b9bef5b2bce49dffa6ea84db" style="width: 100%;height: auto;min-width: 100%;max-width: 100%;"></a></div>--}}
{{--    </div>--}}
{{--    <div id="faq">--}}
{{--        <div class="container" style="padding-top: 50px;padding-bottom: 50px;">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}
{{--                    <h1 class="display-4 text-break" style="font-family: Poppins, sans-serif;"><strong>Have a&nbsp;question?</strong></h1><img class="img-fluid" src="/storage/page/home/qimage.png?h=8bde172d0a4515c23ec577606df6819a"></div>--}}
{{--                <div class="col-md-6">--}}
{{--                    <div role="tablist" id="accordion-1">--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-1" href="#accordion-1 .item-1" class="colbut" style="color: rgb(8,0,4);"><br>+ What is Go3Checkin?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-1" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-2" href="#accordion-1 .item-2" class="colbut" style="color: rgb(8,0,4);"><br><strong>+ What devices are compatible to Go3checkin?</strong><br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-2" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-3" href="#accordion-1 .item-3" class="colbut" style="color: rgb(8,0,4);"><br>+ What are the hardware requirements of Go3checkin?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-3" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-4" href="#accordion-1 .item-4" class="colbut" style="color: rgb(8,0,4);"><br>+ How do I set up the Go3checkin?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-4" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-5" href="#accordion-1 .item-5" class="colbut" style="color: rgb(8,0,4);"><br>+ How do I check in as a customer in the Go3checkin?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-5" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-6" href="#accordion-1 .item-6" class="colbut" style="color: rgb(8,0,4);"><br>+ How can I sent a review regarding the service made?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-6" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-7" href="#accordion-1 .item-7" class="colbut" style="color: rgb(8,0,4);"><br>+ How do I sent out promotions to my customers?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-7" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-8" href="#accordion-1 .item-8" class="colbut" style="color: rgb(8,0,4);"><br>+ How can I manage my app?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-8" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-9" href="#accordion-1 .item-9" class="colbut" style="color: rgb(8,0,4);"><br>+ How much does your service cost?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-9" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card coldiv colbut" style="font-family: Poppins, sans-serif;">--}}
{{--                            <div class="card-header" role="tab" style="background-color: transparent;padding: 0px;height: 50px;">--}}
{{--                                <h5 class="mb-0" style="line-height: 20px;height: 45px;font-family: Poppins, sans-serif;"><a data-toggle="collapse" aria-expanded="false" aria-controls="accordion-1 .item-10" href="#accordion-1 .item-10" class="colbut" style="color: rgb(8,0,4);"><br>+ Who do I turn to for questions and support?<br><br></a></h5>--}}
{{--                            </div>--}}
{{--                            <div class="collapse item-10" role="tabpanel" data-parent="#accordion-1" style="font-size: 16px;">--}}
{{--                                <div class="card-body">--}}
{{--                                    <p class="card-text" style="font-size: 16px;font-family: Poppins, sans-serif;color: rgb(0,0,0);">content 1</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
