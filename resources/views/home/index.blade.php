@extends('layouts.app')

@section('stylesheets')
    <link rel="stylesheet" href="{{ mix('css/templates/template.css') }}">
    <link rel="stylesheet" href="{{ mix('css/glide/glide.css') }}">
    <link rel="stylesheet" href="{{ mix('css/swiper/swiper.css') }}">
    <link rel="stylesheet" href="{{ mix('css/ratings/star-rating.css') }}">
    <link rel="stylesheet" href="{{ mix('css/fancyapps/jquery.fancybox.css') }}">
    <style>
        #map {
            height: 100%;
        }

    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ mix('js/ratings/star-rating.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/home/index.js') }}"></script>
{{--    <script>--}}
{{--        var map;--}}
{{--        function initMap() {--}}
{{--            map = new google.maps.Map(document.getElementById('map'), {--}}
{{--                center: {lat: -34.397, lng: 150.644},--}}
{{--                zoom: 8--}}
{{--            });--}}
{{--        }--}}
{{--    </script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places" async defer></script>
    <script type="text/javascript" src="{{ mix('js/templates/webprint.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/templates/product.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/fancyapps/jquery.fancybox.js') }}"></script>
@endsection

@section('content')
    <!-- Masthead -->
    <header class="masthead" id="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="glide" id="masthead-glide">
                    <div data-glide-el="track" class="glide__track">
                        <ul class="glide__slides">
                            <li class="glide__slide">
                                <h1 class="text-white font-weight-bold mb-5">Welcome to GO3LOUNGE</h1>
                                <p class="font-weight-light mb-5">Creative, flexible and affordable website design. Custom websites at affordable prices. Delivering success with every solution.</p>
                                @if (!session()->has('customer'))
                                    <div data-glide-el="controls">
                                        <a class="btn btn-border-white btn-xl " id="goto-login" href="#" data-glide-dir=">">Login To Start</a>
                                    </div>
                                @endif
                            </li>
                            @if (!session()->has('customer'))
                            <li class="glide__slide">
                                <h1 class="text-uppercase text-white font-weight-bold " >Login</h1>
                                <form action="#" method="POST" autocomplete="off">
                                    <div class="form-group">
                                        <input type="hidden" name="prefix" id="contact-prefix">
                                        <input type="text" class="form-control-lg input-md mt-4 mb-4" id="contact-number" name="contact_number" placeholder="Enter your number" >
                                        <div>
                                            <h6 id="error-msg" class="font-weight-bold text-white"></h6>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="login" class="btn btn-border-white btn-sm " data-toggle="modal">Submit</button>
                                    </div>
                                </form>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @include('templates.index')

@if (session()->has('customer'))
    <section class="page-section" id="visited-store">
        <div class="container-fluid">
            <div class="row justify-content-center align-content-center">
                <div class="col-md-12">
                    <div class="header text-center" id="visited-store-header">
                        <h1 class="font-weight-bold">MOST VISITED STORE</h1>
                        <p>list of stores that you mostly visited</p>
                    </div>
                    <div class="item">
                        <div class="card-group">
                            @foreach (array_slice($stores, 0,3) as $store)
{{--                                @if (array_key_exists('merchant', $store))--}}
                                    @if ($store['customer']['mobile_number'] == session('customer')['contact_number'])
                                    <div class="col-md-6 col-lg-4">
                                        <div class="card shadow visited-card" >
                                            <div class="position-relative">
                                                <div class="img-holder">
                                                    <img class="card-img-top"
                                                    src="{{ (is_null($store['merchant']['business_cover_img']))
                                                    ? env("APP_NAME")."/".url('storage/img/massage.jpg') : env("APP_MERCHANT_URL").'/'.$store['merchant']['business_cover_img'] }}" >
                                                </div>
                                                <div class="starrating d-flex flex-row-reverse">
                                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading"
                                                    value="{{  $store['total_review'] }}"
                                                    dir="ltr" data-size="md">
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <a href="javascript:;" class="view-store" data-is-show="1"
                                                            data-business-name="{{ $store['merchant']['business_name'] }}"
                                                            data-business-phone="{{ $store['merchant']['business_phone'] }}"
                                                            data-business-address="{{ $store['merchant']['business_address'] }}"
                                                            data-business-email="{{ $store['merchant']['business_email'] }}"
                                                            data-business-cover="{{ $store['merchant']['business_cover_img'] }}"
                                                            data-business-rate="{{ $store['total_review'] }}"
                                                           @if (array_key_exists('name', $store['google']) || array_key_exists('geometry', $store['google']))
                                                               data-google="{{ json_encode($store['google']) }}"
                                                               data-google-query="{{ json_encode($store['google']['name']) }}"
                                                               data-google-lat="{{ json_encode($store['google']['geometry']['location']['lat']) }}"
                                                               data-google-lng="{{ json_encode($store['google']['geometry']['location']['lng']) }}"
                                                            @endif
                                                        >
                                                            <h4 class="card-title float-right text-purple font-italic">{{ $store['merchant']['business_name'] }} </h4>
                                                        </a>
                                                    </div>
                                                    <div class="col-lg-12 d-inline-block">
                                                        <div class="float-left">
                                                            <i class="fas fa-phone-square-alt text-red"></i>
                                                        </div>
                                                        <div>
                                                            <p class="ml-4">{{ $store['merchant']['business_phone'] }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 d-inline-block">
                                                        <div class="float-left">
                                                            <i class="fas fa-map-marker text-cyan"></i>
                                                        </div>
                                                        <div>
                                                            <p class="ml-4">{{ $store['merchant']['business_address'] }}</p>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-12 d-inline-block">
                                                        <div class="float-left">
                                                            <i class="fas fa-envelope-square text-blue "></i>
                                                        </div>
                                                        <div>
                                                        <p class="ml-4">{{ $store['merchant']['business_email'] }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                    <a class="btn btn-purple text-uppercase btn-xl" data-customer="{{ session('customer')['contact_number']  }}" data-stores="{{ json_encode($stores)  }}" id="view-all-stores" href="#" data-toggle="modal">View all Stores </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section bg-purple" id="new-promotion">
        <div class="container ">
            <div class="row align-items-center justify-content-center text-center">
                <div class="header text-center" id="new-promotion-header">
                    <h1 class="text-white text-center">New Promotions</h1>
                    <p class="text-white">list of new promotions that you might have an interest</p>
                </div>
                <!-- Slider main container -->
                <div class="swiper-container" id="new-promotion-swiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="picture">
                                <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/spa-massage-parlor-flyer-template-design-80ace4b9ca91002760fc115b2c22ae4e_screen.jpg?ts=1561379143" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="picture">
                                <img src="https://toccarespa.com.ph/promos/1a_classic_FB.jpg" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="picture">
                                <img src="https://pbs.twimg.com/media/DLQWEwDUEAAFtHB.jpg" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="picture">
                                <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/spa-promo-design-template-2f488f8771cb79b1a35de6a62e47d13e_screen.jpg?ts=1567439530" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
{{--                        <div class="col-md-12">--}}
{{--                            <a class="btn btn-border-white btn-xl " id="view-promotions" href="javascript:void(0)">View available Promotions</a>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section" id="history">
        <div class="container-fluid ">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="header text-center" id="history-header">
                        <h1 class="text-center">History</h1>
                        <p >list of stores visited and used to promotions that you have availed</p>
                    </div>
                    <div class="item">
                        <div class="container">
                            <nav>
                                <div class="nav nav-tabs nav-fill " id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-store-tab" data-toggle="tab" href="#nav-store" role="tab" aria-controls="nav-store" aria-selected="true">Store</a>
                                    <a class="nav-item nav-link" id="nav-promotions-tab" data-toggle="tab" href="#nav-promotions" role="tab" aria-controls="nav-promotions" aria-selected="false">Promotions</a>
                                </div>
                            </nav>
                        </div>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-store" role="tabpanel" aria-labelledby="nav-store-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-group">
{{--                                            @if (!is_null($recentStores))--}}
                                            @foreach (array_slice($recentStores, 0, 3) as $key => $recentStore)
                                                @if ($recentStore['customer']['mobile_number'] == session('customer')['contact_number'])
                                                    <div class="col-md-6 col-lg-4">
                                                        <div class="card shadow visited-card" >
                                                            <div class="position-relative">
                                                                <div class="img-holder">
                                                                    <img class="card-img-top lazy" id="b-cover-image"
                                                                         src="{{ (is_null($recentStore['merchant']['business_cover_img'])) ?  env("APP_NAME").'/'.url('storage/img/massage.jpg') : env("APP_MERCHANT_URL").'/'.$recentStore['merchant']['business_cover_img'] }}" alt="" style="height: 550px" >
                                                                </div>
                                                                <div class="starrating d-flex flex-row-reverse">
                                                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading"
                                                                           value="{{  $recentStore['total_review'] }}"
                                                                           dir="ltr" data-size="md">
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <a href="javascript:void(0)" class="view-store" data-is-show="1"
                                                                           data-business-name="{{ $recentStore['merchant']['business_name'] }}"
                                                                           data-business-phone="{{ $recentStore['merchant']['business_phone'] }}"
                                                                           data-business-address="{{ $recentStore['merchant']['business_address'] }}"
                                                                           data-business-email="{{ $recentStore['merchant']['business_email'] }}"
                                                                           data-business-cover="{{ $recentStore['merchant']['business_cover_img'] }}"
                                                                           data-business-rate="{{ $recentStore['total_review'] }}"
                                                                           @if (array_key_exists('name', $store['google']) || array_key_exists('geometry', $store['google']))
                                                                               data-google="{{ json_encode($store['google']) }}"
                                                                               data-google-query="{{ json_encode($store['google']['name']) }}"
                                                                               data-google-lat="{{ json_encode($store['google']['geometry']['location']['lat']) }}"
                                                                               data-google-lng="{{ json_encode($store['google']['geometry']['location']['lng']) }}"
                                                                            @endif
                                                                        >
                                                                            <h4 class="card-title float-right text-purple font-italic">{{ $recentStore['merchant']['business_name'] }} </h4>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-lg-12 d-inline-block">
                                                                        <div class="float-left">
                                                                            <i class="fas fa-phone-square-alt text-red"></i>
                                                                        </div>
                                                                        <div>
                                                                            <p class="ml-4">{{ $recentStore['merchant']['business_phone'] }}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 d-inline-block">
                                                                        <div class="float-left">
                                                                            <i class="fas fa-map-marker text-cyan"></i>
                                                                        </div>
                                                                        <div>
                                                                            <p class="ml-4">{{ $recentStore['merchant']['business_address'] }}</p>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-lg-12 d-inline-block">
                                                                        <div class="float-left">
                                                                            <i class="fas fa-envelope-square text-blue "></i>
                                                                        </div>
                                                                        <div>
                                                                            <p class="ml-4">{{ $recentStore['merchant']['business_email'] }}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane fade show" id="nav-promotions" role="tabpanel" aria-labelledby="nav-promotions-tab">
                                <div class="row">
                                    <div class="card-group">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="card ">
                                                <div class="position-relative">
                                                    <div class="img-holder">
                                                        <img class="card-img-top" src="{{ url('storage/img/massage.jpg') }}" >
                                                    </div>
                                                    <div class="starrating d-flex flex-row-reverse">
                                                        <input id="input-3-ltr-star-md" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="0" dir="ltr" data-size="md">
                                                    </div>
                                                </div>
                                                <div class="card-body text-dark">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-lg-4">
                                            <div class="card ">
                                                <div class="position-relative">
                                                    <div class="img-holder">
                                                        <img class="card-img-top" src="{{ url('storage/img/massage.jpg') }}" >
                                                    </div>
                                                    <div class="starrating d-flex flex-row-reverse">
                                                        <input id="input-3-ltr-star-md" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="0" dir="ltr" data-size="md">
                                                    </div>
                                                </div>
                                                <div class="card-body text-dark">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4">
                                            <div class="card ">
                                                <div class="position-relative">
                                                    <div class="img-holder">
                                                        <img class="card-img-top" src="{{ url('storage/img/massage.jpg') }}" >
                                                    </div>
                                                    <div class="starrating d-flex flex-row-reverse">
                                                        <input id="input-3-ltr-star-md" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="0" dir="ltr" data-size="md">
                                                    </div>
                                                </div>
                                                <div class="card-body text-dark">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{--        <section class="page-section " id="user-profile">--}}
{{--            <div class="container">--}}
{{--                <div class="row align-items-center justify-content-center text-center">--}}
{{--                    <div class="header text-center" id="user-profile-header">--}}
{{--                        <h1 class="text-center">User Profile</h1>--}}
{{--                        <p >your registered personal information you used in our system, <br> you can change or edit the details as you please.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <div class="table-responsive ">--}}
{{--                            <table class="table">--}}
{{--                                <tbody>--}}
{{--                                <tr>--}}
{{--                                    <td>No. 0001</td>--}}
{{--                                    <td>Name: John Doe</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <td>Email: johndoe@email.com</td>--}}
{{--                                    <td>Nickname: John</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <td>Mobile Number: (63) 987-654-3210</td>--}}
{{--                                    <td>Birth Day: December 25, 2019</td>--}}
{{--                                </tr>--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row mt-5 text-center">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <a class="btn btn-border-gray btn-xl" id="edit-profile" href="{{ route('profile.index') }}"><i class="fas fa-pencil-alt"></i> EDIT PROFILE</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}

@endif
    <div class="modal " id="login-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered animated fadeIn" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">OTP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" id="submit-otp" autocomplete="off">
                    <div class="modal-body">
                        <div id="otp-confirm"></div>
                        <div id="otp-code"></div>
                        @csrf
                        <input type="text" class="form-control" name="otp" id="otp" maxlength="8" >
                        <input type="hidden" class="form-control-lg input-lg mt-4 mb-4" id="otp-checker" name="otp_checker" placeholder="Enter your number">
                        <h6 class="text-success" id="otp-msg"></h6>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" disabled id="otp-submit">Submit</button>
                        <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal animated fadeIn modal-pr-0" id="all-store-modal" >
        <div class="modal-dialog modal-95">
            <div class="modal-content">
                <div class="modal-header" id="visited-modal-header">
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="item" id="store-item">
                        <div class="container-fluid store-container">
                            <div class="row" id="store-lists-row">
                                <div class="col-md-12">
                                    <div class="store-lists d-inline">
                                        <div class="col-md-12">
                                            <div class="float-left" id="store-title">
                                                <h2 class="font-weight-bold">Store List</h2>
                                                <p class="font-italic">list of stores you mostly visited</p>
                                            </div>

{{--                                            <div class="float-right" id="store-form">--}}
{{--                                                <form action="#" method="POST">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <input type="text" class="form-control input-lg mt-3" id="find-store" name="store" placeholder="Find Store">--}}
{{--                                                    </div>--}}
{{--                                                </form>--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="all-stores-row">
                                    {{-- @foreach ($stores as $store)
                                        <div class="col-md-6 col-lg-6 col-xl-4 mt-4">
                                            <div class="card shadow visited-card">

                                                    <div class="img-holder">
                                                        <img class="card-img-top" src="{{ (is_null($store['merchant']['business_cover_img'])) ? url('storage/img/massage.jpg') : $store['merchant']['business_cover_img'] }}" >
                                                    </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <a href="javascript:void(0)" class="view-store" data-is-show="1"
                                                               data-business-name="{{ $store['merchant']['business_name'] }}"
                                                               data-business-phone="{{ $store['merchant']['business_phone'] }}"
                                                               data-business-address="{{ $store['merchant']['business_address'] }}"
                                                               data-business-email="{{ $store['merchant']['business_email'] }}"
                                                               data-business-cover="{{ $store['merchant']['business_cover_img'] }}"
                                                            >
                                                                <h4 class="card-title float-right text-purple font-italic">{{ $store['merchant']['business_name'] }} </h4>
                                                            </a>
                                                        </div>
                                                        <div class="col-lg-12 d-inline-block">
                                                            <div class="float-left">
                                                                <i class="fas fa-phone-square-alt text-red"></i>
                                                            </div>
                                                            <div>
                                                                <p class="ml-4">{{ $store['merchant']['business_phone'] }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 d-inline-block">
                                                            <div class="float-left">
                                                                <i class="fas fa-map-marker text-cyan"></i>
                                                            </div>
                                                            <div>
                                                                <p class="ml-4">{{ $store['merchant']['business_address'] }}</p>
                                                            </div>

                                                        </div>
                                                        <div class="col-lg-12 d-inline-block">
                                                            <div class="float-left">
                                                                <i class="fas fa-envelope-square text-blue "></i>
                                                            </div>
                                                            <div>
                                                                <p class="ml-4">{{ $store['merchant']['business_email'] }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach --}}

                            </div>
                        </div>


                    </div>

                    <div class="item d-none" id="view-specific-store">
                        <div class="container-fluid store-container">
                            <div class="row">
                                <a href="javascript:void(0)" class="back-btn ml-4 mb-4 text-purple" data-customer="{{ session('customer')['contact_number'] }}" data-stores="{{ json_encode($stores) }}"><i class="fas fa-arrow-left"></i> Back to store List</a>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-lg-5" >
                                    <div class="position-relative">
                                        <div class="img-holder">
                                            <img class="card-img-top lazy b-cover-image" id="b-cover-image" alt="" style="height: 550px" >
                                        </div>
                                        <div class="starrating d-flex flex-row-reverse">
                                            <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading b-ratings" value="" dir="ltr" data-size="md">
                                        </div>
                                    </div>
                                    <div class="row" >
                                        <div class="col-lg-12">
                                            <h2 class="text-purple font-italic" id="b-name"></h2>
                                        </div>
                                        <div class="col-lg-12 d-inline-block">
                                            <div class="float-left">
                                                <i class="fas fa-phone-square-alt text-red"></i>
                                            </div>
                                            <div>
                                                <p class="ml-4" id="b-phone"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 d-inline-block">
                                            <div class="float-left">
                                                <i class="fas fa-map-marker text-cyan"></i>
                                            </div>
                                            <div>
                                                <p class="ml-4" id="b-address"></p>
                                            </div>

                                        </div>
                                        <div class="col-lg-12 d-inline-block">
                                            <div class="float-left">
                                                <i class="fas fa-envelope-square text-blue "></i>
                                            </div>
                                            <div>
                                                <p class="ml-4" id="b-email"></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-7">
{{--                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1414.6951193476475!2d-84.96499369735076!3d32.50661808318764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x888ccc94ed099c81%3A0xe66e9914ffd61fdf!2s1900%20Manchester%20Expy%2C%20Columbus%2C%20GA%2031904%2C%20USA!5e0!3m2!1sen!2sph!4v1581049365661!5m2!1sen!2sph" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>--}}
                                    <div id="map"></div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
