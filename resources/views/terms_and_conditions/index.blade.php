@extends('layouts.app2')

@section('pageHeader')
    Terms and Conditions
@endsection

@section('content')
    <div class="container" style="padding-top: 100px;padding-bottom: 10px;width: 100%;height: 100%;min-height: calc(100vh - 127px);">
        <div class="row">
            <div class="col-md-12" style="width: 100%;padding-top: 40px;">
                <h1 class="display-1 text-center" style="width: 100%;font-size: 42px;font-family: Poppins, sans-serif;"><strong> Terms and Conditions</strong><br></h1>
            </div>

            <div class="col-md-12">
                <p>
                    Welcome to Go3Checkin! Before you use our Sites, please read these Terms and Conditions carefully.
                </p>
                <p>
                    GO3 Solutions, Inc (the “Company”) owns and operates certain websites https://www.go3checkin.com.,
                    certain technology platforms, and certain other related online and mobile services that reference or will reference these Terms and Conditions.
                    These Terms and Conditions (the “Agreement”) constitute a contract between you and us that governs your access and use of the Sites.
                    It means that by accessing and/or using the Sites or our services through the Sites, or by clicking a button or checking a box marked
                    “I Agree” (or something similar), you agree to all the terms and conditions of this Agreement.
                    If you do not agree, do not use the Sites. As used in this Agreement, “you” means any visitor, user, or
                    other person who accesses our Sites, whether or not such person registered for an Account (as defined below).
                </p>
                <p>
                    <strong> I. ABOUT GO3CHECKIN</strong> <br>
                    GO3CHECKIN connects with local salon and business. You may book appointment through the Sites
                    from particular salon or spa or other purveyors of salon in cities throughout United States.
                </p>
                <p>
                    <strong> II. USING GO3CHECKIN.COM</strong> <br>
                    You may only create and hold one account for your personal use. You may have another account if you are using
                    the Site as part of a corporate account created for business purposes pursuant to a separate agreement with GO3CHECKIN.
                    In consideration of the use of the Sites and the services contained therein, you agree that you are able
                    to create a binding legal obligation with GO3CHECKIN and you also agree to: (a) provide true, accurate, current,
                    and complete information about yourself, and (b) maintain and promptly update the personal information
                    you provide to keep it true, accurate, current, and complete.
                </p>
                <p>
                    The Sites may permit you to make purchases without an Account or without logging in to your Account.
                    If you make a purchase in this manner, we may create an Account for you based on the information provide to
                    us in connection with the transaction (e.g., your name, phone number, email address, and other transaction information).
                    If you are a minor in the jurisdiction in which you reside (generally under the age of 18), you must have the permission of,
                    and be directly supervised by, your parent or legal guardian to use the Sites, and your parent or legal guardian must
                    read and agree to this Agreement prior to your using the Sites. Notwithstanding the foregoing, you are not authorized to use the Sites
                    if you are under the age of 13. If you are using the Sites on behalf of an entity, organization, or company,
                    you represent and warrant that you have the authority to bind that organization to this Agreement and you agree to be
                    bound by this Agreement on behalf of that organization.
                </p>
                <p>
                    if you provide any information that is untrue, inaccurate, not current or incomplete, including, without limitation,
                    having an invalid or expired payment method on file, or if GO3CHECKIN has reasonable grounds to suspect that
                    any information you provide is untrue, inaccurate, not current or incomplete, GO3CHECKIN has the right to block your current or
                    future use of the Sites (or any portion thereof) and/or terminate this Agreement with you. If your Account is cancelled for any or no reason,
                    you may forfeit any pending, current, or future account credits or promotional offers and any other forms of unredeemed value in or
                    associated with your Account without prior notice to you.
                </p>
                <p>
                    You are responsible for maintaining the confidentiality and security of your Account including your password and, if applicable,
                    any password for Facebook, Google, or other third party login. You are also responsible for all activities or
                    any other actions that occur under or that are taken in connection with your Account. You agree to: (a) immediately notify
                    E of any known or suspected unauthorized use(s) of your password or Account, or any known or suspected breach of security, including,
                    without limitation, loss, theft, or unauthorized disclosure of your password or credit card information; and (b) ensure that you exit from
                    your Account at the end of each session. GO3CHECKIN will not be liable for any injury, loss, or damage of any kind arising from or relating to your
                    failure to comply with (a) and/or (b) or for any acts or omissions by you or someone else who is using your Account and/or password.
                </p>
                <p>
                    <strong> III. PAYMENT AND OUR CREDIT POLICY</strong> <br>
                    Certain features of the Sites, including, without limitation, the membership plan, may require you to make certain payments.
                    When paid by you, these payments are final and non-refundable, unless otherwise determined by us. In its sole discretion,
                    we may offer credits or refunds on a case-by-case basis including, by way of example, in the event of an error with your order or
                    in the amounts you were charged.
                </p>
                <p>
                    We will charge, and you authorize us to charge, the payment method you specify at the time of purchase.
                    If you pay any amounts with a credit card, we may seek pre-authorization of your credit card account prior to your
                    purchase to verify that the credit card is valid and has credit available for your intended purchase.
                    In the event that you fail to pay such invoices within thirty (30) days of the date of such invoice (the “Payment Due Date”),
                    you grant us the right, but not the obligation, to charge the credit card you provide with your Account at any time after any Payment
                    Due Date, unless prohibited by law.
                </p>
                <p>
                    We reserve the right to establish, remove, and/or revise prices, fees, and/or surcharges for any or all
                    services or goods obtained through the use of the services at any time. We may also, in its sole discretion,
                    make promotional offers with different features and different rates to any or all of our customers. Unless made to you,
                    these promotional offers will have no bearing on your obligation to pay the amounts charged.
                </p>
                <p>
                    <strong>IV. Use of Cookies</strong><br>
                    This website, like many other commercial applications and web sites, utilizes a standard technology called "cookies"
                    to collect information about how our site is used. Cookies are designed to help a web site operator determine that a
                    particular user has visited the site previously and thus save and remember any preferences that may have been set while the user was browsing the site.
                    Cookies are small strings of text that web sites can send to your browser.
                    Cookies cannot retrieve any other data from your hard drive or obtain your e-mail address. If you are simply browsing the website,
                    a cookie may be used to identify your browser as one that has visited the site before.
                </p>

                <p>
                    We also make use of memory-based cookies in support of authenticating the user of certain the Company website applications.
                    Similarly, if you are a registered user of a site providing service to the Company users (and have a user ID and password),
                    such sites may use cookies to provide personalized information based on preferences you have indicated while using that site.
                    Although you have the ability to modify your browser to either accept all cookies, notify you when a cookie is sent, or reject all cookies,
                    it may not be possible to utilize features of this website if you reject cookies.
                </p>

                <p>
                    <strong>V. Communications</strong>
                    We may request that you voluntarily supply us with information, including your e-mail address, street address,
                    telephone number or other information so that we may enhance your site visit, fulfill any orders you may place
                    through our site, or follow up with you after your visit. Whether you provide any information is entirely up to you.
                </p>

                <p>
                    <strong>VI.CHANGES TO THE AGREEMENT</strong>
                    We may change this Agreement from time to time and without prior notice. If we make a change to this Agreement,
                    it will be effective as soon as we post it and the most current version of this Agreement will always be posted under
                    the "Terms and Conditions" link available on our Sites. If we make a material change to the Agreement, we may notify you.
                    You agree that you will review this Agreement periodically. By continuing to access and/or use the Sites after we post Updated
                    Terms and Conditions, you agree to be bound by the Updated Terms and Conditions, and if you do not agree to the Updated Terms and Conditions,
                    you will stop using the Sites. The terms and conditions of the Privacy Policy are incorporated into this Agreement.
                </p>

                <p>
                    <strong>VII.GOVERNING LAW</strong> <br>
                    You acknowledge and agree that your access to and/or use of the Sites, the Materials, and other content on the Sites is subject
                    to all applicable federal, state, and local laws and regulations.
                    If you have questions or concerns regarding this Agreement, you should contact the Company as follows: <br>
                    Toll Free: 888-377-3818 <br>
                    E-mail: Support@goetu.com
                </p>

            </div>
        </div>
    </div>
@endsection
