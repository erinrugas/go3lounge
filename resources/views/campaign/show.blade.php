@extends('layouts.promo')

@section('title', 'Campaign Details')

@section('content')
    <div class="bg-image" style="background-image: url(/storage/img/cover.jpg)"> </div>
    
    <div class="container bg-white p-0">
        <div class="container-fluid">
            <div class="row">
                <p class="h4 text-center p-3 w-100">
                    You are entitled to avail this promo<br/>
                    Code: <strong>{{ 'C-'.$campaign->code.'-'.$lead->id }}</strong>
                </p>
            </div>
            <div class="row">
                <img class="conver-image w-100" src="{{ config('app.campaign_img_url').'/'.$campaign->img_src }}">
                <p class="h6 w-100 text-center mt-2 text-muted">Note: this promo can only be use with the registered number<br/>{{ $lead->mobile_number }} </p>
            </div>
            <div class="row">
                <div class="col-12 merchant-footer bg-dark py-3">
                    <p class="h5 w-100 text-center mt-2 text-white">Promo duration from {{ $from }} until {{ $until }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection