@extends('layouts.app2')

@section('pageHeader')
    Contact Us
@endsection

@section('myStyle')
    <link rel="stylesheet" href="css/intl-tel-input/intlTelInput.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <style>
        .select2-container:focus, {
            /*border: 1px solid rgba(238,9,121,.25);*/
            box-shadow: 0 0 0 .2rem rgba(238,9,121,.25);
        }

        select {
            width: 100%;
        }
    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script src="js/datetimepicker/jquery.datetimepicker.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="js/intl-tel-input/intlTelInput.js"></script>
@endsection

@section('myScript')
    <script>
        $(".select2").select2();
        $('#regForm').submit(function (e) {
            e.preventDefault();

            $(this).find('.invalid-feedback').remove();
            $(this).find('input').removeClass('border-danger');

            var formData = $(this).serializeArray();
            $.post('/contact-us', formData).then(function(data) {

                $('.alert').removeClass('d-none');
                $('input').val('');

                setTimeout(() => {
                    if (!$('.alert').hasClass('d-none')){
                        $('.alert').addClass('d-none');
                    }


                }, 10000);
                location.href = "/contact-us"
            }).catch(function(err) {

                var errors = err.responseJSON.errors;
                $.each(errors, function(key, value) {
                    var err_html = '<div class="invalid-feedback d-block ">'+value[0]+'</div>';

                    $('input[name='+key+'], textarea[name='+key+']').addClass('border-danger');

                    switch(key) {
                        case 'contact_number':
                            $('input[name='+key+']').parents('.form-group').append(err_html);
                            break;
                        default:
                            $('input[name='+key+'], textarea[name='+key+']').after(err_html);
                    }
                });

            });
        });

        // input format alphabet only with spaces
        var alpha = function(inputName, length, i = 0) {
            var formatArr = [];

            for(i; length>i; i++) {
                formatArr.push('A');
            }

            var format = formatArr.join('');
            $('input[name="'+inputName+'"]').mask(format, {
                'translation': {
                    A: {pattern: /[A-Za-z ]/ }
                }
            });
        }

        // input format numbers only
        var num = function(inputName, length, i = 0) {
            var formatArr = [];

            for(i; length>i; i++) {
                formatArr.push('A');
            }

            var format = formatArr.join('');
            $('input[name="'+inputName+'"]').mask(format, {
                'translation': {
                    A: {pattern: /[0-9]/ }
                }
            });
        }

        $("#contact_number").attr('placeholder', 'XXX-XXX-XXXX');
        $("#contact_number").mask('000-000-0000');

        var businessContact = "#contact_number";
        var intlInput = function(selector) {
            var prefix = '_prefix';
            var input = document.querySelector(selector)
            var iti = window.intlTelInput(input, {
                allowDropdown: false,
                formatOnDisplay: true,
                onlyCountries: ['us'],
                separateDialCode: true,
                utilsScript: "js/intl-tel-input/utils.js",
            });
            var dialCode = iti.getSelectedCountryData().dialCode;
            $(selector+prefix).val(dialCode);
        }

        intlInput(businessContact);
    </script>
@endsection

@section('content')
    <div class="container" style="padding-top: 100px;padding-bottom: 10px;width: 100%;height: 100%;min-height: calc(100vh - 127px);">
        <div class="row">
            <div class="col-md-12" style="width: 100%;padding-top: 40px;">
                <h1 class="display-1 text-center" style="width: 100%;font-size: 42px;font-family: Poppins, sans-serif;"><strong>Contact Us</strong><br></h1>
            </div>
        </div>
        <form id="regForm">
            @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            @csrf
            <div class="row pb-3">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="first_name">First Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="first_name" id="first_name">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="last_name">Last Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="last_name" id="last_name">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="subject">Subject <span class="text-danger">*</span></label>
                        <select name="subject" class="select2" id="subject">
                            @foreach(json_decode(file_get_contents(base_path('resources/json/contact.json'))) as $subject)
                                <option value="{!!  $subject[0]->sale !!}">{!!  $subject[0]->sale !!}</option>
                                <option value="{!!  $subject[0]->technical !!}">{!!  $subject[0]->technical !!}</option>
                                <option value="{!!  $subject[0]->other !!}">{!!  $subject[0]->other !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email <span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="email" id="email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_number">Contact Number <span class="text-danger">*</span></label>
                        <input type="hidden" id="contact_number_prefix" name="contact_prefix" value="{{ old('tel_prefix') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="contact_number" id="contact_number">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="message">Message <span class="text-danger">*</span></label>
                        <textarea name="message" id="message" class="form-control" cols="30" rows="10" style="resize: none">{{ old('message') }}</textarea>
                    </div>
                </div>

                <div class="col-md-12 py-5 text-right">
                    <button type="submit" class="btn btn-primary btn-lg text-white border rounded-0 gradbutton business-register" role="button" style="width: 200px;font-family: Poppins, sans-serif;">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
