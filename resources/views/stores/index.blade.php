@extends('layouts.app')
@section('stylesheets')
    <link rel="stylesheet" href="{{ mix('css/ratings/star-rating.css') }}">
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ mix('js/ratings/star-rating.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/stores/index.js') }}"></script>
@endsection


@section('content')
    <header class="masthead" id="store-masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center text-center">
                <div class="glide" id="masthead-glide">
                    <div data-glide-el="track" class="glide__track">
                        <h1 class="text-white font-weight-bold mb-5 text-capitalize">Stores</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <section class="page-section store-list" >
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div id="promotion-list-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="float-left">
                                    <h1>Store Lists</h1>
                                    <p>list of stores that you mostly visited.</p>
                                </div>
                            </div>

                            <div class="col-md-6 mt-4">
                                <div class="float-right">
                                    <input type="text" class="form-control" placeholder="Eind Store" id="find-store">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" id="store-list">
                <div class="card-group">
                    <div class="col-md-4 col-lg-4">
                        <div class="card ">
                            <div class="position-relative">
                                <img class="card-img" src="{{ url('storage/img/massage.jpg') }}" alt="Card image">
                                <div class="starrating d-flex flex-row-reverse">
                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="3" dir="ltr" data-size="md">
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="card ">
                            <div class="position-relative">
                                <img class="card-img" src="{{ url('storage/img/massage.jpg') }}" alt="Card image">
                                <div class="starrating d-flex flex-row-reverse">
                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="3" dir="ltr" data-size="md">
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="card ">
                            <div class="position-relative">
                                <img class="card-img" src="{{ url('storage/img/massage.jpg') }}" alt="Card image">
                                <div class="starrating d-flex flex-row-reverse">
                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="3" dir="ltr" data-size="md">
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="card ">
                            <div class="position-relative">
                                <img class="card-img" src="{{ url('storage/img/massage.jpg') }}" alt="Card image">
                                <div class="starrating d-flex flex-row-reverse">
                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="3" dir="ltr" data-size="md">
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="card ">
                            <div class="position-relative">
                                <img class="card-img" src="{{ url('storage/img/massage.jpg') }}" alt="Card image">
                                <div class="starrating d-flex flex-row-reverse">
                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="3" dir="ltr" data-size="md">
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="card ">
                            <div class="position-relative">
                                <img class="card-img" src="{{ url('storage/img/massage.jpg') }}" alt="Card image">
                                <div class="starrating d-flex flex-row-reverse">
                                    <input id="store-rate" name="input-3-ltr-star-md" class="kv-ltr-theme-fas-star rating-loading" value="3" dir="ltr" data-size="md">
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title float-right">Ladies Nail & Spa</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-phone-square-alt"></i> <p class="d-inline-block">(000) 000 - 0000</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-map-marker"></i> <p class="d-inline-block">1900 Manchester Expy, Columbus, GA 31904</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <i class="fas fa-envelope-square"></i> <p class="d-inline-block">storeemailaddress@mail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
