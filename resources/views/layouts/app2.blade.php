<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Go3Checkin - @yield('pageHeader')</title>
    <meta name="description" content="Go3Checkin">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <link rel="stylesheet" href="css/website2.min.css">
    @yield('myStyle')

</head>

<body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top navbar-custom" style="background-color: #e3dfeb;font-weight: bold;padding-left: 50px;padding-right: 50px;">
        <div class="container-fluid"><a href="/#about"><img class="img-fluid" src="/storage/page/home/go3checkinlogo200.png" style="margin-right: 50px;"></a><button data-toggle="collapse" class="navbar-toggler" data-target="#navbarResponsive"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
                id="navbarResponsive">
                <ul class="nav navbar-nav mr-auto" style="font-weight: normal;">
                    <li class="nav-item" role="presentation" style="padding-left: 15px;padding-right: 15px;"><a class="nav-link" href="/#about" style="font-weight: 1000;font-size: 14px;font-family: Poppins, sans-serif;">ABOUT</a></li>
                    <li class="nav-item" role="presentation" style="padding-right: 15px;padding-left: 15px;"><a class="nav-link" href="/#how-it-works" style="font-weight: 1000;font-style: normal;font-size: 14px;font-family: Poppins, sans-serif;">HOW IT WORKS</a></li>
                    <li class="nav-item" role="presentation" style="padding-right: 15px;padding-left: 15px;"><a class="nav-link" href="/#benefit" style="font-weight: 1000;font-size: 14px;font-family: Poppins, sans-serif;">BENEFITS</a></li>
{{--                    <li class="nav-item" role="presentation" style="padding-right: 15px;padding-left: 15px;"><a class="nav-link" href="/#faq" style="font-weight: 1000;font-size: 14px;font-family: Poppins, sans-serif;">FAQ</a></li>--}}
                    <li class="nav-item" role="presentation" style="padding-right: 15px;padding-left: 15px;"><a class="nav-link" href="/#process" style="font-weight: 1000;font-size: 14px;font-family: Poppins, sans-serif;">PROCESS</a></li>
                    <li class="nav-item" role="presentation" style="padding-right: 15px;padding-left: 15px;"><a class="nav-link" href="/#download" style="font-weight: 1000;font-size: 14px;font-family: Poppins, sans-serif;">DOWNLOAD</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="nav-item" role="presentation" style="padding-right: 15px;padding-left: 15px;background-color: #1bc0ff;"><a class="nav-link" href="{{ route('contact.index') }}" style="color: #f8f8f8;font-weight: bold;font-family: Poppins, sans-serif;">CONTACT US</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <footer class="py-5 bg-black" style="opacity: 0.30;">
        <div class="container">
            <p class="text-center text-light m-0 small" style="color: rgb(17,16,16);font-weight: bold;font-size: 20px;">© 2020 Go3Checkin. All Rights Reserved&nbsp; &nbsp; &nbsp; <a href="{{ route('terms.index') }}" target="_blank" style="color:#fff">Terms & Condition</a></p>
        </div>
    </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
    <script src="js/website2.min.js"></script>
    @yield('script')

    @yield('myScript')
</body>

</html>
