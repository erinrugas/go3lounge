<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="g-key" content="{{ env('GOOGLE_API_KEY') }}">
    <meta name="app-name" id="app-name" content="{{ config('app.name') }}">
    <meta name="merchant-url" id="merchant-url" content="{{ env("APP_MERCHANT_URL") }}">
    <title>{{ config('app.name') }} {{ (\Route::currentRouteName() == "appointment.index") ? 'Appointments' : 'Reviews' }}</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&display=swap" rel="stylesheet">

    <!-- Styles -->
    @if (\Route::currentRouteName() == "appointment.index")
        <link href="{{ mix('css/appointment.css') }}" rel="stylesheet">
    @else
        <link href="{{ mix('css/review.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('css/ratings/star-rating.css') }}">
    @endif
</head>

<body>
<div class="wrapper fadeIn first p-0">
    @yield("content")
</div>

<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>

@yield('scripts')

@if (\Route::currentRouteName() == "review.index" || \Route::currentRouteName() == "review.show")
<script type="text/javascript" src="{{ mix('js/ratings/star-rating.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/reviews/index.js') }}"></script>
@endif
</body>
</html>
