<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/review.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/ratings/star-rating.css') }}">
</head>

<body>
<div class="wrapper fadeIn first p-0">
    @yield("content")
</div>

@yield('scripts')

</body>
</html>
