<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-name" id="app-name" content="{{ config('app.name') }}">
    <meta name="merchant-url" id="merchant-url" content="{{ env("APP_MERCHANT_URL") }}">
    <meta name="g-key" content="{{ env('GOOGLE_API_KEY') }}">

    <title>{{ config('app.name') }} {!! (ucwords(explode('.',\Route::currentRouteName())[0]) == "Home") ? '' : "- ".ucwords(explode('.',\Route::currentRouteName())[0]) !!}</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600">
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('stylesheets')
</head>

<body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
        <div class="container-fluid">
            <div class="float-left">
                <a class="navbar-brand js-scroll-trigger font-weight-bold" href="{{ route('home') }}">{{ strtoupper(config('app.name')) }}</a>
                <a class="navbar-brand js-scroll-trigger font-weight-bold" id="goto-merchant" target="_blank" href="#">Merchant</a>
                <a class="navbar-brand js-scroll-trigger font-weight-bold" id="goto-admin" target="_blank" href="#">Admin</a>
            </div>
            @if (session('customer')['contact_number'])
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0">
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger font-weight-bold" href="#"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ "XXX-XXX-".substr(session('customer')['contact_number'], -4) }} (Log out)
                            </a>
                        </li>
                    </ul>
                </div>
            @endif
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </nav>

    @yield("content")

    <footer class="py-5">
        <div class="container text-center">
            <small>&copy; {{ \Carbon\Carbon::now()->format('Y') }} {{ config('app.name') }} All rights reserved.</small>
        </div>
    </footer>

    <!-- Back to top button -->
    <a id="back-to-top"></a>

    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
