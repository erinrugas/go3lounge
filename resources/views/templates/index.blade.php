<!-- NAVIGATION -->
 <div class="container">
    <div class="row pageSection">
        <!-- CATEGORIES -->
        @include('templates.categories')
        <!-- CATEGORIES -->
        <div class="col-md-8 col-xl-9">
        <!-- MOBILE CATEGORIES -->
        <div class="d-block d-md-none" style="margin: 30px 0;">
            <h4 style="margin-bottom: 0px;">Categories</h4>
            <div class="categories-mobile">
                <ul id="categories-mobile">

                </ul>
            </div>
        <hr>
        </div>
        <!-- MOBILE CATEGORIES -->
            <!-- <div class="banner"><h3 id="category-title"></h3></div> -->
            <div>
                <div style="padding: 15px;">
                    <div id="scrolltothis"></div>
                    <!-- <span class="pull-right">
                    Sort:
                     <select class="sort" id="sort">
                        <option value="byName">By Name</option>
                        <option value="byDate">Recently Added</option>
                        <option value="byViews">Most Viewed</option>
                        <option value="byOrders">Most Ordered</option>
                    </select> 
                    </span><br> -->
                    <!-- LOADER -->
                    <div class="products-loader" style="margin-top: 30px;">
                        <div class="row product-section">
                            <?php for ($x = 1; $x <= 12; $x++) { ?>
                                <div class="col-xl-4 col-lg-6 col-sm-6 col-6 product-collection"><div><div class="img-cont loader linear-background"></div></div></div>
                            <?php } ?>
                        </div> 
                    </div>
                    <!-- LOADER -->
                    <div class="row product-section" id="products">
                        <div class="text-center" id="no-products"></div>
                        
                        <!-- <div class="col-xl-4 col-lg-6 col-sm-6 product-collection">
                            <div>
                                <div class="img-cont"><img src="assets/img/ad2.png">
                                    <p>Product Name &nbsp;afa asasfwef fwef</p><span class="products-btn"><a class="btn btn-sm view-btn" role="button">VIEW</a><a class="btn btn-sm order-btn" role="button">ORDER NOW</a></span>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div style="margin: 15px 0px; cursor: default;">
                        <!-- <div class="text-center">
                            <a  class="btn"> NEXT PAGE </a>
                        </div> -->
                        <div class="float-right">
                            <!-- <a class="pageText firstPage">First</a> | -->
                            <a class="pageText prevPage yellowPage"><span style="font-size: 30px;"><i class="fa fa-angle-left"></i></span></a>&nbsp;&nbsp; 
                                <input type="number" name="page" class="page" style="width:50px;text-align: center;" min="1" max="9"> out of <span id="totalpage"> </span>
                            <!-- <select class="sort page">
                                <option>1</option>
                            </select> -->
                            &nbsp;&nbsp;<a class="pageText nextPage yellowPage"><span style="font-size: 30px;"><i class="fa fa-angle-right" style="width: 28px;"></i></span></a>
                                <!-- | <a class="pageText lastPage">Last</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>