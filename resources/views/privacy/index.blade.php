@extends('layouts.app2')

@section('pageHeader')
    Privacy Policy
@endsection

@section('content')

    <div class="container" style="padding-top: 100px;padding-bottom: 10px;width: 100%;height: 100%;min-height: calc(100vh - 127px);">
        <div class="row">
            <div class="col-md-12" style="width: 100%;padding-top: 40px;">
                <h1 class="display-1 text-center" style="width: 100%;font-size: 42px;font-family: Poppins, sans-serif;"><strong>Privacy Policy</strong><br></h1>
            </div>

            <div class="col-md-12">
                <p>
                    We are concerned about our customer’s privacy and want you to be familiar with how we collect,
                    use and disclose Personally Identifiable Information (as defined below).
                    This Privacy Policy describes our practices in connection with privacy and the Personally Identifiable
                    Information that we collect through this website (Go3Checkin.com), the related mobile sites and mobile
                    application(s) and selected other domains (the "Sites"). Except as indicated below,
                    this Privacy Policy does not address the collection,
                    use or disclosure of information by any of our affiliates or by other third parties on any websites
                    other than the Sites. As used in this Agreement, "Go3Checkin.com" "we," "us," and "our" shall mean
                    Go3Checkin.com and its subsidiaries and affiliates.
                </p>
                <p>
                    By accessing and/or using the Sites, you agree to all the terms and conditions of this Privacy Policy.
                    If you do not agree to all the terms and conditions of this Privacy Policy, do not use the Sites.
                    We may change this Privacy Policy from time to time and without prior notice.
                    If we make a change to this Privacy Policy, it will be effective as soon as we post it, and the most
                    current version of this Privacy Policy will always be posted under the Privacy Policy tab of the
                    Go3Checkin.com website. If we make a material change to this Privacy Policy, we may notify you.
                    You agree that you will check out this Privacy Policy periodically. By continuing to access and/or
                    use the Sites after we make changes to this Privacy Policy, you agree to be bound by the revised Privacy Policy.
                    You agree that if you do not agree to the new terms of the Privacy Policy, you will stop using the Sites.
                </p>

                <ol>
                    <li class="mb-4"> &nbsp; Information Go3Checkin.com Collects: <br>
                        Go3Checkin.com may collect the following information from users of our Sites: first name, last name,
                        street address, city, state, zip code, cross streets (optional), phone number, e-mail address, and
                        Sites-specific display name, (Go3Checkin.com stores only last four digits) (collectively,
                        "Personally Identifiable Information" or "PII"). Go3Checkin.com is not intended for use by
                        children under the age of 13, and Go3Checkin.com does not knowingly collect PII from children under the age of 13.
                        Use of the Sites requires that you register and/or create an account ("Account") or use the Sites as a guest.
                        If you register an account on the Website, you are responsible for maintaining the confidentiality
                        of your account and password and for restricting access to your account, and you agree to accept
                        responsibility for all activities that occur under your account or password. If you are under 13,
                        you may browse the Website but you may not register an account, purchase goods or participate in any offers or promotions.
                        The company reserves the right to refuse service, terminate accounts, remove or edit content, or cancel orders in its sole discretion.
                        The Company will not knowingly allow registration of an account for any person that the Company believes to be younger than 13.
                        We also do not collect any personally identifiable information from any persons under the age of 13 and if we discover
                        that we have been provided any such information, we will delete this information from our records. In addition to the PII set forth above,
                        Go3checkin.com may collect information regarding Account holders' Go3Checkin.com orders, gratuity amounts, favorite restaurants,
                        customer service inquiries, service/restaurant reviews, and certain social networking preferences relating to the
                        Sites (e.g. pages or entities you like, recommend or follow).
                        Go3Checkin.com also collects aggregated information, demographic information and other information that does not reveal your specific identity ("Non-PII").
                        Non-PII may include information about the device you use to access the Sites, information from referring websites and your engagement with the Sites.
                        Go3Checkin.com also aggregates certain information collected by Delivery and Order including,
                        but not limited to, certain order data, gratuity information, delivery locations, delivery driver mileage,
                        and delivery driver location. Go3Checkin.com may publish these statistics or share them with third parties without including PII.
                        Online tracking Some browsers allow you to indicate that you would not like your online activities tracked,
                        using "Do Not Track" indicators ("DNT Indicators"), however, we are not obligated to respond to these indicators.
                        Presently, we are not set up to respond to DNT Indicators. This means that while the Sites may
                        track certain latent information about your online activities, the collected information will be used to
                        improve your use of our Site and in ways consistent with the provisions of this Privacy Policy.
                    </li>
                    <li class="mb-4"> &nbsp;
                        Go3Checkin Use Of Collected Information : <br>
                        Go3Checkin.com uses PII to create users' Go3Checkin.com Accounts, to communicate with users (directly and through Salon and Spa) about
                        Go3Checkin.com services, offer users additional services, assist with Customer service inquiries,
                        offer promotions and special offers, charge for purchases and gratuities made through
                        Go3Checkin.com and fulfill Go3Checkin.com orders. Users may opt to allow Go3Checkin.com to store
                        certain PII used to create users' Go3Checkin.com Accounts, including, but not limited to, credit card
                        information (last four digits only). Go3Checkin.com uses certain stored PII to customize future order processing for you.
                        You may request that Go3Checkin.com cease storing certain PII at any time, but you might not be able to take advantage of certain customized features.
                        Go3Checkin.com may also use PII to enforce Go3Checkin.com Terms of Use.
                        Go3Checkin.com allows certain social media platforms to host plug-ins or widgets on the Sites
                        which may collect certain information about those users who choose to use those plug-ins or widgets.
                        Go3Checkin.com may also disclose PII to third parties such as attorneys, collection agencies, tribunals or
                        law enforcement authorities pursuant to valid requests in connection with alleged violations of
                        Go3Checkin.com terms of use and service or other alleged contract violations, infringement or similar harm to persons or property.
                        User generated content posted through the Sites such as service/salon reviews and certain social
                        networking preferences (e.g. pages you "Like" or "Follow") may be viewed by the general public.
                        Accordingly, Go3Checkin.com cannot ensure the privacy of any PII included in such user generated content.
                    </li>

                    <li class="mb-4"> &nbsp;
                        Go3Checkin.com Protection of PII: <br>
                        Go3Checkin.com uses reasonable security measures equal to or exceeding industry standard to protect
                        PII from unauthorized access, destruction, use, modification and disclosure. Internally, we will use reasonable efforts to restrict
                        access to your personally identifiable information to employees or contractors who need access to the information to do their jobs.
                        These employees and contractors are required to adhere to our privacy policies. We will review and update our
                        security arrangements and procedures from time to time as we deem appropriate. Unfortunately, even with these measures,
                        Go3Checkin.com cannot guarantee the security of PII. No data transmission over the Internet can be guaranteed to be completely secure.
                        As a result, while we strive to protect your personally identifiable information, we cannot ensure or warrant the security of any
                        information you transmit to us or receive from us or through one of our applications. This is especially true for information you transmit via e-mail.
                        We have no way of protecting that information until it reaches us. Once we receive your transmission,
                        we use what we believe are commercially reasonable efforts to ensure its security on our servers.
                        By using the Sites, you acknowledge and agree that Go3Checkin.com makes no such guarantee, and that you use the Sites at your own risk.
                    </li>

                    <li class="mb-4"> &nbsp;
                        Accessing and Correcting Your PII: <br>
                        Registered Go3Checkin.com Account holders can access and change their own PII using the "Change" or "Edit"
                        function on the Go3Checkin.com website. Account e-mail addresses cannot be deleted.
                        However, an Account may be closed and Go3Checkin.com will cause the corresponding e-mail address to be scrambled.
                        A user may then open a new Account with a different e-mail address.
                    </li>

                    <li class="mb-4"> &nbsp;
                        Privacy Policy Amendments: <br>
                        Go3Checkin.com may change this Privacy Policy at any time by posting a new version on this page or on a successor page.
                        The new version will become effective on the posting date, which will be listed at the top of the page as the effective date
                        September 1, 2020. You agree that you will check for new versions of this Privacy Policy.
                        By continuing to access and/or use the Sites following the posting of a new Privacy Policy version, you agree to be bound by the then-current version.
                        If you do not agree to the terms of the new version of the Privacy Policy, you will stop using the Sites.
                        <br>
                        As noted above, we may change our privacy policies from time to time and we may also modify this Privacy Policy.
                        If we plan to use your personally identifiable information in a manner different from that stated at the time of collection we will notify you via
                        the e-mail address that you have provided to us or by other means that are reasonable under the circumstances.
                        You will have a choice as to whether we use your information in this different manner.
                        <br>
                        This website may contain links to other websites. While we seek to link only to sites that share our high standards and respect for privacy,
                        we are not responsible for the privacy practices employed by, or the content contained on or accessible from, such other websites.
                        <br>
                        In the event that the Company, its business or this website becomes owned or controlled by other individuals or entities,
                        the information we have collected about you through this website or otherwise may be transferred to such other individuals or entities.
                        In such an event, this Privacy Policy will continue to apply to the information gathered online about you through this site until you are notified otherwise,
                        or a change is made to this Policy and posted or notified as provided above.
                        <br>
                        If you have questions or concerns regarding this Privacy Policy, you should contact the Company as follows:
                    </li>

                    <li>
                        Contact Information: <br>
                        If you have questions or concerns regarding this Privacy Policy, you should contact the Company as follows: <br>
                        Toll Free: 888-377-3818 <br>
                        E-mail: Support@goetu.com
                    </li>


                </ol>
            </div>

        </div>
    </div>


@endsection
