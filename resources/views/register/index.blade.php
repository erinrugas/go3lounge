@extends('layouts.app2')

@section('pageHeader')
    Register
@endsection

@section('myStyle')

    <link rel="stylesheet" href="css/select2/select2.css">
    <link rel="stylesheet" href="css/datetimepicker/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="css/intl-tel-input/intlTelInput.css">
    <style>
        .select2-container--classic .select2-selection--single, .select2-container--default .select2-selection--single {
            height: 50px !important;
            border-radius: 150px !important;
            padding: 10px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 10px !important;
        }

        .select2-search { margin: 4px 0 }
        .select2-search input,
        .select2-search input:focus {
            background-color: #fff;
            background-image: none;
            background: #fff;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border: none;
            /*border-radius: 300px;*/
        }

    </style>
@endsection

@section('content')
    <div class="container" style="padding-top: 100px;padding-bottom: 10px;width: 100%;height: 100%;min-height: calc(100vh - 127px);">
        <div class="row">
            <div class="col-md-12" style="width: 100%;padding-top: 40px;">
                <h1 class="display-1 text-center" style="width: 100%;font-size: 42px;font-family: Poppins, sans-serif;"><strong>Pre-registration Form</strong><br></h1>
            </div>
            <div class="col-md-12" style="width: 100%;padding-top: 40px;">
                <div class="alert alert-success d-none" role="alert">
                    <h4 class="alert-heading">Well done!</h4>
                    <p>
                        Our support will contact you and confirm your registration and ask for other informations to complete your registration.
                        We will also provide you manuals of how to use our system. Thank You!
                    </p>
                </div>
            </div>
        </div>
        <form id="regForm" method="POST" action="#" enctype="multipart/form-data">
            @csrf
            <div class="row pb-3">
                <div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">Business Information</h6>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="business_name">Business Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="business_name" id="business_name">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_first_name">Business Owner First Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="business_owner_first_name" id="business_first_name">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_last_name">Business Owner Last Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="business_owner_last_name" id="business_last_name">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="business_category">Business Category <span class="text-danger">*</span></label>
                        <select class="form-control rounded-pill" name="business_category" id="business_category" style="height: 50px;">
                            <option value="">Select Business Category</option>
                            <option value="1">Salon</option>
                            <option value="2">Nail Spa</option>
                            <option value="3">Dentist</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label class="mb-1"><i class="feather icon-image mr-25"></i>Upload Business Permit</label><br>
                    <small>Recommended size: 500px x 500px <span class="text-danger font-medium-5">*</span></small><br>
                    <div class="form-group">
                        <label class="btn btn-sm btn-info" for="business-permit">
                            <input name="business_permit" id="business-permit" type="file" accept=".xlsx,.xls,.csv, image/*" style="display:none"
                               onchange="$('#upload-business-permit-info').html(this.files[0].name)">
                            >
                            Upload Business Permit
                        </label>
                        <span class='label label-info' id="upload-business-permit-info">No file chosen</span>
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label class="mb-1"><i class="feather icon-image mr-25"></i>Upload Tax ID</label><br>
                    <small>Recommended size: 500px x 500px <span class="text-danger font-medium-5">*</span></small><br>
                    <div class="form-group">
                        <label class="btn btn-sm btn-info" for="tax-id">
                            <input name="business_tax_id" id="tax-id" type="file" accept=".xlsx,.xls,.csv, image/*" style="display:none"
                                   onchange="$('#upload-business-tax-id-info').html(this.files[0].name)">
                            >
                            Upload Tax ID
                        </label>
                        <span class='label label-info' id="upload-business-tax-id-info">No file chosen</span>
                    </div>
                </div>

                <div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">DBA Address</h6>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="business_street">Business address<span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="business_street" id="business_street">
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label>Zip Code:<span class="text-danger font-medium-5">*</span></label>
                        <select name="business_zipcode" data-placeholder="Type Zip Code" id="business_zipcode" class="select2 form-control business_zipcode"></select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="business_state">States<span class="text-danger font-medium-5">*</span></label>
                        <select name="business_state" data-placeholder="Select State" id="select2-state"
                                class="select2 form-control business_state"
                                style="height: 50px; border-radius: 300px"
                                value="{{ old('business_state') }}">
                            <option value="" index="">Select State</option>
                            @foreach($states as $state)
                                <option value="{{ $state->name }}" index="{{ $state->id }}" country="{{ $state->country_id }}">{{ $state->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="business_city">Cities<span class="text-danger font-medium-5">*</span></label>
                        <select name="business_city" data-placeholder="Select City"
                                class="select2 form-control business_city"
                                style="height: 50px; border-radius: 300px"
                                id="business_city"
                        >
                            <option value="" index="">Select City</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->city }}" index="{{ $city->id }}" zipcode="{{ $city->zip_code }}">{{ $city->city }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="business_timezone">Timezones<span class="text-danger font-medium-5">*</span></label>
                        <select name="business_timezone" data-placeholder="Select Timezone"
                                class="select2 form-control business_timezone"
                                style="height: 50px; border-radius: 300px"
                                id="business_timezone"
                        >
                            <option value="" index="">Select Timezone</option>
                            @foreach($timezones as $timezone)
                                <option value="{{ $timezone->id }}">{{ $timezone->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">Business Contact</h6>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_phone_1_prefix">Business Phone 1 <span class="text-danger">*</span></label>
                        <input type="hidden" id="business_phone_1_prefix" name="business_phone_1_prefix" value="{{ old('tel_prefix') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="business_phone_1" id="business_phone_1">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_phone_2_prefix">Business Phone 2 </label>
                        <input type="hidden" id="business_phone_2_prefix" name="business_phone_2_prefix" value="{{ old('tel_prefix') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="business_phone_2" id="business_phone_2">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_contact">Mobile Number <span class="text-danger">*</span></label>
                        <input type="hidden" id="business_contact_prefix" name="business_prefix" value="{{ old('tel_prefix') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="business_contact" id="business_contact">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_contact">Business Fax </label>
                        <input type="hidden" id="business_fax_prefix" name="business_fax_prefix" value="{{ old('tel_prefix') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="business_fax" id="business_fax">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_email">Personal Email <span class="text-danger">*</span></label>
                        <input type="email" class="form-control rounded-pill py-4" name="business_email" id="business_email">
                    </div>
                </div>

                <div class="col-md-12">
                    <label for="business_corporate">Corporate Email</label>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="w-100">Would you like us to create it for you? </label>

                        <label class="mr-2">
                            <input type="radio" name="business_corporate_yesno" value="yes" checked> Yes
                        </label>
                        <label>
                            <input type="radio" name="business_corporate_yesno" value="no"> No
                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="corporateEmailInput" class="form-group">
                        <input type="email" class="form-control rounded-pill py-4" name="business_corporate" id="business_corporate" placeholder="Enter Corporate Email">
                    </div>
                </div>

                <div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">Business Contact Person</h6>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="business_contact">Title <span class="text-danger">*</span></label>
                        <input type="hidden" id="contact_person_title" name="contact_person_title" value="{{ old('contact_person_title') }}">
                        <select name="contact_person_title" class="select2" id="contact_person_title" style="width: 100%">
                            <option value="MR.">MR.</option>
                            <option value="MS.">MS.</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label for="contact_person_first_name">First Name<span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_first_name" id="contact_person_first_name" value="{{ old('contact_person_first_name') }}">
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label for="contact_person_last_name">Last Name<span class="text-danger">*</span></label>
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_last_name" id="contact_person_last_name" value="{{ old('contact_person_last_name') }}">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="contact_person_first_name">Middle Initial </label>
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_middle_initial" id="contact_person_middle_initial" value="{{ old('contact_person_middle_initial') }}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_person_phone_1">Phone 1 <span class="text-danger">*</span></label>
                        <input type="hidden" id="contact_person_phone_1_prefix" name="contact_person_phone_1_prefix" value="{{ old('contact_person_phone_1') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_phone_1" id="contact_person_phone_1" >
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_person_phone_2">Phone 2 </label>
                        <input type="hidden" id="contact_person_phone_2_prefix" name="contact_person_phone_2_prefix" value="{{ old('contact_person_phone_2') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_phone_2" id="contact_person_phone_2">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_person_mobile_number">Mobile Number <span class="text-danger">*</span></label>
                        <input type="hidden" id="contact_person_mobile_number_prefix" name="contact_person_mobile_number_prefix" value="{{ old('contact_person_mobile_number') }}">
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_mobile_number" id="contact_person_mobile_number">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_person_fax">Fax </label>
                        <input type="text" class="form-control rounded-pill py-4" name="contact_person_fax" id="contact_person_fax">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_person_email">Email <span class="text-danger">*</span></label>
                            <input type="text" class="form-control rounded-pill py-4" name="contact_person_email" id="contact_person_email">
                    </div>
                </div>

            </div>
            <div class="custom-control custom-checkbox my-1 mr-sm-2">
                <input type="checkbox" class="custom-control-input" id="review-setting" name="review_setting">
                <label class="custom-control-label" for="review-setting">Proceed to Review Settings, Business Images, Menu & Others ?</label>
            </div>

            <div class="custom-control custom-checkbox my-1 mr-sm-2">
                <input type="checkbox" class="custom-control-input" id="merchant-settings" name="merchant_setting">
                <label class="custom-control-label" for="merchant-settings">Proceed to Merchant Settings ?</label>
            </div>

            <div class="custom-control custom-checkbox my-1 mr-sm-2">
                <input type="checkbox" class="custom-control-input" id="promotion-and-campaign" name="promotion_and_campaign">
                <label class="custom-control-label" for="promotion-and-campaign">Proceed to Promotion and Campaigns ?</label>
            </div>

            <div class="custom-control custom-checkbox my-1 mr-sm-2">
                <input type="checkbox" class="custom-control-input" id="manage-device" name="manage_device">
                <label class="custom-control-label" for="manage-device">Proceed to Manage Device?</label>
            </div>

            <div class="row review-row d-none">
                {{--<div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">Other Information</h6>
                </div>--}}

                {{--                <div class="col-md-6">--}}
                {{--                    <div class="form-group">--}}
                {{--                        <label for="birth_date">Birthdate <span class="text-danger">*</span></label>--}}
                {{--                        <input type="text" class="birthdate form-control rounded-pill py-4" name="birth_date" id="birth_date" autocomplete="off">--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                <div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">Review ID & Settings</h6>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="google_id">Google Place ID</label>
                        <input type="text" class="form-control rounded-pill py-4" name="google_id" id="google_id">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="yelp_id">Yelp Business ID</label>
                        <input type="text" class="form-control rounded-pill py-4" name="yelp_id" id="yelp_id">
                        <small>Note: your yelp id should consist of store name + city where your business is located. <br/>e.g nail-salon-city</small>
                    </div>
                </div>

                <div class="col-md-12">
                    <label for="facebook_page">Facebook Business Page</label>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="w-100">Would you like us to create it for you? </label>

                        <label class="mr-2">
                            <input type="radio" name="facebook_page_yesno" value="yes" checked> Yes
                        </label>
                        <label>
                            <input type="radio" name="facebook_page_yesno" value="no"> No
                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="facebookPageInput" class="form-group">
                        <input type="text" class="form-control rounded-pill py-4" name="facebook_page" id="facebook_page" placeholder="Please enter your preferred facebook page name">
                    </div>
                </div>

                <div class="col-md-12">
                    <h6 class="text-left py-3" style="font-family: Poppins, sans-serif;font-size: 1.75rem;">Business Images, Menu & Others</h6>
                </div>

                <div class="col-md-12">
                    <label>Attach Business Logo</label>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="w-100">Would you like us to create it for you? </label>

                        <label class="mr-2">
                            <input type="radio" name="business_logo_yesno" value="yes"> Yes
                        </label>
                        <label>
                            <input type="radio" name="business_logo_yesno" value="no" checked> No
                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group d-none logoUploadYes">
                        <input type="text" class="form-control rounded-pill py-4" name="logo_preference" id="logo_preference" placeholder="What is your color preference?">
                    </div>

                    <div class="form-group d-none logoUploadYes">
                        <input type="text" class="form-control rounded-pill py-4" name="logo_description" id="logo_description" placeholder="Description of the Logo Design">
                    </div>

                    <div class="form-group logoUploadNo">
                        <div class="col-md-12 col-12">
                            <small>Recommended size: 500px x 500px <span class="text-danger font-medium-5">*</span></small><br>
                            <div class="form-group">
                                <label class="btn btn-sm btn-info" for="business-logo">
                                    <input name="business_logo" id="business-logo" type="file" accept="image/*" style="display:none"
                                           onchange="$('#upload-logo-info').html(this.files[0].name)">
                                    Upload Logo
                                </label>
                                <span class='label label-info' id="upload-logo-info">No file chosen</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <label>Upload Kiosk/Andriod Backcover</label>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="w-100">Would you like us to create it for you? </label>

                        <label class="mr-2">
                            <input type="radio" name="business_cover_yesno" value="yes"> Yes
                        </label>
                        <label>
                            <input type="radio" name="business_cover_yesno" value="no" checked> No
                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group d-none coverUploadYes">
                        <input type="text" class="form-control rounded-pill py-4" name="cover_preference" id="cover_preference" placeholder="What is your color preference?">
                    </div>

                    <div class="form-group d-none coverUploadYes">
                        <input type="text" class="form-control rounded-pill py-4" name="cover_description" id="cover_description" placeholder="Description of the Back Cover Design">
                    </div>

                    <div class="form-group coverUploadNo">
                        <!-- <input type="file" name="business_cover" id="coverImageInput" accept="image/png, image/jpeg" placeholder="upload business cover"> -->
                        <div class="col-md-12 col-12">
                            <small>Recommended size: 1750px x 300px <span class="text-danger font-medium-5">*</span></small><br>
                            <div class="form-group">
                                <label class="btn btn-sm btn-info" for="business-cover">
                                    <input name="business_cover" id="business-cover" type="file" accept="image/*" style="display:none"
                                           onchange="$('#upload-cover-info').html(this.files[0].name)">
                                    Upload Cover
                                </label>
                                <span class='label label-info' id="upload-cover-info">No file chosen</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-3">
                    <label class="mb-1"><i class="feather icon-image mr-25"></i>Upload Menu / Services</label><br>
                    <!-- <small>Recommended size: 500px x 500px <span class="text-danger font-medium-5">*</span></small><br> -->
                    <div class="form-group">
                        <label class="btn btn-sm btn-info" for="service_menu">
                            <input multiple name="service_menu[]" id="service_menu" type="file" accept="xlsx,.xls,.csv, image/*, application/pdf" style="display:none"
                                {{--                            onchange="$('#upload-service-info').html(this.files[0].name)"--}}
                            >
                            Upload Menu / Service
                        </label>
                        <span class='label label-info' id="upload-service-info">No file chosen</span>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="marketing_materials">What other marketing materials do you use right now?</label>
                        <input type="text" class="form-control rounded-pill py-4" name="marketing_materials" id="marketing_materials">
                    </div>
                </div>
            </div>

            <div class="row merchant-settings-row d-none">
                <div class="col-md-12">
                    <h6 class="mt-3">Download sample format <a href="/format/merchant-settings.xlsx " download>here</a>. Fill up all the corresponding tabs.</h6>

                    <div class="col-md-12 col-12">
                        <small>Recommended size: 2MB</small><br>
                        <div class="form-group">
                            <label class="btn btn-sm btn-info" for="merchant_setting_file">
                                <input name="merchant_setting_file" id="merchant_setting_file" type="file"
                                       accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"  style="display:none"
                                       onchange="$('#merchant-settings-info').html(this.files[0].name)">
                                Upload Merchant Setting File
                            </label>
                            <span class='label label-info' id="merchant-settings-info">No file chosen</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row promo-and-campaign-row d-none">
                <div class="col-md-12">
                    <h6 class="mt-3">Download sample format <a href="/format/promotion-and-campaign.xlsx" download>here</a>. Fill up all the corresponding tabs.</h6>

                    <div class="col-md-12 col-12">
                        <small>Recommended size: 2MB</small><br>
                        <div class="form-group">
                            <label class="btn btn-sm btn-info" for="merchant_promotion_and_campaigns">
                                <input name="merchant_promotion_and_campaigns" id="merchant_promotion_and_campaigns" type="file"
                                       accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"  style="display:none"
                                       onchange="$('#promotion-and-campaign-info').html(this.files[0].name)">
                                Upload Promotion and Campaign File
                            </label>
                            <span class='label label-info' id="promotion-and-campaign-info">No file chosen</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row manage-device-row d-none">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label for="add-setting-name">Name <span class="text-danger font-medium-5">*</span></label>
                        <input id="add-setting-name" type="text" class="form-control" name="device_name" placeholder="name" value="{{ old('device_name') }}">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-theme">Theme Color</label>
                        <input type="color" class="form-control" name="theme_color1">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-theme"> </label>
                        <input type="color" class="form-control" name="theme_color2">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-theme">Font Color</label>
                        <input type="color" class="form-control" name="font_color1">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-theme"> </label>
                        <input type="color" class="form-control" name="font_color2">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-theme">Button Color</label>
                        <input type="color" class="form-control" name="btn_color1">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-theme"> </label>
                        <input type="color" class="form-control" name="btn_color2">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-pin">Pin <span class="text-danger font-medium-5">*</span></label>
                        <input id="pin" type="text" class="form-control" name="pin" placeholder="0000" data-mask="0000" data-mask-clearifnotmatch="true"/>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="add-setting-slide">Slide Duration <span class="text-danger font-medium-5">*</span></label>
                        <input id="slide_duration" type="text" class="form-control" name="slide_duration" placeholder="In seconds" data-mask-clearifnotmatch="true"/>
                    </div>
                </div>
                <div class="col-md-12 col-12">

                    <label class="mb-1"><i class="feather icon-image mr-25"></i>Upload Logo</label><br>
                    <small>Recommended size: 500px x 500px <span class="text-danger font-medium-5">*</span></small><br>
                    <div class="form-group">
                        <label class="btn btn-sm btn-info" for="device-logo">
                            <input name="logo" id="device-logo" type="file" accept=".xlsx,.xls,.csv, image/*" style="display:none"
                                   onchange="$('#upload-file-info').html(this.files[0].name)">
                            >
                            Upload Logo
                        </label>
                        <span class='label label-info' id="upload-file-info">No file chosen</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="privacy_url">Privacy URL</label>
                        <input type="text" class="form-control" name="privacy_url">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="terms_and_condition">Terms & Condition</label>
                        <textarea type="text" class="form-control" name="terms_and_condition"></textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="terms" name="terms">
                        <label class="custom-control-label" for="terms">I agree with Go3checkin's <a href="{{ route('terms.index') }}" target="_blank">Terms & Condition</a> </label>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="privacy" name="privacy">
                        <label class="custom-control-label" for="privacy">I agree with Go3checkin's <a href="{{ route('privacy.index') }}" target="_blank">Privacy Policy</a></label>
                    </div>
                </div>
            </div>

            <div class="col-md-12 py-5 text-right">
                <button type="submit" class="btn btn-primary btn-lg text-white border rounded-0 gradbutton business-register"
                        role="button"
                        style="width: 200px;font-family: Poppins, sans-serif;">Register</button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="js/select2/select2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script src="js/datetimepicker/jquery.datetimepicker.full.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="js/intl-tel-input/intlTelInput.js"></script>
@endsection

@section('myScript')
    <script>
        $(".select2").select2();
        $(document).ready(function() {

            $.get('/counrty/cacheCountryStateCities');
        });

        $("#service_menu").on('change', function () {
            var filenames = $.map(this.files, function (file) {
                return file.name;
            });
            $("#upload-service-info").html(filenames.join(', '));
        });


        // select box selectors
        var WorldCountry = '.business_country';
        var WorldState ='.business_state';
        var WorldCity = '.business_city';
        var WorldZip = '.business_zipcode';

        // populate city on state change
        $(WorldState).on('change', function() {
            var id = $('option:selected', this).attr('index');
            var country = $('option:selected', this).attr('country');

            if (id != "" && typeof id !== 'undefined' && typeof country !== 'undefined' ) {
                $.get('/cities/get/'+id+'/'+country).then(function(data) {
                    var options = '<option value="" index="">Select City</option>';
                    $.each(data, function(index, value) {
                        options += '<option value="'+value.city+'" index="'+value.id+'" zipcode="'+value.zip_code+'">'+value.city+'</option>';
                    });

                    $(WorldCity).html(options);
                });
            }
        });

        $(".business_zipcode").select2({
            dropdownAutoWidth: true,
            width: '100%',
            ajax: {
                url: "/cities/all",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    console.log("params "+params.term);
                    return {
                        _token:  $('meta[name="csrf-token"]').attr('content'),
                        country: "US",
                        search: params.term
                    };
                },
                processResults: function (response, params) {
                    console.log(response);
                    return {
                        results: response
                    };
                },
                cache: true,
            },
            placeholder: 'Enter Zipcode',
            minimumInputLength: 4,
        }).on('select2:select', function(event) {
            const { zip, city, state } = event.params.data;

            setTimeout(function() {
                $('#select2-business_zipcode-container').html(zip);
            }, 100)

            $(WorldState).val(state);
            $(WorldState).trigger('change');

            setTimeout(function(){
                $(WorldCity).val(city);
                $(WorldCity).trigger('change');
            }, 1000);
        });

        $('#regForm').submit(function (e) {
            e.preventDefault();

            // show data loading on process
            Swal.fire({
                text: "processing data please wait...",
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading ()
                }
            });

            $(this).find('.invalid-feedback').remove();
            $(this).find('input').removeClass('border-danger');

            var url = '/register/store';
            var form = new FormData(this);

            $.ajaxSetup({
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post(url, form).then(function() {
                Swal.close();

                $('.alert').removeClass('d-none');
                $('input').each(function() {
                    var input = $(this);
                    var name = input[0].name;

                    if (name != '_token') {
                        input.val('');
                    }
                });

                $('#upload-logo-info').html('No file chosen');
                $('#upload-cover-info').html('No file chosen');
                $('#upload-service-info').html('No file chosen');
                // // success alert
                Swal.fire({
                    type: "success",
                    title: "Success!",
                    text: "Your business has been registered, kindly wait for our support to confirm your registration.",
                    confirmButtonClass: "btn btn-success",
                }).then(function(){
                    // reload page
                    location.reload();
                });

            }).catch(function(err) {

                var errors = err.responseJSON.errors;
                $.each(errors, function(key, value) {
                    var err_html = '<div class="invalid-feedback d-block ">'+value[0]+'</div>';

                    $('input[name='+key+']').addClass('border-danger');

                    switch(key) {
                        case "business_city":
                        case "business_state":
                        case "business_country":
                        case "business_timezone":
                        case "business_zipcode":
                            // append error after flag dropdown
                            $('select[name='+key+']').parents('.form-group')
                            .find('span.select2')
                            .after(err_html);

                            $('select[name='+key+']').parents('.form-group')
                            .find('span.select2-selection')
                            .addClass('border-danger');
                        break;
                        case 'business_category':
                            $('select[name='+key+']').addClass('border-danger');
                            $('select[name='+key+']').parents('.form-group').append(err_html);
                        case 'business_contact':
                            $('input[name='+key+']').parents('.form-group').append(err_html);
                        break;
                        default:
                            $('input[name='+key+']').after(err_html);
                    }
                });
                Swal.close();
            });
        });

        var curDate = new Date();

        $('.birthdate').datetimepicker({
            timepicker:false,
            mask:true,
            format:'Y-m-d',
            maxDate:'0',
            yearEnd: curDate.getFullYear(),
        });

        // input format alphabet only with spaces
        var alpha = function(inputName, length, i = 0) {
            var formatArr = [];

            for(i; length>i; i++) {
                formatArr.push('A');
            }

            var format = formatArr.join('');
            $('input[name="'+inputName+'"]').mask(format, {
                'translation': {
                    A: {pattern: /[A-Za-z ]/ }
                }
            });
        }

        // input format numbers only
        var num = function(inputName, length, i = 0) {
            var formatArr = [];

            for(i; length>i; i++) {
                formatArr.push('A');
            }

            var format = formatArr.join('');
            $('input[name="'+inputName+'"]').mask(format, {
                'translation': {
                    A: {pattern: /[0-9]/ }
                }
            });
        }

        $("#business_contact, #contact-number, #business_phone_1, #business_phone_2, #contact_person_phone_1, " +
            "#contact_person_phone_2, #contact_person_mobile_number, #contact_person_fax, #business_fax").attr('placeholder', 'XXX-XXX-XXXX');
        $("#business_contact, #contact-number, #business_phone_1, #business_phone_2, #contact_person_phone_1, " +
            "#contact_person_phone_2, #contact_person_mobile_number, #contact_person_fax, #business_fax").mask('000-000-0000');
        $("#business_zip").mask('0000000000');

        // alpha('business_city', 60);
        alpha('business_state', 60);

        var businessContact = "#business_contact";
        var businessContact = "#contact-number";
        var businessContact = "#business_contact";

        var intlInput = function(selector) {
            var prefix = '_prefix';
            var input = document.querySelector(selector)
            var iti = window.intlTelInput(input, {
                allowDropdown: false,
                formatOnDisplay: true,
                onlyCountries: ['us'],
                separateDialCode: true,
                utilsScript: "js/intl-tel-input/utils.js",
            });
            var dialCode = iti.getSelectedCountryData().dialCode;
            $(selector+prefix).val(dialCode);
        }

        intlInput(businessContact);

        // business_corporate_yesno
        $('input[name="business_corporate_yesno"]').on('change', function() {

            // set variables
            var value = $(this).val();
            var corporateEmailInput = $('#corporateEmailInput');

            switch (value) {
                case 'yes':
                    if (corporateEmailInput.hasClass('d-none')) {
                        corporateEmailInput.removeClass('d-none');
                    }
                break;
                default:
                    if (!corporateEmailInput.hasClass('d-none')) {
                        corporateEmailInput.addClass('d-none');
                    }
            }
        });

        $('input[name="facebook_page_yesno"]').on('change', function() {
            // set variables
            var value = $(this).val();
            var facebookPageInput = $('#facebookPageInput');

            switch (value) {
                case 'yes':
                    if (facebookPageInput.hasClass('d-none')) {
                        facebookPageInput.removeClass('d-none');
                    }
                break;
                default:
                    if (!facebookPageInput.hasClass('d-none')) {
                        facebookPageInput.addClass('d-none');
                    }
            }
        });

        $('input[name="business_logo_yesno"]').on('change', function() {
            // set variables
            var value = $(this).val();
            var logoUploadYes = $('.logoUploadYes');
            var logoUploadNo = $('.logoUploadNo');

            switch (value) {
                case 'yes':
                    if (logoUploadYes.hasClass('d-none')) {
                        logoUploadYes.removeClass('d-none');
                    }

                    if (!logoUploadNo.hasClass('d-none')) {
                        logoUploadNo.addClass('d-none');
                    }
                break;
                default:
                    if (!logoUploadYes.hasClass('d-none')) {
                        logoUploadYes.addClass('d-none');
                    }

                    if (logoUploadNo.hasClass('d-none')) {
                        logoUploadNo.removeClass('d-none');
                    }
            }
        });

        $('input[name="business_cover_yesno"]').on('change', function() {
            // set variables
            var value = $(this).val();
            var coverUploadYes = $('.coverUploadYes');
            var coverUploadNo = $('.coverUploadNo');

            switch (value) {
                case 'yes':
                    if (coverUploadYes.hasClass('d-none')) {
                        coverUploadYes.removeClass('d-none');
                    }

                    if (!coverUploadNo.hasClass('d-none')) {
                        coverUploadNo.addClass('d-none');
                    }
                break;
                default:
                    if (!coverUploadYes.hasClass('d-none')) {
                        coverUploadYes.addClass('d-none');
                    }

                    if (coverUploadNo.hasClass('d-none')) {
                        coverUploadNo.removeClass('d-none');
                    }
            }
        });

        $("#review-setting").on('change', function() {
            if ($(this).is(':checked')) {
                $(".review-row").removeClass('d-none');
            } else {
                $(".review-row").addClass('d-none');
            }
        });

        $("#merchant-settings").on('change', function() {
            if ($(this).is(':checked')) {
                $(".merchant-settings-row").removeClass('d-none');
            } else {
                $(".merchant-settings-row").addClass('d-none');
            }
        });
        $("#manage-device").on('change', function() {
            if ($(this).is(':checked')) {
                $(".manage-device-row").removeClass('d-none');
            } else {
                $(".manage-device-row").addClass('d-none');
            }
        });

        $("#promotion-and-campaign").on('change', function() {
            if ($(this).is(':checked')) {
                $(".promo-and-campaign-row").removeClass('d-none');
            } else {
                $(".promo-and-campaign-row").addClass('d-none');
            }
        });
    </script>
@endsection
