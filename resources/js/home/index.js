import Slider from '../utilities/Slider.js';
import * as Config from '../utilities/Config.js';
import LazyLoad from "vanilla-lazyload"
import InputMask from 'inputmask';
import * as intlTelInput from 'intl-tel-input';
import IntlInput from "../utilities/IntlInput";

 //login
Slider.glideSlider('#masthead-glide', { perView: 1, grabCursor: false,  loop: false, rewind: false, keyboard: false });

//new promotion
Slider.swiperSlider('#new-promotion-swiper', {
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 3,
    loop: true,
    coverflowEffect: {
        rotate: 5,
        stretch: 0,
        depth: 350,
        modifier: 1,
        slideShadows: false
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 3
        }
    }
});

//history
Slider.swiperSlider('#history-swiper', {
    slidesPerView: 3,
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 3
        }
    }
});

/** Login **/
$("#login").on('click', function(e) {
    let errorMsg = $("#error-msg");
    let contact = $("#contact-number").val();
    let prefix = $("#contact-prefix").val();
    $("#otp").val("");
    $("#otp-msg").html("");
    $("#otp-confirm").html(`<p>Enter the code we sent to your number <br> +${prefix} ${contact} <br> <a href="#" id="resend-otp" style="color: #64a19d">Resend Code</a> </p>`);
    $(this).prop('disabled', true);
    $(this).html("Loading <i class=\"fas fa-spinner fa-spin\"></i> ");
    $("#contact-number").prop('disabled', true);

    if (contact) {

        axios.get(`${Config.APP_URL}/otp/get`, {
            params: {
                contact_number: contact,
                prefix: $("#contact-prefix").val()
            }
        }).then(res => {
            $("#otp-checker").val(res.data.internalMessage);
            errorMsg.html("");
            $("#otp-confirm").html(`<p>Enter the code we sent to your number <br> +${prefix} ${contact} <br> <a href="#" id="resend-otp" style="color: #64a19d" >Resend Code</a> </p>`);
            // $("#otp-code").html(`OTP CODE: ${res.data.userMessage}`);
            $("#login-modal").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
            $(this).prop('disabled', false);
            $(this).html("SUBMIT");
            $("#contact-number").prop('disabled', false);
        }).catch(err => {
            errorMsg.html("Number is not registered.");
            $(this).prop('disabled', false);
            $(this).html("SUBMIT");
            $("#contact-number").prop('disabled', false);
        });
    } else {

        $("#contact-number").prop('disabled', false);
        errorMsg.html("Enter your contact number");
        $(this).prop('disabled', false);
        $(this).html("SUBMIT");
    }

    e.preventDefault();
});

$(document).on('click', "#resend-otp", function() {
    axios.get(`${Config.APP_URL}/otp/get`, {
        params: {
            contact_number: $("#contact-number").val(),
            prefix: $("#contact-prefix").val()
        }
    }).then(res => {
        $("#otp-msg").addClass('text-success').removeClass('text-danger').html("Code has been sent.");
        // $("#otp-code").html(`OTP CODE: ${res.data.userMessage}`);
    }).catch(err => {
        $("#otp-msg").addClass('text-danger').removeClass('text-success').html("Resend otp failed try again or check your mobile number.")
    });
});

$("#submit-otp").on('submit', (e) => {
    let otpChecker = $("#otp-checker").val();
    let otp = $("#otp").val();

    axios.post(`${Config.APP_URL}/login`, {
        otp_checker: otpChecker,
        otp: otp,
        contact_number: $("#contact-number").val(),
        prefix: $("#contact-prefix").val()
    }).then(res => {
        $("#otp-msg").html("");

        location.href = Config.APP_URL + '/';
    }).catch(err => {
        if (err.response.status === 500) {
            $("#otp-msg").addClass('text-danger').removeClass('text-success').html("Something went wrong.");
        } else if (err.response.status === 422) {
            $("#otp-msg").addClass('text-danger').removeClass('text-success').html("I nvalid code, try to resend code.");
        }
    });

    e.preventDefault();
});

let otp = $("#otp");
otp.on('keyup', function() {
    let val = $(this).val();

    if (val.length > 0) {
        $("#otp-submit").prop('disabled', false);
    } else {
        $("#otp-submit").prop('disabled', true);
    }
});

/** RATING **/
$('.kv-ltr-theme-fas-star').rating({
    hoverOnClear: false,
    theme: 'krajee-fas',
    containerClass: 'is-star',
    min: 0, max: 5, step: 0.1, stars: 5,
    displayOnly: true,
    showClear: false,
    // showCaption: false,
    starCaptions: function(val) {
        return val
    },
    starCaptionClasses: function(val) {
        return 'badge badge-primary'
    }

});

/** LAZY LOAD **/
let myLazyLoad = new LazyLoad({
    elements_selector: ".lazy",
    load_delay: 500, //adjust according to use case
});

/** STORE **/
let storeItem = $("#store-item");
let viewSpecificStore = $("#view-specific-store");

$("#view-all-stores").on('click', function() {

    let stores = $(this).data('stores');
    let customer = $(this).data('customer');

    let html = "";
    stores.forEach(store =>  {

        if (store.customer) {
            if (store.customer.mobile_number == customer) {

                console.log((store.google.hasOwnProperty('geometry')) ? store.google.geometry.location.lat : [{}]);

                html += `
                <div class="col-md-6 col-lg-4 xl-4 mt-4">
                    <div class="card-group" >
                        <div class="card shadow visited-card">

                            <div class="img-holder">
                                <img class="card-img-top" src="
                                ${ (store.merchant.business_cover_img !== "") ? Config.APP_URL+'/storage/img/massage.jpg' : Config.APP_MERCHANT_URL+'/'+store.merchant.business_cover_img  }" >
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <a href="javascript:void(0)" class="view-store" data-is-show="1"
                                            data-business-name="${store.merchant.business_name}"
                                            data-business-phone="${store.merchant.business_phone}"
                                            data-business-address="${store.merchant.business_address}"
                                            data-business-email="${store.merchant.business_email}"
                                            data-business-cover="${ (store.merchant.business_cover_img !== "") ? Config.APP_URL+'/storage/img/massage.jpg' : store.merchant.business_cover_img  }"
                                            data-business-rate="${store.total_review}"
                                            data-google="${ (store.google) ? store.google : null }"
                                            data-google-query="${(store.google.hasOwnProperty('name')) ? store.google.name : null}"
                                            data-google-lat="${ (store.google.hasOwnProperty('geometry')) ? store.google.geometry.location.lat : null}"
                                            data-google-lng="${ (store.google.hasOwnProperty('geometry')) ? store.google.geometry.location.lat : null}"
                                            >
                                            <h4 class="card-title float-right text-purple font-italic">${store.merchant.business_name}</h4>
                                        </a>
                                    </div>
                                    <div class="col-lg-12 d-inline-block">
                                        <div class="float-left">
                                            <i class="fas fa-phone-square-alt text-red"></i>
                                        </div>
                                        <div>
                                            <p class="ml-4">${store.merchant.business_phone}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 d-inline-block">
                                        <div class="float-left">
                                            <i class="fas fa-map-marker text-cyan"></i>
                                        </div>
                                        <div>
                                            <p class="ml-4">${store.merchant.business_address}</p>
                                        </div>

                                    </div>
                                    <div class="col-lg-12 d-inline-block">
                                        <div class="float-left">
                                            <i class="fas fa-envelope-square text-blue "></i>
                                        </div>
                                        <div>
                                            <p class="ml-4">${store.merchant.business_email}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
            }
        }
     });

    $("#all-stores-row").html(html);

    storeItem.fadeIn(1000).removeClass('d-none');
    viewSpecificStore.addClass('d-none');

    $("#all-store-modal").modal('show');
});

$(document).on('click', ".view-store" ,function() {

    let isShow = $(this).data('is-show');
    let bName = $(this).data('business-name');
    let bPhone = $(this).data('business-phone');
    let bAddress = $(this).data('business-address');
    let bEmail = $(this).data('business-email');
    let bCoverImg = $(this).data('business-cover');
    let bCover = $(".b-cover-image");
    let bReview = $(this).data('business-rate');
    let googleInfo = $(this).data('google');

    $(".b-ratings").rating('update', bReview).val();

    if (isShow === 1) {
        $("#b-name").html(bName);
        $("#b-phone").html(bPhone);
        $("#b-address").html(bAddress);
        $("#b-email").html(bEmail);
        $(".b-ratings").val(bReview);

        initMap($(this).data('google-lat'), $(this).data('google-lng'), $(this).data('google-query'));

        bCover.attr('src', bCoverImg);

        if (bCoverImg === "") {
            bCover.prop('src', Config.APP_URL+'/storage/img/massage.jpg');
        } else {
            bCover.prop('src', Config.APP_MERCHANT_URL+'/'+bCoverImg);
        }

        storeItem.fadeOut(1000).addClass('d-none');
        viewSpecificStore.removeClass('d-none');

        $("#all-store-modal").modal('show');
    } else {
        initMap($(this).data('google-lat'), $(this).data('google-lng'), $(this).data('google-query'));
        storeItem.fadeOut(1000).addClass('d-none');
        viewSpecificStore.removeClass('d-none');

    }
});

$(".back-btn").on('click', function() {
    storeItem.fadeIn(1000).removeClass('d-none');
    let stores = $(this).data('stores');
    let customer = $(this).data('customer');

    let html = "";
    stores.forEach(store =>  {
        if (store.customer) {
            if (store.customer.mobile_number == customer) {

                html += `
                <div class="col-md-6 col-lg-4 xl-4 mt-4">
                    <div class="card-group" >
                        <div class="card shadow visited-card">

                            <div class="img-holder">
                                <img class="card-img-top" src="
                                ${ (store.merchant.business_cover_img === "") ? Config.APP_URL+'/storage/img/massage.jpg' : Config.APP_MERCHANT_URL+'/'+store.merchant.business_cover_img  }" >
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <a href="javascript:void(0)" class="view-store" data-is-show="1"
                                            data-business-name="${store.merchant.business_name}"
                                            data-business-phone="${store.merchant.business_phone}"
                                            data-business-address="${store.merchant.business_address}"
                                            data-business-email="${store.merchant.business_email}"
                                            data-business-cover="${ (store.merchant.business_cover_img === "") ? Config.APP_URL+'/storage/img/massage.jpg' : Config.APP_MERCHANT_URL+"/"+store.merchant.business_cover_img  }"
                                            data-business-rate="${store.total_review}"
                                            data-google="${ (store.google) ? store.google : null }"
                                            data-google-query="${(store.google.name) ? store.google.name : null}"
                                            data-google-lat="${ (store.google.geometry) ? store.google.geometry.location.lat :null }"
                                            data-google-lng="${ (store.google.geometry) ? store.google.geometry.location.lat : null}"
                                            >
                                            <h4 class="card-title float-right text-purple font-italic">${store.merchant.business_name}</h4>
                                        </a>
                                    </div>
                                    <div class="col-lg-12 d-inline-block">
                                        <div class="float-left">
                                            <i class="fas fa-phone-square-alt text-red"></i>
                                        </div>
                                        <div>
                                            <p class="ml-4">${store.merchant.business_phone}</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 d-inline-block">
                                        <div class="float-left">
                                            <i class="fas fa-map-marker text-cyan"></i>
                                        </div>
                                        <div>
                                            <p class="ml-4">${store.merchant.business_address}</p>
                                        </div>

                                    </div>
                                    <div class="col-lg-12 d-inline-block">
                                        <div class="float-left">
                                            <i class="fas fa-envelope-square text-blue "></i>
                                        </div>compo
                                        <div>
                                            <p class="ml-4">${store.merchant.business_email}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
            }
        }

             });

        $("#all-stores-row").html(html);

    viewSpecificStore.addClass('d-none');
});

$("#find-store").on('keypress, keyup', function() {
    let val = $(this).val();

    axios.get(`${Config.APP_URL}/stores/search`, {
        params: {
            search: val,
        }
    }).then(res => {

    });
});

$(".close").on('click', function() {
    $("#contact-number").prop('disabled', false).val("");
});

let contactNumber = document.getElementById('contact-number');
let contactPrefix = document.getElementById('contact-prefix');
InputMask({"mask": "999-999-9999"}).mask(contactNumber);

let intl = IntlInput.load(contactNumber, {
    separateDialCode: true,
    onlyCountries: ['us', 'ph'],
    initialCountry: "us",
});

contactNumber.addEventListener('countrychange', () => {
    let inputVal = intl.getSelectedCountryData();
    contactPrefix.value = inputVal.dialCode;
});

let inputVal = intl.getSelectedCountryData();
contactPrefix.value = inputVal.dialCode;

function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var marker = new google.maps.Marker({
            map: map,
            place: {
                placeId: results[0].place_id,
                location: results[0].geometry.location
            }
        });
    }
}

let map;
function initMap(lat, lng, query) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: 18
    });

    let request = {
        location: map.getCenter(),
        radius: '100',
        fields: ['name', 'geometry'],

        query: query
    };

    let service = new google.maps.places.PlacesService(map);
    service.textSearch(request, callback);

}
