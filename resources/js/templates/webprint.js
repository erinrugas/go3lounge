const url = 'https://admin.westminsterocprinting.com/api/';
const imgURL_thumb = 'https://admin.westminsterocprinting.com/storage/thumbnail_'
const imgURL = 'https://admin.westminsterocprinting.com/storage/image_'

var x = window.location.href; //current_url
var y = window.location.origin; //url

//get the parameter from url
var url_string = window.location.href;
var current_url = new URL(url_string);
var categoryName = current_url.searchParams.get("category") ? current_url.searchParams.get("category") : 'Business Card'; //get the category
var product_id = current_url.searchParams.get("product_id"); //get product id
var page = current_url.searchParams.get("page") ? current_url.searchParams.get("page") : 1;
//get the parameter from url

var dataCategories;
var categoryid;
var total_pages;
var currPage, currSort;
var prods = [];

//get the values from local storage
sessionStorage.getItem('currSort') ?  currSort = sessionStorage.getItem('currSort') : currSort = 'byViews';
sessionStorage.getItem('currPage') ?  currPage = parseInt(sessionStorage.getItem('currPage')) : currPage = 1;
// var categvalue = $('select[name="category"]').val() ? $('select[name="category"]').val() : '';
// end get the values from local storage


if(ifReviewOrderPage()){
  sessionStorage.setItem("frontProd", $('input[name="prod1[id]"]').val());
  sessionStorage.setItem("frontImg", $('input[name="prod1[image]"]').val());
  sessionStorage.setItem("frontName", $('input[name="prod1[name]"]').val());
  sessionStorage.setItem("backProd", $('input[name="prod2[id]"]').val());
  sessionStorage.setItem("backImg", $('input[name="prod2[image]"]').val());
  sessionStorage.setItem("backName", $('input[name="prod2[name]"]').val());
  sessionStorage.setItem("category", $('input[name="category"]').val());
  sessionStorage.setItem("busPhone", $('input[name="business_phone"]').val());
  sessionStorage.setItem("contactNumber", $('input[name="business_contact"]').val());

  var newAttachments = new Array();
  $('input[name^="attachment"]').each(function() {
    newAttachments.push($(this).val());
  });
  sessionStorage.setItem("attachments", JSON.stringify(newAttachments));  
}

if(ifOrderPage() == false && ifReviewOrderPage() == false){
  sessionStorage.removeItem("frontProd");
  sessionStorage.removeItem("frontImg");
  sessionStorage.removeItem("frontName");
  sessionStorage.removeItem("backProd");
  sessionStorage.removeItem("backImg");
  sessionStorage.removeItem("backName");
  sessionStorage.removeItem("attachments");
  sessionStorage.removeItem("category");
  sessionStorage.removeItem("busPhone");
  sessionStorage.removeItem("contactNumber");

}

// fetching data
function setAjaxHeader() {
    var token = window.localStorage.getItem('token');
    $.ajaxSetup({
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            "Authorization": "Bearer " + token
        }    
    });    
    
    categoryList();
    allproducts();
    //para sa page lang
    if(ifProductDetailsPage()){
        productDetails();
    }
}    

function apiLogin() {
    $.ajax({
        url: url+"login",
        headers: {
          'Accept': '*/*',
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        type: 'POST',
        data: {
            email : 'designer@webprint.com',
            password : 'webprintpass',
        },
        success: function(data){
            window.localStorage.setItem('token', data.success.token);
            setAjaxHeader();
        },
    });    
}

function categoryList(){
    $.ajax({
        url: url+"categories",
        type: 'GET',
        statusCode: {
          401: function() {
              apiLogin();
          },
        },
        success: function(data){
            dataCategories = data.data;

            //search the ID
            categoryid = search(categoryName, 'name', dataCategories);

            // fill category list
            dataCategories.map(function(data) {
                // if(data.parent_category == null){ //if no parent category
                //     if(data.sub_category != ''){ //if subcategory exists
                //       var li = '<li><a href="#'+data.id+'" data-toggle="collapse" aria-exp anded="false" class="dropdown-toggle"><span>'+data.name+'</span></a><ul class="collapse submenu" id="'+data.id+'">'

                //         data.sub_category.map(function(data) {
                //           li += '<li><a class="category subcateg" href="#" data-id="'+data.id+'">'+data.name+'</a></li>';
                //         })

                //         li += '</ul></li>'
                    
                //     }else{ //if subcategory does not exist
                //       var li = '<li><a class="category subcateg" href="#" data-id="'+data.id+'">'+data.name+'</a></li>';
                //     }

                //     $('#categories').append(li);
                //   }


                // TEMPORARY

                if(data.sub_category == ''){
                  var li = '<li><a class="category subcateg" href="#" data-id="'+data.id+'">'+data.name+'</a></li>';
                  $('#categories').append(li);
                }
                // TEMPORARY

                // MOBILE
                if(data.sub_category == ''){
                  var li = '<li><a class="category subcateg" data-id="'+data.id+'" href="#">'+data.name+'</></li>'
                
                  $('#categories-mobile').append(li);
                }

                //MOBILE

                // if(data.sub_category == ''){
                //     var categoryHTML = '<li><a href="index.php?category='+data.name+'">'+data.name+'</a></li>';    
                // }
            });
            //para sa page lang
            if(ifOrderPage()){
                categoryDropdown(dataCategories);      
            }
        },  
        beforeSend: function () {
            $(".category-loader").css("display", "block");
        },
        complete: function () {
            $(".category-loader").css("display", "none");
            
            if(ifProductPage()){
              $("a.category:contains('"+categoryName+"')").addClass('active');;
              $("a.category:contains('"+categoryName+"')").closest('ul').addClass('show');
  
              var categId = categoryid ? categoryid.id : null ;
          
              if(categId == null) {
                  apiURL = url+'productsWithDetails/1';
                  $('#category-title').append('ALL');
              }else{
                  apiURL = url+'productsPerCategory/'+categId+'/1';  
                  $('#category-title').append(categoryid.name);
              }
              products();
            }
        },    
    });    
}

function allproducts(){
    $.ajax({
        url: url+'productsWithDetails/1',
        type: 'GET',
        success: function(data){
            var products = data.data;

            var bestselling = products.sort(SortByOrders_desc).slice(0,6);
            var bestselling = products.slice(0,6);
            
            var newProducts = products.sort(SortByDate_desc);
            var newProducts = products.slice(0,9);

            var mostViewedProducts = products.sort(SortByViews_desc);
            var mostViewedProducts = products.slice(0,9);

            $('#bestSellerProds').empty();

            // BEST SELLING SECTION
            bestselling.map(function(data){
              
               bestsellingHTML = '<li><div class="best-seller"><a href="product_details.php?product_id='+data.product_id+'"><p class="d-xl-flex align-items-start"><img src="'+imgURL_thumb+data.photos[0].name+'">'+data.product_name+'</p></a></div></li>';
              
              $('#bestSellerProducts').append(bestsellingHTML);
            })
            
            // NEW PRODUCTS SECTION
            newProducts.map(function(data){
              
              addRibbon = ifDays15(data.date_created);
              newProductsHTML = '<div class="col-xl-4 col-lg-6 col-sm-6 col-6 product-collection"><div><div class="img-cont">'+addRibbon+'<a href="product_details.php?product_id='+data.product_id+'"><img src="'+imgURL_thumb+data.photos[0].name+'"></a><p>'+data.product_name+'</p><span class="products-btn"><a href="product_details.php?product_id='+data.product_id+'" class="btn btn-sm view-btn" role="button">VIEW</a><a href="order.php?product_id='+data.product_id+'" class="btn btn-sm order-btn" role="button">ORDER NOW</a></span></div></div></div>';
              $('#newProducts').append(newProductsHTML);
              
            });
            
            // SORT BY VIEWS PRODUCTS SECTION
            mostViewedProducts.map(function(data){
              
              addRibbon = ifDays15(data.date_created);
              mostViewedProductsHTML = '<div class="col-xl-4 col-lg-6 col-sm-6 col-6 product-collection"><div><div class="img-cont">'+addRibbon+'<a href="product_details.php?product_id='+data.product_id+'"><img src="'+imgURL_thumb+data.photos[0].name+'"></a><p>'+data.product_name+'</p><span class="products-btn"><a href="product_details.php?product_id='+data.product_id+'" class="btn btn-sm view-btn" role="button">VIEW</a><a href="order.php?product_id='+data.product_id+'" class="btn btn-sm order-btn" role="button">ORDER NOW</a></span></div></div></div>';
              $('#mostViewedProducts').append(mostViewedProductsHTML);
          
            });

        },
        beforeSend: function () {
            $(".allproducts-loader").css("display", "block");
        },
        complete: function () {
            $(".allproducts-loader").css("display", "none");
        },  
    });
}

function products(){

    $.ajax({
        url: apiURL,
        type: 'GET',
        success: function(data){

        var products = data.data;

        $('#no-products').empty();
        if(products.length <= 0){
            var productHtml = 'Sorry, No Products Found in this Category.';
            $('#no-products').append(productHtml);
        }

        // sorting
        var sort = currSort;
          if(sort == 'byName'){
            products = products.sort(SortByName_asc);
          }else if(sort == 'byViews'){
            products = products.sort(SortByViews_desc);
          }else if(sort == 'byDate'){
            products = products.sort(SortByDate_desc);
          }else if(sort == 'byOrders'){
            products = products.sort(SortByOrders_desc);
          }
        //sorting
        // pagination
        var page = currPage;
        var total_items = products.length;
        
        var limit = 12;//number of items
        total_pages = Math.ceil(total_items/limit);          
        $('#totalpage').empty()
        $('#totalpage').text(total_pages); //display the number of pages
        var offset = (page-1)*limit; //offset
        // end pagination


        $('input[class=page]').val(page);
        $("input[class=page]").attr('max',total_pages);
        ////for select pagination
        // $('.page').empty();
        // var i = 1; //initial number of page
        // while(total_pages >= i){
        //   if(i==page){  
        //     var addHtml = 'selected'
        //   }else{
        //     var addHtml = '';
        //   }
        //   var pageHtml = '<option value="'+i+'" '+addHtml+'>'+i+'</option>';
        //     $('.page').append(pageHtml);
        //     i++;
        // }

        products = products.slice(offset, offset+limit);

        products.map(function(data){
            var addRibbon = ifDays15(data.date_created);
            var productHtml = '<div class="col-xl-4 col-lg-6 col-sm-6 col-6 product-collection"><div><div class="img-cont">'+addRibbon+'<a href="#" class="product-details" data-product-id="'+data.product_id+'"><img src="'+imgURL_thumb+data.photos[0].name+'"></a><p>'+data.product_name+'</p><span class="products-btn"><a href="#" class="btn btn-sm view-btn product-details" data-product-id="'+data.product_id+'" role="button">VIEW</a></span></div></div></div>';

            $('#products').append(productHtml);
        });
        },
        beforeSend: function () {
            $('#products').empty();
            $(".products-loader").css("display", "block");
        },
        complete: function () {
            $(".products-loader").css("display", "none");
        }
    });
}

function productDetails(){
    $.ajax({
        url: url+'productDetails/'+product_id+'/1',
        type: 'GET',
        success: function(data){
            product = data.data;
            if(product[0].videos[0] != null){
              var srcUrl = new URL(product[0].videos[0].src);
              var YTid = srcUrl.searchParams.get('v');
              youtubeHtml = '<iframe width="560" height="349" src="http://www.youtube.com/embed/'+YTid+'?autoplay='+product[0].videos[0].auto_play+'&version=3&loop='+product[0].videos[0].loop_status+'&playlist='+YTid+'&controls=0" frameborder="0" allowfullscreen></iframe>';
              $('#youtube-div').append(youtubeHtml);
              $('#xzoom-magnific').remove();
            }else{
              $("#xzoom-magnific").attr("src", imgURL+product[0].photos[0].name);
              $("#xzoom-magnific").attr("xoriginal", imgURL+product[0].photos[0].name);
              
              photos = product[0].photos;
              
              photos.map(function(data){
                  thumbnailHtml = '<a href="'+imgURL+data.name+'"><img class="xzoom-gallery5 thumbnail3" width="80" src="'+imgURL_thumb+data.name+'" xpreview="'+imgURL+data.name+'"></a>';
                  $('#thumbnails').append(thumbnailHtml);
              });
            }

            $('#product_name').append(product[0].product_name);
            $('#product_desc').append(product[0].product_description);

            $("#order-now").attr("href", 'order.php?product_id='+product_id);
         
            xzoom();

        },
        // beforeSend: function () {
        //     $("#loader").css("display", "block");
        // },
        // complete: function () {
        //     $("#loader").css("display", "none");
        // }
    });
}



// call functions
if(window.localStorage.getItem('token')){
    setAjaxHeader();
}else{
    apiLogin();
};



// utilities
// search from array
function search(nameKey, prop, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i][prop] === nameKey) {
            return myArray[i];
        }    
    }    
}

function ifDays15(date_created){
    // var date_created = moment(date_created);
    // var today = moment();
    // var DaysOld = today.diff(date_created);
    // DaysOld = Math.floor(DaysOld/(1000*60*60*24));
    // if(DaysOld <= 60){
    //   return '<div class="ribbon-wrapper"><div class="ribbon">new</div></div>';
    // }else{
      return '';
    // }    
} 

function ifReviewOrderPage(){
    if(x == y + "/review_order.php" || x == y + "/review_order.php#"){
    return true;
  }else{
    return false;
  }
}

function ifProductPage(){
  // var regex = new RegExp('\/products.php');
  // var currentUrl = window.location.href; 
  // if(currentUrl.match(regex)){
    return true;
  // }else{
    // return false;
  // }
}
function ifProductDetailsPage(){
    var regex = new RegExp('\/product_details.php');
    var currentUrl = window.location.href; 
    if(currentUrl.match(regex)){
      return true;
    }else{
      return false;
    }
}

function ifIndexPage(){
    if(x == y + "/index.php" || x == y || x == y + "/" || x == y + "/#"){
    return true;
  }else{
    return false;
  }
}

function ifOrderPage(){
  var regex = new RegExp('\/order.php');
  var currentUrl = window.location.href; 
  if(currentUrl.match(regex)){
    return true;
  }else{
    return false;
  }
}

// SORTING FUNCTION
function SortByOrders_desc(a, b){ 
return ((a.product_orders > b.product_orders) ? -1 : ((a.product_orders < b.product_orders) ? 1 : 0));
}
function SortByOrders_asc(a, b){ 
return ((a.product_orders < b.product_orders) ? -1 : ((a.product_orders > b.product_orders) ? 1 : 0));
}
function SortByPrice_asc(a, b){ 
return ((a.price[0].pivot.price < b.price[0].pivot.price) ? -1 : ((a.price[0].pivot.price > b.price[0].pivot.price) ? 1 : 0));
}
function SortByName_asc(a, b){ 
return ((a.product_name < b.product_name) ? -1 : ((a.product_name > b.product_name) ? 1 : 0));
}
function SortByViews_desc(a, b){ 
return ((a.product_views > b.product_views) ? -1 : ((a.product_views < b.product_views) ? 1 : 0));
}
function SortByDate_desc(a, b){ 
return ((a.product_id > b.product_id) ? -1 : ((a.product_id < b.product_id) ? 1 : 0));
}


// onclick
$(document).on('click', 'a[class="category subcateg"]', function(e) {
  currPage = 1;
  categId = $(this).data('id');
  // console.log($('a .category'))
  $('a').removeClass('active');
  $(this).addClass('active');
  sessionStorage.setItem("currPage", currPage);
  apiURL = url+'productsPerCategory/'+categId+'/1'; 
  products();
  var device = getBrowserWidth();
  if(device == "sm" || device == "xs"){
    scroll();
  }
  return false;
});

$(document).on('change', '#sort', function() {
    currSort = $(this).val();
    currPage = 1;
    sessionStorage.setItem("currSort", $(this).val());
    sessionStorage.setItem("currPage", 1);
    products();
});

// $('a .category').click(function() {
//   alert();
// });

// $('.categoy a').click(function() {
//   alert("Do something");
// });​

// initialized
$("#sort").val(currSort);



//PRODUCTS PAGE
// on click pages -- pagination
$(document).on('change', '.page', function() {
  currPage = $(this).val();
  if(currPage <= total_pages && currPage > 0){
    sessionStorage.setItem("currPage", $(this).val());
    $(this).removeClass('error');
    products();
    scroll();
  }else{
      $(this).addClass('error');
  }
});
$(document).on('click', '.firstPage', function() {
  if(currPage>1){
  currPage = 1;
  sessionStorage.setItem("currPage", 1);
  products();
  }
});
$(document).on('click', '.nextPage', function() {
  if(currPage<total_pages){
  currPage += 1;
  sessionStorage.setItem("currPage", currPage);
  products();
  scroll();
  }
});
$(document).on('click', '.prevPage', function() {
  if(currPage >1 ){
  currPage -= 1;
  sessionStorage.setItem("currPage", currPage);
  products();
  scroll();
  }
});
$(document).on('click', '.lastPage', function() {
  if(currPage<total_pages){
  currPage = total_pages;
  sessionStorage.setItem("currPage", currPage);
  products();
  }
});
// end on click pages -- pagination



function xzoom(){

  //Integration with "Magnific Popup" plugin
  $('.xzoom5, .xzoom-gallery5').xzoom({tint: '#006699', Xoffset: 15});
  //Integration with hammer.js
  var isTouchSupported = 'ontouchstart' in window;

  if (isTouchSupported) {
    //If touch device
    $('.xzoom5').each(function(){
        var xzoom = $(this).data('xzoom');
        xzoom.eventunbind();
    });
    
  $('.xzoom5').each(function() {
    var xzoom = $(this).data('xzoom');
    $(this).hammer().on("tap", function(event) {
        
        setTimeout(openmagnific,300);

        function openmagnific() {
                var gallery = xzoom.gallery().cgallery;
                var i, images = new Array();
                for (i in gallery) {
                    images[i] = {src: gallery[i]};
                }
                $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
        }
    });
  });
  } else {
    //If not touch device

    //Integration with magnific popup plugin
    $('#xzoom-magnific').bind('click', function(event) {
        var xzoom = $(this).data('xzoom');
        xzoom.closezoom();
        var gallery = xzoom.gallery().cgallery;
        var i, images = new Array();
        for (i in gallery) {
            images[i] = {src: gallery[i]};
        }
        $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
        event.preventDefault();
    });
  }
}

function scroll(){
  $('html, body').animate({
    scrollTop: $("#scrolltothis").offset().top-150
  }, 2000);
}

var getBrowserWidth = function(){
  if(window.innerWidth < 768){
      // Extra Small Device
      return "xs";
  } else if(window.innerWidth < 991){
      // Small Device
      return "sm"
  } else if(window.innerWidth < 1199){
      // Medium Device
      return "md"
  } else {
      // Large Device
      return "lg"
  }
};

$(document).on('click', '.product-details', function() {
  var product_id = $(this).data('product-id');

  $.ajax({
      url: url+'productDetails/'+product_id+'/1',
      type: 'GET',
      success: function(data){
          product = data.data;
          images = product[0].photos;

          var fancyGallery = [];
          
          $(images).each(function(index, value){
            fancyGallery.push({src: imgURL+value.name});
          });
                    
          $.fancybox.open(fancyGallery, {
            loop : true
          });

      },
  });
  return false;
});


