let token = document.head.querySelector('meta[name="csrf-token"]').getAttribute("content");
let gKey = document.head.querySelector('meta[name="g-key"]').getAttribute("content");
let appName = document.head.querySelector("#app-name").getAttribute("content");
let protocol = location.protocol;
let appUrl = protocol + "//" + location.hostname;
let appMerchantUrl = document.head.querySelector('meta[name="merchant-url"]').getAttribute("content");


export const
    APP_NAME = appName,
    APP_TOKEN = token,
    APP_URL = appUrl,
    G_KEY = gKey,
    APP_MERCHANT_URL = appMerchantUrl,
    PROTOCOL = protocol
