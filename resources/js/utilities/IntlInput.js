import * as intlTelInput from "intl-tel-input";

class IntlInput {

    static load(selector, options = {}) {
        return intlTelInput(selector, options);
    }

}

export default IntlInput;
