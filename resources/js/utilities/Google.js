import * as Config from './Config.js';

class Google {


    static placeAPI(placeId) {
        axios.get(`https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${Config.G_KEY}`)
            .then(res => {
                return res;
            })
            .catch(err => `placeid: ${err}`);
    }
}

export default Google;
// https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ8014tUKhkTMRMu8eKoki5HE&key=AIzaSyBprFXVphtQAWlNcoxAOYHlihFPqe8y6sU
