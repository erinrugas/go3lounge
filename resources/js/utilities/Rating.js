class Rating {

    /**
     * star rating dynamic function
     *
     *
     * @param element
     * @param objs = (see https://plugins.krajee.com/star-rating#options to add / modify rating object)
     * @returns {{min: number, containerClass: string, max: number, hoverOnClear: boolean, theme: string, step: number, stars: number, showClear: boolean} & {}}
     */
    static starRating(element, objs = {}) {

        let defaultSettings = {
            hoverOnClear: false,
            theme: 'krajee-fas',
            containerClass: 'is-star',
            min: 0,
            max: 5,
            step: 0.1,
            stars: 5,
            showClear: false,

        };

        let rating = {};

        for (let defaultSetting in defaultSettings) {
            if (objs) {
                for (let obj in objs) {
                    if (defaultSetting === obj) {
                        console.log("same key name " + obj);
                        rating[defaultSetting] = objs[obj]; //new value;
                    } else {
                        rating[obj] = objs[obj];
                    }
                }
            }
        }

        return Object.assign(defaultSettings, rating);

    }

}
export default Rating;
