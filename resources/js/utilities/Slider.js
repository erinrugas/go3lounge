import Glide from '@glidejs/glide'
import Swiper from 'swiper';
import Helper from "./Helper";

class Slider {
    /**
     *
     * @param element
     * @param obj = {
     *     type: type of movement (slider or carousel) (String) ,
     *     startAt: start at specific slider number (Number),
     *     perView: Number of visible Slider (Number),
     *     focusAt: Focus currently active slide at a specified position (Number | String),
     *     gap: size of space between sliders (Number),
     *     autoplay: change sliders after a specific interval (Number | Boolean),
     *     hoverpause: stop autoplay on mouseover (Boolean),
     *     keyboard: change slides with keyboard arrows (Boolean),
     *     bound: Stop running perView number of slides from the end (Boolean),
     *     swipeThreshold - Minimal swipe distance needed to change the slide (Number | Boolean),
     *     dragThreshold - Minimal mousedrag distance needed to change the slide (Number | Boolean),
     *     perTouch - A maximum number of slides moved per single swipe or drag (Number | Boolean),
     *     touchRatio - Alternate moving distance ratio of swiping and dragging (Number),
     *     touchAngle - Angle required to activate slides moving (Number),
     *     animationDuration - Duration of the animation (Number),
     *     rewind - Allow looping the slider type (Boolean),
     *     rewindDuration - Duration of the rewinding animation (Number)
     *     animationTimingFunc - Easing function for the animation (String),
     *     direction - Moving direction mode (String),
     *     peek - The value of the future viewports which have to be visible in the current view (Number|Object),
     *     breakpoints - Collection of options applied at specified media breakpoints (Object),
     *     classes - Collection of used HTML classes (Object),
     *     throttle - Throttle costly events (Number)
     * }
     * @returns {Glide}
     */
    static glideSlider(element, obj = {}) {
        return new Glide(element, obj).mount();
    }

    /**
     *
     * @param element
     * @param obj = {}
     * @returns {Swiper}
     */
    static swiperSlider(element, obj = {}) {
        return new Swiper(element, obj);
    }

}
export default Slider;
