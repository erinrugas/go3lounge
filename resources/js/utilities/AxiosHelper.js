class AxiosHelper {

    static request(msgElem, elemHide) {
        axios.interceptors.request.use(function(config) {
            Helpers.preloader(".table-responsive");
            $(elemHide).hide();
            return config;
        }, function(error) {
            return Promise.reject(error);
        });
    }

    static response(elemShow, elemHide) {
        axios.interceptors.response.use(function(response) {
            setTimeout(() => {
                $(".table").show();
                $(".table-responsive > .data-loader").hide();
            }, 1000);
            return response;
        }, function(error) {
            return Promise.reject(error);
        });

    }

}

export default AxiosHelper;
