$('.kv-ltr-theme-fas-star').rating({
    hoverOnClear: false,
    theme: 'krajee-fas',
    containerClass: 'is-star',
    min: 0, max: 5, step: 0.1, stars: 5,
    displayOnly: true,
    showClear: false,
    showCaption: false,
});
