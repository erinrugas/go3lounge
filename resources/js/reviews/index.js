import Rating from "../utilities/Rating";
import * as Config from "../utilities/Config";
import Google from "../utilities/Google";
let storeRate = $('#store-rate');
showComment(storeRate.val());

let ratings = Rating.starRating('#store-rate', {step: 1, showCaption: false, animate: false});

storeRate.rating(ratings);

storeRate.on('rating:change', (event, value, caption) => {
    showComment(value);
    $("#btn-reviews").prop('disabled', false);
});

let googleReview = $("#google-review");

googleReview.on('click', (e) => {
    let bid = $('#google-review').data('bid');
    let lid = $("#google-review").data('lid');
    let googlePID = $("#google-review").data('google-pid');
    googleReview.prop('disabled', true);
    googleReview.html("Please wait...");
    axios.put(`${Config.APP_URL}/reviews/${bid}/${lid}`, {
        rate: '5',
        platform: 'google'
    }).then(res => {
        window.open(`https://search.google.com/local/writereview?placeid=${googlePID}`, '_blank');
        location.href = `${Config.APP_URL}/reviews/${bid}/${lid}`;
        googleReview.prop('disabled', false);
        googleReview.html('RATE IN GOOGLE');
    }).catch(error => {
        alert("error in google review "+error);
    });

    e.preventDefault();
});

let yelpReview = $("#yelp-review");
yelpReview.on('click', (e) => {
    let bid = $('#yelp-review').data('bid');
    let lid = $("#yelp-review").data('lid');
    let yelpId = $("#yelp-review").data('yid');
    yelpReview.prop('disabled', true);
    yelpReview.html("Please wait...");
    axios.put(`${Config.APP_URL}/reviews/${bid}/${lid}`, {
        rate: '5',
        platform: 'yelp'
    }).then(res => {
        console.log(res.data);
        window.open(`https://www.yelp.com/biz/${yelpId}`, '_blank');
        location.href = `${Config.APP_URL}/reviews/${bid}/${lid}`;
        yelpReview.prop('disabled', false);
        yelpReview.html("RATE IN YELP")
    }).catch(error => {
        alert("error in yelp review "+error);
    });

    e.preventDefault();
});

let facebookReview = $("#facebook-review");
facebookReview.on('click', (e) => {
    let bid = facebookReview.data('bid');
    let lid = facebookReview.data('lid');
    let fbID = facebookReview.data('fb-review');
    facebookReview.prop('disabled', true);
    facebookReview.html("Please wait...");
    axios.put(`${Config.APP_URL}/reviews/${bid}/${lid}`, {
        rate: '5',
        platform: 'facebook'
    }).then(res => {
        window.open(`https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2F${fbID}%2Freviews%2F`, '_blank');
        location.href = `${Config.APP_URL}/reviews/${bid}/${lid}`;
        facebookReview.prop('disabled', false);
        facebookReview.html("RATE IN YELP")
    }).catch(error => {
        alert("error in facebook review "+error);
    });

    e.preventDefault();
});


function showComment(val) {
    let platformChecker = $("#checker").data('platform-checker');

    if (val < 4) {
        $("#comment-form").fadeIn().removeClass('d-none');
        $("#social-review").fadeOut().addClass('d-none');
        $("#review-submit").removeClass('d-none');
    } else {
        if (platformChecker == "1") {
            $("#comments").val("");
            $("#comment-form").fadeOut().addClass('d-none');
            $("#social-review").fadeIn().removeClass('d-none');
            $("#review-submit").addClass('d-none')
        } else {
            $("#comment-form").fadeIn().removeClass('d-none');
            $("#social-review").fadeOut().addClass('d-none');
            $("#review-submit").removeClass('d-none');
        }
    }
}

// //responsive
// let mediaQuery = (mq) => {
//     let phone = document.querySelector('.business-phone');
//     let address = document.querySelector('.business-address');
//     let email = document.querySelector('.business-email');
//     if (mq.matches) {
//         phone.innerHTML = "Call Us";
//         address.innerHTML = "Navigation";
//         email.innerHTML = "Email Us";
//     } else {
//         phone.innerHTML = phone.dataset.phone;
//         address.innerHTML = address.dataset.address;
//         email.innerHTML = email.dataset.email;
//     }
// }
//
// let mQ = window.matchMedia("(max-width: 576px)");
// mediaQuery(mQ);
// mQ.addListener(mediaQuery);
