const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css').options({
        processCssUrls: false
    })
    .sass('resources/sass/reviews/review.scss', 'public/css')
    .sass('resources/sass/appointments/appointment.scss', 'public/css');


/** VENDORS **/

mix.styles([
    "node_modules/bootstrap-star-rating/css/star-rating.css",
    'node_modules/bootstrap-star-rating/themes/krajee-svg/theme.css',
    'node_modules/bootstrap-star-rating/themes/krajee-fas/theme.css',
], 'public/css/ratings/star-rating.css');

mix.scripts([
    'node_modules/bootstrap-star-rating/js/star-rating.js',
    'node_modules/bootstrap-star-rating/themes/krajee-svg/theme.js',
    'node_modules/bootstrap-star-rating/themes/krajee-fas/theme.js',
], 'public/js/ratings/star-rating.js');

mix.styles([
    'node_modules/@glidejs/glide/dist/css/glide.core.min.css',
    'node_modules/@glidejs/glide/dist/css/glide.theme.min.css'
], 'public/css/glide/glide.css');

mix.scripts([
    'resources/vendors/jquery/jquery.min.js',
], 'public/js/jquery.min.js');

mix.styles([
    'resources/vendors/bootstrap/css/bootstrap.min.css',
],'public/css/bootstrap/bootstrap.min.css');

mix.scripts([
    'resources/vendors/bootstrap/js/bootstrap.min.js',
], 'public/js/bootstrap/bootstrap.min.js');

mix.styles([
    'resources/vendors/datepicker/daterangepicker.css',
],'public/css/datepicker/datepicker.css');

mix.scripts([
    'resources/vendors/datepicker/daterangepicker.js',
], 'public/js/datepicker/datepicker.js');

mix.styles([
    'resources/vendors/datetimepicker/jquery.datetimepicker.min.css',
],'public/css/datetimepicker/jquery.datetimepicker.min.css');

mix.scripts([
    'resources/vendors/datetimepicker/jquery.datetimepicker.full.min.js',
], 'public/js/datetimepicker/jquery.datetimepicker.full.min.js');

mix.styles([
    'resources/vendors/intl-tel-input/css/intlTelInput.css',
],'public/css/intl-tel-input/intlTelInput.css');

mix.copy('resources/vendors/intl-tel-input/img', 'public/css/intl-tel-input/img');

mix.scripts([
    'resources/vendors/intl-tel-input/js/intlTelInput.js',
], 'public/js/intl-tel-input/intlTelInput.js');

mix.scripts([
    'resources/vendors/intl-tel-input/js/utils.js',
], 'public/js/intl-tel-input/utils.js');

mix.styles([
    'node_modules/swiper/css/swiper.css'
],'public/css/swiper/swiper.css');

mix.styles([
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css',
], 'public/css/fancyapps/jquery.fancybox.css');

mix.scripts([
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
], 'public/js/fancyapps/jquery.fancybox.js');

mix.styles([
     'resources/vendors/select2/dist/css/select2.min.css',
    'node_modules/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css'
], 'public/css/select2/select2.css');

mix.scripts([
    'resources/vendors/select2/dist/js/select2.min.js',
], 'public/js/select2/select2.js');

/** END VENDORS **/

//home
mix.js('resources/js/home/index.js', 'js/home');

//profile
mix.js('resources/js/profile/index.js', 'js/profile');

//stores
mix.js('resources/js/stores/index.js', 'js/stores');

//reviews
mix.js('resources/js/reviews/index.js', 'js/reviews');


//template
mix.js('resources/js/templates/webprint.js', 'js/templates');
mix.js('resources/js/templates/product.js', 'js/templates');
mix.js('resources/js/website2.min.js', 'js/');
mix.styles([
    'resources/css/templates/template.css',
],'public/css/templates/template.css');
mix.styles([
    'resources/css/website2.min.css',
],'public/css/website2.min.css');

//npm run production
if (mix.inProduction()) {
    mix.version();
}
