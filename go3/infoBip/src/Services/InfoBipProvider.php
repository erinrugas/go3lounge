<?php
/**
 * Created by Eunamagpantay.
 * User: Eunamagpantay
 * Date: 4/25/18
 * Time: 4:31 PM
 */

namespace Go3\InfoBip;

use Go3\InfoBip\Http\HttpClient;

class InfoBipProvider implements InfoBipService
{
    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * Username
     * @var string
     */
    protected $userName;

    /**
     * Password
     * @var string
     */
    protected $password;

    /**
     * Sender
     * @var string
     */
    protected $sender;

    /**
     * Setting client to use for infoBip.
     * PrintNodeProvider constructor.
     * @param string $userName
     * @param string $password
     * @param string $sender
     */
    public function __construct($userName = "", $password = "", $sender = "")
    {
        $this->userName = $userName;
        $this->password = $password;
        $this->sender = $sender;
        $baseUrl = "https://api2.infobip.com/api/v3/sendsms/plain";
        $this->httpClient = HttpClient::getInstance($baseUrl);
    }

    /**
     * Send PLAIN SMS
     * @param $message
     * @param $mobileNumber
     * @return mixed
     */
    public function send($message, $mobileNumber)
    {
        $formParams = [
            "form_params" => [
                "user" => $this->userName,
                "password" => $this->password,
                "send" => $this->sender,
                "SMSText" => $message,
                "GSM" => $mobileNumber
            ]
        ];
        return $this->httpClient->get("", $formParams);
    }
}
