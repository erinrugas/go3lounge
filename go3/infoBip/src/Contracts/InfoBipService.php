<?php
/**
 * Created by PhpStorm.
 * User: eunamagpantay
 * Date: 4/25/18
 * Time: 4:13 PM
 */

namespace Go3\InfoBip;

interface InfoBipService
{
    const SMS_MESSAGE_PLAIN = "plain";
    /**
     * Send plain sms
     * @param $message
     * @param $mobileNumber
     * @return mixed
     */
    public function send($message, $mobileNumber);
}
