<?php
/**
 * Created by PhpStorm.
 * User: eunamagpantay
 * Date: 4/26/18
 * Time: 3:04 PM
 */

namespace Go3\InfoBip\Http;

use GuzzleHttp\Client;


class HttpClient
{
    /**
     * Single instance.
     *
     * @var $instance
     */
    private static $instance;

    /**
     * GuzzleHttp
     *
     * @var Client $httpClient
     */
    protected $httpClient;

    /**
     * Http options.
     *
     * @var array options
     */
    protected $options;


    /**
     * HttpClient constructor.
     *
     * @param string|null $baseUri
     * @param array $options
     */
    protected function __construct($baseUri = null, array $options = [])
    {
        $options = array_merge($options, ["base_uri" => $baseUri]);

        $this->options = $options;
    }

    /**
     * @param mixed $instance
     */
    public static function setInstance($instance): void
    {
        self::$instance = $instance;
    }

    private function __clone()
    {
    }

    /**
     * Get a instance of the Guzzle HTTP client.
     *
     * @return Client
     */
    protected function getHttpClient()
    {
        if (isset($this->options)) {
            $this->httpClient = new Client($this->options);
        } else {
            $this->httpClient = new Client();
        }

        return $this->httpClient;
    }

    /**
     * Get request options
     * @return array
     */
    protected function getRequestOptions()
    {
        return [];
    }

    /**
     * Execute the http request by guzzleHttp.
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function execute(string $method, string $url, array $options = [])
    {
        $requestOptions = $this->getRequestOptions();

        if (!empty($this->options)) {
            $requestOptions = array_merge($requestOptions, $this->options);
        }

        $requestOptions = array_merge($requestOptions, $options);

        $response = $this->getHttpClient()->request($method, $url, $requestOptions);

        $result = json_decode($response->getBody(), true);

        return $result;
    }

    /**
     * Get request to the url.
     *
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function get(string $url, array $options = [])
    {
        return $this->execute("GET", $url, $options);
    }

    /**
     * Post request the url.
     *
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function post(string $url, array $options = [])
    {
        return $this->execute("POST", $url, $options);
    }

    /**
     * Post request the url.
     *
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function put(string $url, array $options = [])
    {
        return $this->execute("PUT", $url, $options);
    }

    /**
     * Get http client instance.
     *
     * @param string $baseUrl
     * @param array $options
     * @return HttpClient
     */
    public static function getInstance(string $baseUrl = null, array $options = [])
    {
        if (static::$instance) {
            return static::$instance;
        } else {
            return new HttpClient($baseUrl, $options);
        }
    }
}
