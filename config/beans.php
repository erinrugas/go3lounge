<?php
/**
 *
 * Created by PhpStorm.
 * User: goetu-erinrugas
 * Date: 1/28/20
 * Time: 11:15 AM
 */


return [
    "App\Contracts\OTP" => [
        "class" => "App\Services\OtpServices",
        "shared" => false,
        "singleton" => true,
    ],
    "App\Notifications\Channels\InfobipSmsChannel" => [
        "class" => "App\Notifications\Channels\InfobipSmsChannel",
        "shared" => false,
        "singleton" => true,
    ],
    "App\Contracts\MerchantInformationContract" => [
        "class" => "App\Services\MerchantInformationServices",
        "shared" => false,
        "singleton" => true,
    ],
];
